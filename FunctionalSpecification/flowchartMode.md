
As a User, I want a flowchart view on my Form
---------------------------------------------
![Flowchart](./img/flowchart.png)
## I want to be able to jump to Flowchart Mode easily if a Form is open
-   When user hovers over Mode icon in the Upper Bar, Flowchart mode icon should show
-   Clicking Flowchart mode shall transition the user to Flowchart Mode
    and show the icon as the new Mode icon in the Upper Bar
-   Flowchart Mode
    -   shall bring up a *Flowchart Canvas* on the whole *Form Control Panel*
    -   shall transform the Form (pages/sections/questions) to Card Formant
-   Flowchart Canvas
    -   shall have Zoom In, Zoom Out, Fit to screen icons/functionality
    -   shall have a sticky footer of the Navigation icons (next/
-   I want to have a footer to show the name of the "Submit" to
    represent the end of the page
-   When the Question has Visibility rules, the Card shall have a "red" visibility icon
    ![Flowchart Card](./img/flowchart_card.png)
-   I want to be able to navigate around the Flowchart by dragging with
    the mouse

### Able to see Visibility Rule relationships of Questions/Sections/Pages in a flowchart
-   I want see a Page column, a Section Column and a Question Column
    organized by this category from left to right
-   I want to see a arrows from Page-\>Section-\>Question-\>Other
    Questions to represent the flow of the whole Form
-   I want to see "quick links" to off-screen Pages/Sections/Questions
    that a card is related to

### I want to be able to interact with Flowchart cards
![Flowchart Visibility](./img/flowchart_visibility.png)
-   I want to click on a Question and see Visibility Settings come up on
    the right sidebar
-   I want to be able to click "quick links" to off screen
    Sections/Questions/Pages and jump to them
    
### I want to be able to add a flow relationship to a card
![Flowchart Relationship](./img/flowchart_relationship.png)
-   Hovering over any Page/Section/Question card shall fade in "Add Visibility Rule" that shows up on the side bar *\<see* *Visibility Rule Editor\>*
-   Selected card shall have a highlight

### I want to be able to export / import an excel sheet to set the flowchart rules
-   Export Logic Chart Hovering over any Page/Section/Question card
    shall fade in "Add Visibility Rule" that shows up on the side bar
    *\<see* *Visibility Rule Editor\>*
-   Selected card shall have a highlight