As a User, I want to configure questions
----------------------------------------

![Sections Panel](./img/sections_panel.png)
## Add questions
-   An "Add Question" button shall be visible at the end of any Section in
    the Form Edit Panel
-   When "Add Question" is clicked
    -   A new question card will be created
    -   Default Question Type shall be "TEXT INPUT"
    -   cursor shall jump to and select the Question Name
    -   question shall be in Expanded state
-   Questions shall only be allowed to be added inside Sections

## Question Actions in Form Edit Panel
-   I want to see a "..." icon in the Question when it is folded/collapsed
-   I want to see the following icons always visible when a Question is expanded:
    -   Settings icon
    -   Copy icon
    -   Delete icon
    -   Collapse icon
    -   Mandatory icon
-   The following "intuitive" icons replace the "..." icon when I "hover" over the Question when folded:
    -   Settings icon
    -   Copy icon
    -   Delete icon
    -   Expand icon

### Question Settings
-   I want to click the 'settings' icon and see the Question Settings page come up
    -   Visibility Rules \<see Visibility Rules\>
    -   Validation Rules \<see Validation Rules\>
    -   Help Text

### Copy questions
-   I want to be click the copy icon and find the whole Question copied
    directly below the current Question
-   I want to stay locked with the Question selected after I copy that
    question

### Delete questions with prompt
-   I want to be prompted "Are you sure you want to Delete this
    Question? Doing so cannot be reverted."
-   I want to still see my Question if I click "no" on the prompt
-   I want for my Question to be deleted if I click "yes" on the prompt

### Mandatory 
-   Mandatory icon shall be present in Expanded Vuew
-   A red asterisk on the Question in both Collapsed View shall be visible when Mandatory Toggle is "on"
-   In Display Mode, when field is mandatory and not answered. This error Message should show:
    -   "This field is required"

### Collapse/Expand Question
-   I want to collapse/expand a question when I click on the collapse/expand icon
-   I want to collapse/expand a question when I click on the blank space in a question
-   User may ONLY keep at most one question expanded per section
-   I want to see a highlight on the question in focus when in Expanded

## Question Content
-   Question Name in textfield shall always be present
    -   Question Name can be empty, and have this placeholder visible
        -   "Type your Question here..."
    -   Question Name when exceeded field length should wrap to next line upto a maximum of 3 lines
-   A Question Type dropdown option shall be present to select the Question Type
-   A Question count shall be visible beside the Question Name collapsed/expanded
    -   Question count shall start form 1 and increment for every question in sequence for one form.
        -   Every from will start its count from 1
    -   When rearranging questions(edit and design mode)/sections/pages, Question number shall update to correct sequence based on order in the whole form

### Reorganize questions using Drag and Drop within the Page/Section
-   I want to see a Moveable Indicator when I "hover" over the Edit/Summary view of a Question
-   I want to be able to drag and drop questions
    -   within a section and to other sections
    -   to other sections in other Pages in the Pages Outline Panel
        -   the question card shall become smaller when in Pages Outline Panel
        -   the focus should jump to the question dropped when let go
-   Interaction   
    -   When dragging:
        -   All Question shall fold in Summary View
        -   Question dragged shall fold in Summary View
        -   Questions in the background shall open up spaces to show space to indicate where my Question will land
        -   Edge of screen drag on Pages Outline Panel/Form Edit Panel shall scroll up/down to get to the page/section I need
    -   When dropped:
        -   If question is previously expanded, it shall expand
        -   If question is previously collapsed, it shall stay collapsed

## Question Referencing [MVP3]
-   I want to type '#' and be given a dropdown list of Question' values one may reference to
-   I want to be able to remove the Question Value Reference easily
