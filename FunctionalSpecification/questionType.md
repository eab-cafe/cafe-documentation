## Intro - Different types of questions
---------------------------------------------------------
-   In Question Edit View, I want to be able to edit the Question Name
-   In Question Edit View, I want to see a dropdown to choose the
    Question Type
-   Question Settings 
 
## Question Type Settings Matrix
----------------------------- 
?> Below is a Matrix to define the behavior of standard settings for each question: Mandatory, Visibility Rule, Plain Text, Calculated Field

![Settings Matrix](img/questionType_SettingsMatrix.png)

## Text Input
![Text Input](./img/questionType_textInput.png)
![Text Input](./img/questionType_textInput2.png)
![Text Input](./img/questionType_textInput3.png)
-   There shall be 2 *Input Type* choices:
    -   "Single Line"
        -   shall be the default selected option
    -   "Multiple Lines"
-   Standard Settings
    -   "Prefix" and "Postfix"
        -   In Preview Mode:
            -   Shall show the text field with Text Adornments
            -   "Prefix" shows at start of text
            -   "postfix" shows at the end of text
    -   "Format" (dropdown)
        -   Values shall contain:
            -   Any
            -   Numeric Only
            -   Characters Only
            -   E-mail
        -   In Preview Mode:
            -   Numeric Only, Character Only - shall only allow the
                format as indicated
            -   E-mail -- shall raise an error when input is not an
                email
                -   "Please enter a valid email address."
        -   Currency [MVP2]
            -   Shall have a field to input Currency Code. Can be left
                blank
            -   Shall have checkbox option to show with Decimals
        -   Phone number [MVP2]
            -   shall show country code picker
            ![Text Input](./img/questionType_textInputPhone.png)
    -   "Max Length" (text field accepts numeric only)
        -   When empty, show placeholder: "no limit"
        -   In Preview Mode
            -   Character count indication shall show on the text field
            -   When Character limit is reached
                -   Stop cursor from typing further
                -   Character count turns red
-   Validation Rule \< see Validation Rule\>
    -   **Condition**:
        -   "Min Length"
            -   **Value**: Textbox (numeric only)
        -   "Required characters" (for passwords)
            -   **Value**:
                -   Checkbox fields (multiple selection)
                    -   0-9/a-z/A-Z/a-Z/Special Characters
        -   "Greater Than", "Less Than", "\>=", "\<=", "Equal to"
            -   Shall only enable when *Accepted format* is Numeric
            -   Choices shall end with either "Field" or "Value"
                -   E.g.
                    -   Greater than Field
                    -   Greater than Value
                    -   Less than Field
                    -   ...
            -   **Value:**
                -   If ends with "field" -\> Dropdown pointing to other
                    questions
                -   If ends with "Value" -\> Textbox (Numeric Only)
-   Design Question Styles
    -   \<To be filled\> 

## Multiple Choice
![Multiple Choice](./img/questionType_multiplechoice.png)
-   There shall be 2 *Input Type* choices:
    -   "Single Answer" 
        -   shall be the default selected option 
        -   shall have `Radio Buttons` icons at the start of every option 
    -   "Multiple Answers" 
        -   shall have `Check Boxes` icons at the start of every option
-   Interaction:
    -   [Common Components - List Handling](/FunctionalSpecification/questionType?id=common-components-list-handling)
-   Standard Settings 
    -   "Upload Image Choices" [MVP2] 
        -   This icon shall appear for every option in mltiple choice 
    -   "Show 'others' option" 
        -   Shall show up a Text field in Display View 
        -   Shall deselect all other options automatically 
    -   "Assign 'point' value" 
        -   Shall have a textbox (numeric only) appear for every option 
-   Validation Rule \< see Validation Rule\> 
    -   **Condition**:
        -   "Maximum number of choices"
            -   Shall only show when "multiple answers" is selected
            -   **Value**:
                -   textbox (numeric only)
                -   Defines the maximum allowed 'checks' in validation
        -   "Minimum number of choices"
            -   \<same as Maximum number of choices\>
-   Design Question Styles
    -   \<To be filled\> 

## Cascading Dropdown List 
-   Description Text shall be visible: 
    -   "A dropdown in a "Cascaded Dropdown List" automatically filters
        values depending on the selected value of the prior dropdown as
        defined in the *Cascading Dropdown Settings*." 
-   In Preview Mode, user shall see number of dropdowns depending on the
    user Cascading Dropdown settings file 
-   There shall be 2 options to load in *Cascading Dropdown Settings* 
    -   Upload a *Cascading Dropdown Settings File* in xls format
        (upload button) 
        -   Help Text: 
            -   "Upload Cascading Dropdown Settings to specify the
                relationships of each dropdown." 
            -   There shall be a downloadable attachment on a sample of
                the *Cascading Dropdown Settings File* 
    -   Select an existing "Cascading Dropdown Settings Resource File"
        from the resource list (dropdown) \[MVP3\] 
-   Once the *Cascading Dropdown Settings* is loaded, Question Name
    Fields should appear according to Dropdown Lists in the file. 
    -   i.e. If the file contains 3 dropdown lists, there shall be 3
        Question Name Fields for user to input 
-   Preview Panel shall show number of dropdown depending on the
    settings in the File. 
-   Interaction
    -   [Common Components - List Handling](/FunctionalSpecification/questionType?id=common-components-list-handling) 
-   Standard Settings 
    -   "Show 'others' option" 
        -   Shall show up a Text field in Display View 
-   Validation Rule \< see Validation Rule\> 
    -   \<none\> 
-   Design Question Styles 
    -   \<To be filled\> 

## Dropdown 
![Dropdown Choice](./img/questionType_dropdown.png)
-   In Preview Mode, user shall be able to type to refine the dropdown selection 
-   There shall be 2 *Input Type* choices:
    -   "Custom" 
        -   shall be the default selected option 
        -   options shall be prepended with numbering starting from 1 
    -   "Select from a predefined list" 
        -   List shall show as a dropdown to select from 
            -   Months of Year, Gender, Countries, etc. 
        -   User shall be able to remove the list at any time 
        -   User shall be able to save the listed options as a Resource
            \[MVP3\] 
-   Interaction:
    -   Include checking rules: [Common Components - List Handling](/FunctionalSpecification/questionType?id=common-components-list-handling) 
-   Standard Settings 
-   Validation Rule \< see Validation Rule\> 
    -   \<none\> 
-   Design Question Styles 
    -   \<To be filled\> 

## Date and/or Time 
![Date and Time](./img/questionType_dateTime.png)
![Date and Time](./img/questionType_dateTime2.png)
![Date and Time](./img/questionType_dateTime3.png)
-   There shall be 3 *Date/Time Type* choices: 
    -   "Date" 
        -   shall be the default selected option 
    -   "Time" 
    -   "Date & Time" 
-   Standard Settings 
    -   "Range" (Checkbox) 
        -   In Preview mode, question shall show 2 date/time inputs 
    -   "Format" (dropdown) 
        -   Choices for "Date" should be: 
            -   Month / Year 
            -   Month / Day / Year 
            -   Day / Month / Year 
            -   YYYY/MM/DD
        -   In Preview Mode, date shall show above chosen format 
    -   "Time Format" (dropdown) 
        -   Choices for "Time" should be: 
            -   12hr 
            -   24hr 
            -   w/ GMT timezone, etc. 
    -   "Min/Max Date/Time" (2 date picker) 
        -   Time Range limit should have placeholder with default value as "AM"
        -   In Preview Mode, dates outside listed range shall be disabled 
    -   "Min/Max Date/Time" from another field \[MVP3\] 
-   Validation Rule \< see Validation Rule\> 
    -   Condition: 
        -   "After/Before the Date", "Equal to", 
            -   Shall end with either "Field" or "Value" 
                -   E.g. 
                    -   After the Date Field 
                    -   After the Date Value 
                    -   Before the Date Field 
                    -   ... 
        -   **Value**: 
            -   If ends in "Field", shall show dropdown pointing to other questions 
        -   If condition ends in "Value", Date Picker (matching the format of the date being tested) 
    -   Condition: In the Past (from the day of input) 
        -   Shall disable Value field 
    -   Condition: In the Future (from the day of input) 
        -   Shall disable Value field
-   Design Question Styles 
    -   \<To be filled\> 
    
## Rating Scale [MVP4] 
![Rating](./img/questionType_ratingScale.png)
-   Rating scale difference to Slider is that it is EXCLUSIVE of other options
-   There shall only no Scale Type choice. 
-   Standard Settings 
    -   "Node Count" slider 
        -   Shall accept a numeric input to display count of nodes 
    -   "Allow .5 ratings" toggle 
        -   Shall allow user to select rating with .5 specific
            capabilities 
    -   "Display selected rating" toggle 
        -   shall show the value slid towards in a label below 
        ![Rating](./img/questionType_ratingScale2.png)
-   "Show labels" toggle 
    -   Shall provide 3 label text with predefined positions 
        -   Left 
        -   Center 
        -   Right 
    -   Labels shall disable when "Show labels" are unticked 
-   "Image Choices" [MVP2] 
    -   Shall provide an image visual on top of the scale question 
-   Validation Rule \< see Validation Rule\> 
    -   \<none\> 
-   Design Question Styles 
    -   Rating Type 
        -   Star 
        -   Numbered (0....10) 
        -   Moon 
        -   Radio Button 
        -   ... 

## Slider
-   Slider difference to Rating Scale is that it is INCLUSIVE of lesser options
-   There shall be 5 *Input Type* choices: 
    * “Basic”
        ![Slider](./img/questionType_slider.png)
        *  shall be the default selected option
        *  “Range”
            *  Shall have 2 text fields to define start/end of the range (accepts numeric only)
        * “Label”
            *  Shall have text fields to indicate left and right labels of the slider
            *  Both has to be filled up, error shall show if one/two is missing
    * “Basic + Label + Value Input”
        *  Display shall show real time value of the user selected slider value
        *  User may modify the Value in the Display to which the slider node shall adjust positioning to
        *  User may add Prefix/Postfix label to this real time value
    * "Scales"
        * "Range" shall only accept 0-10 as the maximum
        *  Message under "to" field to show: "No more than 10"
    * "Custom Values"
        ![Slider Label](./img/questionType_sliderLabel.png)
        * User can add labels with "+ Choice"
        * In Display, Choices should be evenly spaced
-   Interaction:
    -   Mouse hover over Type selection shall show a non transparent preview of the style [#152](http://192.168.222.60:8001/eab-cafe/Cafe/issues/152)
-   "Label" 
    -   Shall have text fields to indicate left and right labels of the
        slider 
    -   Both has to be filled up, error shall show if one is missing 
    -   When left empty, label shall not be seen in Display Engine 
-   "Show real time user input" (checkbox) 
    -   When checked, Display shall show real time value of the user
        selected slider value 
    -   User may modify the Value in the Display to which the slider
        node shall adjust positioning to 
    -   User may add Prefix/Postfix label to this real time value 
-   "Allow decimals" checkbox [MVP2] 
    -   Shall show choices in Design/Preview View by decimals (2 places
        only) 
-   "Custom" 
    -   Rows 
        -   Number of Nodes shall be settable starting from at least 2
            to 20 options 
    -   "Node Labels" textboxes 
        -   Labels shall be customizable for each step of the node 
        -   Image Upload \[MVP 2\] 
-   Standard Settings 
    -   "Display slider value" toggle 
        -   shall show the value slid towards in a textbox/label 
        -   May be positioned on top/right/left/bottom (design mode) 
        -   When "use 2 slider" is ticked, show 2 value textbox/labels 
    -   "Use 2 sliders" toggle [MVP2] 
-   In Design Mode/Display Engine:
    -   When moving slider knob for answer selection, selected value shall be visible on top the knob[#152](http://192.168.222.60:8001/eab-cafe/Cafe/issues/152)
-   When ticked, left and right slider shall be available for the user 
-   When unticked, only right slider shall be available 
<!-- --> 
-   Validation Rule \< see Validation Rule\> 
    -   <none> 
-   Design Question Styles 
    -   <To be filled> 

## Toggle 
![Toggle](./img/questionType_toggle.png)
-   There shall only be no *Toggle Type* choice 
-   Standard Question Type Settings 
    -   "Toggle Step" 
        -   Shall accept 2 options to display count of nodes 
            -   "Two" 
                -   Shall be the default selected option (red highlight) 
                -   Shall have 2 label input areas 
                -   E.g. 
                    -   Yes/No 
                    -   Male/Female 
                    ![Toggle](./img/questionType_toggle2.png)
-   "Three" 
    -   Shall have 3 label input areas 
    -   E.g. 
        -   Week/month/year 
        -   No/NA/YES 
        ![Toggle](./img/questionType_toggle3.png)
-   "Label" 
    -   Label rows shall match the value selected in "Toggle Step" 
    -   Label rows shall be numbered starting from 1 
    -   If label is left empty, an error shall come up saying: 
        -   "A toggle label is left empty. Please fill in the required fields." 
        -   Missing fields should highlight in red 
-   "Image Choices" [MVP2] 
    -   Shall allow user to upload image 
    -   Image uploaded shall be removeable with an intuitive "x" button beside Upload button
    -   Display of image shall appear under Question and above the toggle
-   In Design Mode/Display Engine:
    -   Default option selected shall be the first choice (#1) in Edit Mode 
    -   Color of stepper shall follow Design Mode\>Header Background color
-   Validation Rule \< see Validation Rule\> 
    -   \<none\> 
-   Design Question Styles 
    -   \<To be filled\> 

## Plain Text
-   “Type Label Here” shall be seen instead of the “Input Question Here”
-   Label Field shall show plain text without user input
    -   "#" annotation shall show a "suggestions list" to other questions to be part of the label
    -   User shall be able to remove the question reference by backspacing
-   There shall be a bar for adding style within Edit mode for Plain Text Questions
-   In Design Mode/Display Engine:
    -   Plain text shall show as written/styled in Edit Mode
    -   All the "#" references in the Plain Text shall be evaluated/replaced with the answer when displaying the Plain Text
    -   Displaying the Plain Text should appear without border, and only text [#154](http://192.168.222.60:8001/eab-cafe/Cafe/issues/154)

## Calculated Field 
-   "Type Label Here" shall be seen instead of the "Input Question Here" 
    -   Label can be left empty, in this case the Value will be the only
        visible element 
-   There shall be 2 *Calculation Field* choices: 
    ### Basic Calculation
    ![Calculated Field Basic](img/questionType_calculatedField_basic.png)
    -   There shall be a 2 dropdown choices for each condition
        -  A dropdown for question 
            -   Choices shall be other Questions within the form
            -   Questions available in the list only appears when input value is “numeric”
        -  A Button Group Operator:
            -   "Add" button
            -   "Subtract" button
    -   "+" Button at the right most only when it is the last row
    -   Interaction:
        -  When a new Calculation Field is created, these shall show:
            -  Dropdown for question
            -  "+" Button
            ![Calculated Field Basic](img/questionType_calculatedField_1row.png)
        -  When "+" Button is clicked:
            -  "Add","Subtract" button shall be visible
            -  New row shall be added with:
                -  Dropdown for question
                -  "+" Button in this next column
            ![Calculated Field Basic](img/questionType_calculatedField_2rows.png)
        -  When a row is hovered
            - "x" button shall appear on the right most side
            - Moveable indicator shall appear on the left most side
        -  When "x" button is clicked, row shall be deleted
        -  There shall be no Moving Indicator
    -   Exceptions: 
        -   Question shall auto exclude from calculation if deleted. 
            -   If Q1+Q2-Q3 in Calculated field. User deletes Q2. Then Calculated field becomes Q1+Q3 only 
        -   Question shall auto exclude from calculation if "Format" is changed to other than numeric. 
            -   If Q1-Q2+Q3 in Calculated field. User change Format of Q3 to "Any". Then Calculated field becomes Q1-Q2 only. 

    ### Advanced Calculation
    -   There shall be a text field to type in formulas directly 
        -   One row by default 
        -   Expand to next row if exceed width, not more than 5
            lines 
    -   Arithmetic operations shall support Excel Formula format 
    -   Arithmetic operations can retrieve from existing questions
        using the key: "#" 
    -   Interaction:
        -   When Formula is being written, the following behavior is the EDIT BEHAVIOR:
            -   "Check Formula" button enables
            -   Help Text: "Try typing "#" to..."
        -   When "Check Formula" button is clicked:
            -   And Formula is correct
                -    "Check Formula" button disables
                -    "Check" icon shows on the text field
            -   And Formula is incorrect
                -    "Check Formula" button disables
                -    "Exclamation" icon shows on the text field
                -    Error message reads: "Oops somethings wrong with your formula"
                -    Cursor shall stay on the text field
        -   When coming from an Incorrect formula, any edit on the text field, the following behavior occurs:
            -   Remove "Exclamation" icon and error message
            -   EDIT BEHAVIOR is in effect
        -   When Formula is being written, and user clicks outside the text field/"Check Formula" button
            -   Popup message shows with the message: "Are you sure? Leaving will cause data loss"
            -   "Yes, leave" - when clicked will delete the formula and disable "Check Formula"
            -   "No" - when clicked will bring your cursor back to text field and EDIT BEHAVIOR will be in effect
    
    ?> Example implementation:
        * 1+2+3
        * SUM(1,2)
        * (@Q1+@Q2)+@Q3
        * etc.

    Test Cases:
    1. Respect to PEMDAS
       * Q1 + Q2 * Q3 - Q4 / Q5
        
        Ex:
          * 1 + 2 * 3 - 4 / 2 = **5**
    2. Parenthesis and negatives
        * （Q1 + Q2 * (Q3 - Q4)） / Q5

        Ex:
        * （1 + 2 * (3 - 4)） / 2 = **-0.5**
        
    3. Calculate for Future Value in 15 years with this manual formula
       * Q1 * (1 + Q2)^15
    
        where
        * Q1 = What is your starting capital?
        * Q2 = How many percent interest a year?

        Ex:
        * 9,000 * (1 + 0.045) ^ 15 = **17,417.54**
    4. Calculate for Future Value in 15 years with this Excel formula
        * FV(Q2, 15, 0, -Q1)
        
        where
        * Q1 = What is your starting capital?
        * Q2 = How many percent interest a year?

        Ex:
        * FV(0.045,15, 0, -9000) = **17,417.54**

    Sample source: 
        * https://github.com/handsontable/formula-parser
        * https://www.npmjs.com/package/excel-formula

-   There shall be a Prefix and Postfix field (text field) 
    -   Prefix -- adds text before the calculated result 
    -   Postfix -- adds text after the calculated result 
-   Standard Settings 
    -   "Retrieve points" checkbox [MVP2] 
        -   This shall enable only Multiple Choice questions with
            "points" in the dropdown value fields 
        -   Help Text: 
            -   "You will be able to sum total answers based on their
                points assigned. Only applicable to Multiple Choice and
                Dropdown Types." 
-   In Design Mode/Display Engine:
    -   shall carry out the calculation and show the actual arithmetic result 
    -   shall show Prefix and Postfix as defined 
-   Validation Rule \< see Validation Rule\> 
    -   <none> 
-   Design Question Styles 
    -   <To be filled>

## Common Components -- List Handling

### I want to add an option to the list
-   A "+ Choices" shall be visible at the end of the answers/options
    list
-   Clicking on the "+ Choice"
    -   shall transform the row into an editable text field with
        placeholder text "Option \<\#\>". Cursor should select the whole
        text and be editable
    -   shall add a new row with "add option" at the end of the list
-   Placeholder for option rows when empty according to Question Type:
    -   Multiple Choice: "Type choice here..."
    -   Dropdown: "Type option here..."
    -   Toggle: "Type choice here..."

### I want to have control over options in a list
-   Pressing the "Enter" key while editing an option shall add an option as if clicking "+ choice" automatically [166](http://192.168.222.60:8001/eab-cafe/Cafe/issues/166)
-   Pressing the "Up/Down/Tab" key while editing an option shall navigate around the options
-   Pressing "Shift+Enter" key while editing an option shall add new line in the option, with max of 3 visible rows. Design mode shall display as multiple lines with no line limit.
-   There shall be a "Delete" button visible at the end of each option
    -   Deleting an option shall proceed w/o prompt
-   There shall be a Moveable Indicator when I hover over each option
    -   Moving the option shall only move the option within the question