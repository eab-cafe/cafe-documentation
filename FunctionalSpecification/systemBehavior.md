## Question - Reorganization Checking

### I want to have checking when reorganizing questions/sections/page (Q/S/P)
-   This error shall appear when reorganizing Q/S/P that will break
    Visibility Rules order
    -   "Moving this \<question/section/page\> will break the visibility
        rule between \<q1/s1/p1\> and \<q2/s2/p2\> (can be more than
        one). Are you sure you want to move this?"
-   This error shall appear when reorganizing Q/S/P that will break
    Error Message Dependency
    -   "Moving this \<question/section/page\> will break the dependency
        of the Error Message logic in \<q1/s1/p1\> to \<q1/s1/p1\>. Are
        you sure you want to move this?"
-   This error shall appear when reorganizing Q/S/P that will break
    Derived field order
    -   "Moving this \<question/section/page\> will break the dependency
        of the derived label \<q1/s1/p1\> from \<q1/s1/p1\> (can be more
        than one). Are you sure you want to move this?"
-   This error shall appear when reorganizing Q/S/P that will break
    Reference Field order
    -   "Moving this \<question/section/page\> will break a Referenced
        value in \<q1/s1/p1\> from \<q1/s1/p1\>. Are you sure you want
        to move this?"

## Navigation Behaviors
-   I want to be notified when I am navigating off page with pending
    changes on my form. "Are you sure you want to leave this page? Any
    changes will be disregarded. Please save your form"
    -   Example:
        -   Navigating from Edit/Design/Flowchart mode into Forms.
            Warning should pop up
        -   Closing the window/tab
-   I want to be shown a big placeholder image when huge data is being
    loaded and takes time

## Design Mode Navigation
-   "Tab" key to move from questionn to question
-   "Arrow" keys to move from cursor between answers
-   "Enter" key to accept the answer chosen by the "arrow" key
-   "Esc" close/cancel any popup

For all popup with confirmation buttons, let us use this format.

## Pop Up Confirmation Windows
* Text should be in 2 rows. [#28](http://192.168.222.60:8001/eab-cafe/Cafe/issues/28)
  * First row bold
  * Second row with the name of the form/page/section being deleted
  * Text to be written depends on the confirmation needed
* Call to action Buttons (All buttons are right aligned)
   * Cancel button:
      * left position
      * no border
      * black text
   * Accept Button:
      * right popsition
      * pink background of button
      * white text
![System Behavior Popups](./img/systemBehavior_popups.png)
Affected Areas in summary:
1. Publish coonfirmation
  * Cancel button: "Cancel"
  * Accept Button: "Publish Form"
2. Delete Page/section/question confirmation
  * Cancel button: "Cancel"
  * Accept Button: "Delete Question" / "Delete Page" / "Delete Section"
3. Delete Form in Forms list
  * Cancel button: "Cancel"
  * Accept Button: "Delete Form"
4. Visibility rule confirmation:
  * Cancel button: "Cancel"
  * Accept Button: "Apply"