As a user, I want to be able to login to my Café Account
--------------------------------------------------------
### As a user, I want to be able to login through a Login Page
-   There shall be 2 fields for input
    -   User name
    -   Password
-   "Login" Button shall redirect user to the Home page or raise errors when credentials are wrong
-   There shall be a description to welcome the user to the Café Forms
    -   "Welcome to Café Forms."
### As a user, I want to be able to reset my password
-   On the login page, a "Forgot Password" shall be visible that sends an email to the registered email address for password reset
    -   When clicked, a notification shall show:
        -   "Password reset link has been sent to your registered
            address"
-   Format of the email should contain a Password Reset Link that
    redirects to Café Forms Password Reset Link page
-   Password Reset Link page shall have 2 fields for input
    -   New Password
    -   Retype New Password
-   "Save" button shall prompt:
    -   "Password successfully changed!"
    -   Closing this prompt shall redirect user back to Login Page