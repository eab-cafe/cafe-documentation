## Question Settings
-   The Page/Section/Question name should show at the top most portion of the Question Settings Editor
    -   E.g.
        <Page/Section/Question\> \<name of page\>

## Visibility rules on Pages/Sections/Questions
![Visibility](./img/visibility.png)
-   Visibility Tab shall be seen under Section/Question Settings
        -   When clicked, Visibility Rule Editor shall show
-   There shall be 2 modes for Visibility Rules, switchable by a
    checkbox "show advanced rules"
    -   Basic
    -   Advanced

## Add Visibility rule
-   An add button shall be present on every rule row
    -   A Visibility Rule is composed of 3 parts
        1. **Field**
        2. **Condition** 
        3. **Value**
-   Interaction:
    -   When a new rule is added
        -    **Field** = active state
        -    **Condition** = disabled/grayed out state
        -    **Value** = disabled/grayed out state
    -   When **Field** is filled in with a value
        -    **Condition** = active state
    -   When **Condition** is filled in with value
        -    **Value** = active state ONLY when condition requires a value comparison
-   Visibility Rule can be applied to Pages/Section/Questions as further defined below

### Visibility Rule: Question
#### Field - Condition - Value Relationship Table
| **Field**                                                  | **Conditions to show?**       | **Value Input Type** |
| ---------------------------------------------------------- | ---------------------------------- | -------------------- |
| All Questions                                              | Is Visible                         | none (disabled)      |
| All Questions                                              | Has Answer                         | none (disabled)      |
| All Questions                                              | Has No Answer                      | none (disabled)      |
| **Question Types**                                                                                                     |
| Text Input                                                 | Is/IsNot Equals                    | Text Field           |
| Text Input(Numeric); Calculated Fields; SLDR-Numeric       | Is/IsNot Equals, \>, \>=, \<, \<=  | Text Field (numeric) |
| DD; MC; TGL; SLDR-Custom; DD Cascaded, Conditional Choices | Is/IsNot Equals                    | Dropdown Field       |
| Date or Time                                               | Is/IsNot Equals \>, \>=, \<, \<=   | Date or Time Field   |

-   Field: `<refers to other questions>`
    -   Show as dropdown field to select other questions
    -   Placeholder: "Select a question..."
-   Conditions to show: `<Compares Field to Value>`
    -   All Questions have the ff:
        -   `Is Visible`
        -   `Has Answer`
        -   `Has No Answer`
        -   `Is/IsNot Equals`
    -   Show `<`/`>`/`<=`/`>=` only when
        -   Field selected is date or numeric
        -   Field selected choices are numeric in nature
-   Value: `<Value to be compared to>`
    -   Disable Value selection when condition is
        -   `Is Visible`
        -   `Has Answer`
        -   `Has No Answer`
    -   `Text Field` Input Type for Value only when Field selected is Text in nature
    -   `Text Field (numeric)` Input Type for Value only when Field selected is Numeric in nature
    -   `Dropdown` Input Type for Value only when Field selected is defined as options
    -   `Date or Time` Input Type for Value only when Field selected is Date/Time in nature
     

### Visibility Rule: Section
-   Field/Condition/Value rules shall extend from the base Question Visibility Column Rule
-   Section Visibility Rule additions:

| **Field**    | **What conditions to show?** | **Value Input Type** |
| ------------ | ---------------------------- | -------------------- |
| All Sections | Is Visible                   | none (disabled)      |

### Visibility Rule: Page
-   Field/Condition/Value combination shall extend from the base Section Visibility Column Rule
-   Page Visibility Rule additions:

| **Field** | **What conditions to show?** | **Value Input Type** |
| --------- | ---------------------------- | -------------------- |
| All Pages | Is Visible                   | none (disabled)      |

## Delete Visibility rule
-   A delete button shall be present on every rule row

## Basic Visibility Rules
-   Basic Visibility Rules shall have the label:
    -   "Show only when \<logical operator\> conditions are met"
    -   Where \<logical operator\> is a a dropdown choice of the logical operator to be used that affects all P/S/Q rules defined. Choices are:
        -   All
        -   Any
-   Switching from Advanced to Basic shall prompt an warning with "yes/no" action call
    -   "Switching from Advanced will reset matching to 'all' conditions. Are you sure you want to continue?"

## Advanced Visibility Rules [MVP2]
![Visibility Advanced](./img/visibility_advanced.png)
-   Under the Page/Section/Question name, there shall be a label:
    -   "Show Page/Section/Question only when:"
-   There shall be an Open and Close bracket pair buttons surrounding each Visibility Rule
    -   When Open brackets are selected, there shall be a matching close bracket; and vice versa.
    -   Missing matching brackets shall show an error when "saved"
        -   "Missing open/closing bracket. Kindly recheck the Visibility Rules"
-   There shall be an "and/or" toggle in between each Visibility Rule
    -   "and" shall be the default selected toggle

## Actions
-   I want to see a "Save" button that when clicked will save and fold
    out the Visibility Rules Editor
    -   Shall show the error defined above when there are unmatched
        brackets
-   I want to see a "Cancel" button that when clicked will cancel and
    fold out the Visibility Rules popover

## Special Visibility Rule - RULES
-   Visibility rules on the 2nd and up tiers should apply
    -   For Instance 
        1.   Create 3 questions
        2.   Add rules: Show 2nd question based on 1st, show 3rd based on 2nd
        3.   Go to design. Answer all to show 1-3 questions
        4.   Change 1st answer to hide 2nd question, 3rd question should hide if parent dependency is hidden.
-   Visibility Rules has to apply on different pages
    -   For Instance:
        1.   Q1 in page 1
        2.   Q2 is in page 2 and visibilty depends on page 1 answer
        3.   Q2 should be affected by q1 answer

## Integrity preservation of Visibility Rules
-   When a Visibility Rule Value Reference (Dropdown/MultipleChoice/Toggle/etc) is renamed, existing rules with that Value reference SHALL be renamed without a prompt
-   When a Question w/ Option (Dropdown/MultipleChoice/etc) is deleted, and it has existing Visibility Rule Value Reference(s), there SHALL be a prompt before deletion:
    -   "Deleting this value will remove all Rules related to this value. Are you sure?"
    -   No prompt if no Rule Reference
-   When a Question Type is changed, and it has existing Visibility Rule Value Reference(s), there SHALL be a prompt before change: [MVP2]
    -   "Changing the Question Type will remove all Rules related to this value. Are you sure?"
-   When a Question is deleted, and it has existing Visibility Rule Value Reference(s), there SHALL be a prompt before deletion: [MVP2]
    -   "Deleting this question will remove all Rules related to this value. Are you sure?"
    -   no prompt if no Rule Reference