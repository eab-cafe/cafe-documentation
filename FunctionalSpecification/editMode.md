## Edit Mode Header
-   I want an Edit mode icon to fold into the upper Bar when selected
-   I want to hover over Design mode icon and be shown other Modes to
    select from
-   I want to see the Edit mode as Default once a Form is opened for
    edit
-   I want to see all Sections unfolded and all Questions in Summary
    View

## Question Preview panel in real time
![Preview](./img/editMode_previewPanel.png)
-   The *Question Preview Panel* shall only be visible in Edit Mode with
    a fixed width
-   The *Question Preview Panel* shall display at the question in focus
    in Edit Panel
-   Changes to a question shall show live *Question Preview Panel*
    updates with design:
    -   Font style, color, size
    -   Question alignment
    -   Background/Form Color
-   The *Question Preview Panel* shall be hidden in small screen sizes

### I want to be able to hide the Question Preview Panel anytime
-   The *Question Preview Panel* shall contain a hide/show icon on the
    right side
-   The *Form Edit Panel* centers intuitively with/without the *Question
    Preview Panel*

### Aaccess Full Screen Preview
-   Clicking on Header Actions - Preview Mode icon shall lead me to preview mode \<see Preview Mode -- Design engine\>
