## Design Mode Header
![Design Menu](./img/designMode_DesignMenu.png)
-   I want a Design mode icon to fold into the upper Bar when selected
-   I want to hover over Design mode icon and be shown other Modes to
    select from
-   Clicking on Header Actions - Full Screen Preview icon shall lead me
    to full screen mode \<see Full Screen \>
-   I want to click on the Design Mode button and see my Form in Design
    View to replace the whole *Form Control Panel*
-   User shall be able to navigate the form in Design View
-   User shall be able to move Pages/Sections within the *Pages Outline
    Panel*
-   Form action sshall be disabled/invisible
    in Design Mode
    -  Add/Move/Edit/Delete Page
    -  Move/Edit/Delete Section

## Design Settings Panel
-   A *Design Settings sidebar* shall be visible on the right side by
    default
-   User shall be able to close this sidebar and Design Form centered
-   A notice at the bottom of *Design Settings sidebar* shall be seen
    saying
    -   "Changes here affects the whole form."

### Form Themes Settings
-   Form Themes dropdown shall have the following options
    -   Select from available themes
    -   Search available themes
    -   Filters: color, categories...
    -   CSS? [MVP2]
    ![Themes](./img/designMode_Themes.png)
-   I want to select a Theme and be prompted the below message:
    -   *"Are you sure you would like "Create a new theme"/"Load in
        \<name of available theme\>"/"import theme from \<Other Form
        Name\>"? Doing so will overwrite your current theme changes."*
    -   Prompt only when there design changes to the previous loaded
        theme
-   I want to click "yes" to above and see the sub-properties be
    overwritten
-   I want to see the following submenu in my *Design Settings sidebar*
    -   Personalize Theme
    -   Layout Properties
    -   Question Styles
    -   Navigation Properties
    -   Error Messages

### Personalize Theme
![Personalize Theme](./img/designMode_PersonalizeTheme.png)
-   I want to be able to choose a Header Image or Color and choose to
    "show as logo"
    -   If Header Image is picked and "show as logo" is checked, color
        will apply as header background color
    -   If Header image is picked and "show as logo" is not checked,
        color will not show and Image will be applied as header
        background
-   I want to design Page Background Color
![Background Color](./img/designMode_BackgroundColor.png)
-   I want to have a Default Font/Size/Style/Colour for
    -   With choices of Title, Description, Pages, Sections, Questions,
        Answers
        ![Font Style](./img/designMode_FontStyle.png)

### Layout Properties
![Layout](./img/designMode_Layout.png)
-   I want to modify Form Size
    -   There shall be a checkbox to "Stretch to screen width" so it shows as full screen
-   I want to choose a Section Ordinal Type for all sections of the Form
    -   Numbered
        -   All sections at the start of each Page will be numbered 1,2,3,\...
    -   Lettered
        -   All sections at the start of each Page will be lettered A,B,C,...
-   I want to choose a Question Ordinal Type for all questions of the Form
    -   Numbered
        -   All questions at the start of each Section will be numbered 1,2,3,\...
    -   Lettered
        -   All questions at the start of each Section will be lettered A,B,C,...
-   I want to set the Question Alignment of all the questions of the
    form
    -   Left
        -   Simplest. Everything align to the left
            -   e.g.
            ![Left Align](./img/designMode_LeftAlign.png)
    -   Center
        -   If Question Positioning is on Top, Center will center based
            Question and Field independently on their respective column
            center line
            -   E.g.
            ![Center Align](./img/designMode_CenterAlign.png)
-   I want to be able to set Question Positioning with respect to the
    Question Field
    -   Top
        -   Question will appear on the top of the Field
    -   Left
        -   Question will appear on the left of the Field
    -   Inside [MVP2]

### Question Styles
![Question Style](./img/designMode_QuestionStyle.png)
-   I want to choose Individual Question Styles(Text, Dropdown, etc.)
    only for existing question types in the form
    -   Styles for Question Types not contained within the current form
        being designed should be hidden
        ![Text Input](./img/designMode_QuestionStyle_TextInput.png)
-   I want to be able to purchase more styles when I want to

### Navigation Properties
    ![Text Input](./img/designMode_Navigation.png)
-   I want to choose a Progress Bar Style for page navigation
    -   None 
        -   "Next" and "Back" navigation buttons shall be present
    -   Side tabs [MVP2]
        -   "Back" button shall not be present
    -   Upper tabs/Stepper
        ![Stepper](./img/designMode_Navigation_Stepper.png)
        -   Clicking on each tab shall jump to that page
        -   Tab/Step shall show in completed state once Page is
            completely filled up (Mandatory, validation) passes
        -   Color of stepper shall follow Header Background color
    -   Percentage [MVP2]
        ![Percentage](./img/designMode_Navigation_Percentage.png)
\*\*\* In any option - "Next" button shall be present in all but the
last page
\*\*\* In any option - "Submit button" will be found in the last page
only
-   I want to be able to "Show Page Completion" on the progress bar
    style
    -   When the page validation passes, a check mark shall light up in
        the progress bar
-   I want to be able to "Show Page Number" on the progress bar style
    -   Shall show the number on the selected Progress bar style
-   I want to select how Page "Next/Submit" buttons would look like and
    the positioning
    -   Next/Submit text and color
    -   Center/Left/Right/SpaceBetween(Back Button-left aligned; Next
        Button-right aligned)
    -   Sticky or not
        -   Buttons shall have a bar stretched edge to edge behind with
            a little opacity for good design
        -   Info: "Checking this will always show Next/Submit button at
            the bottom of your browser window."
-   I want to specify if the form will "Allow page to be skipped before
    going to the next section"
    -   Info: "If you allow page to be skipped, mandatory questions in
        the page will not show error messages"
-   I want to specify if a "save" button exists, looks and positioning
    [MVP2]
    -   Useful for sections that loose data when navigated away

### Sub-category: Error Messages
    ![Error Messages](./img/designMode_ErrorMessage.png)
-   I want to have a switch for Showing Error Messages
    -   When I have this disabled, everything under Design Sub-category:
        Error Messages will be disabled
-   I want to design the Error Message position
    -   Top
        -   Show error on top of Field if Question Position is Left
        -   Show error beside Question if Question Position is Top
    -   Below - Show error below Field regardless of Question Position
    -   Right - Show error on the right of the Field regardless of
        Question Position
-   I want to specify whether or not Error in fields should *Show error
    upon page load*
    -   When a page comes in to page, whether upon load or click
        next/back, all error validations will run and show errors if any
        exist
-   I want to set *Error Prompt Frequency*
    -   On page redirect
        -   Validate all fields only when page is being redirected
            -   i.e. "Next"/"Back"/"Submit" is clicked
    -   Upon Answering
        -   Validate field everytime focus removed from a field
            -   i.e. onBlur

## Interaction

### Move questions around a section
    ![Section Question Movement](./img/designMode_DesignDetail_SectionQuestionMovement.png)
-   I want to hover a Question and see
    -   all my questions in a Section Grid Box
    -   Open Hand -- to show that you can move the question within the
        section
    -   Question Indicator (highlight) - surrounding the question so
        that user can see the bounds of the question
    -   "Grid columns"
        -   to set the number of columns from 1-3
        -   Column 1 and 2 choice will give form width of 768px (Design/Display Engine)
        -   Column 3 choice will give form width of 1200px (Design/Display Engine) [MVP2]
            -   When chosen, a notice should show saying: “Notice: Selecting 3 columns will increase form width and may not be optimized for viewing on smaller screens.”
        -   Empty space can be hovered and shall show with a dotted guide. This shall not be removable to avoid chaos “click to remove”
    ![Section Columns](./img/designMode_DesignDetail_SectionColumns.png)
-   I want to rearrange questions only within the Section Grid Box
-   I want to hover over spaces above/below a question to see an
    intuitive "Plus" button
    -   I want to be given a selection to add an "row space" or "row
        separator"
    -   I want this "row space" or "row separator" to cover the whole
        width of the box
    -   I want to see an intuitive "delete" icon to delete this "space"
        or "separator"
    ![Section Separator](./img/designMode_DesignDetail_SectionSeparator.png)
-   I want rearranged questions in Design Mode also be in sync with the
    arrangement in Edit Mode (Render behavior from design mode to edit
    mode: left -\> right -\> top -\> down)
-   Rules of drag and drop within Section Grid Box
    -   Hand cursor
    -   Clicking and dragging will lift the question/space/line and
        follow the cursor when moved
    -   Dragging will show shadow on the possible placement in the
        Section Grid Box
    -   Dragging boxes
        -   Dragging a box to any empty space can be dropped
        -   Dragging a box to "collide" with any occupied space will
            adjust the whole row to give way for dropping above/below
    -   Dragging away the last box in a row will Vertical Compact
        (adjust all the rows below it up/down)
    -   Dragging a "row space"/"row separator" will adjust rows up/down
        to provide placement
    ![Question Detail](./img/designMode_DesignDetail_Question.png)

## Design In closer detail

### Design a Section
    ![Design Section](./img/designMode_DesignDetail_Section.png)
-   I want to hover over a section and see the following responses:
    -   Open Hand -- to show that you can move the section within the
        page
    -   Section Indicator (dotted line) surrounding the section so that
        user can see the bounds of the section
    -   Add custom look - button at the center
    -   see an intuitive highlight/glow on all related settings to the
        Section highlight on the *Design Settings sidebar*
        -   Personalize Theme -- Section Font/Size/Style/Color
        -   Personalize Theme -- Questions Font/Size/Style/Color
        -   Personalize Theme -- Answers Font/Size/Style/Color
-   I want to move my section by just clicking and dragging anywhere on
    the Section [MVP2]
    -   I want drag a Section and see all Sections fold so I can drag
        and reorganize my section easily and quickly
-   I want to hover over "add custom look" and be shown a "Custom Look"
    pop up bar that overrides General Section Settings
    -   I want to change Ordinal, Alignment and color
        -   Info Text: "Adding this will give your section a custom
            look"
        ![Section Ordinal](./img/designMode_DesignDetail_SectionOrdinal.png)
-   I want to be able to delete the custom look of a Section

### Design a Question
-   I want to click on any question and see an intuitive highlight/glow
    on all related settings to the Question highlight on the *Design
    Settings sidebar*
    -   Personalize Theme -- Questions Font/Size/Style/Color
    -   Personalize Theme -- Answers Font/Size/Style/Color
    -   Question Style - \<Question Type currently selected\>