## Form Name and Description
![Form Name](./img/editMode_Name.png)
-   I want to see Name and Description in the *Form Details Section*
-   I want to be able to edit the Name just by clicking on it
    -   Form Name shall NOT be empty
        -   "Untitled Form" shall be assigned automatically when empty name is given
    -   Max rows is 3 rows
-   I want to be able to edit the Description just by clicking on it
    -   Font is smaller than Form Name
    -   Max rows is 5 rows

## Action buttons (Save/Publish)
-   I want to have a Save button in the Form Details Section.

### Save
-   I want to be able to click the "Save" button in any mode
-   I want to see a notification message saying "Form Saved" in the lower right part of the screen
-   I want for "ctrl+s" shortcut to work as save
-   Form saved shall not be published
-   I want to see a "Last Saved on 2019 04 02 10:00am" after I click Save

### Publish 
- I want to have a Publish button in the Form Details Section
-   I want to open up Form Settings once I click "Publish" if Publish settings have not yet been set for this Form
-   Info text: "Please define your publish method. (Public link,
-   Form shall be saved in the background prior to this popup.
* Popup shall have notification text:
> “You are about to publish your form `<form name>` with version `<version #>` to a 
    Public Link URL: `<public link>`
    Webhook server IP: `<webhook ip>`
    Do you want to continue?”
    
    * I want to see a “Yes/No” choice
    *  When Yes is clicked, I want to be shown a “Publish Success” Popup
        * I want to see a Notification/Progress bar(similar to Save Notification) to show successful publishing
        * I want to view the “Published Public Form Link” under Form Settings for User to click/copy

## Back Button

## Form settings (IO Settings)
-   I want to see a Form Settings button close to the Form Name
-   I want to click Form Settings button and see a Form Settings Editor
    to overlay on top of the *Form Control Panel See [Form Settings](http://eab-documentation.eabgitlab.com/cafe/#/FunctionalSpecification/formsettings)

Pages in Outline Panel in Edit/Design/Responses Mode

![Pages Outline Panel](./img/pages_outlinePanel.png)

## Tree List Pages Outline Panel
-   The *Pages Outline Panel* shall appear under my Form Name and
    *contain all my Pages and Sections in a list*
-   The *Pages Outline Panel* shall be scrollable when the Page list
    exceeds the panel height
-   When user clicks on any Page in the list, it shall highlight on the
    selected Page
-   User cursor should be a hand cursor when hovering over *Pages
    Outline Panel*

### Navigation by Click action
-   I want to click on any part of the Page/Section to select it
-   I want to see the *Form Control Panel* to jump to the Page/Selection
    I clicked in Edit Mode/Design Mode/Flowchart Mode

## Undo and Redo
-------------
-   An intuitive Header Action -- Undo and Redo icon shall be visible at all times in the Header Actions Bar
-   When I click undo/redo, user changes shall be undone/redone only by 1 step at a time
-   History shall not be available once moved outside of the form