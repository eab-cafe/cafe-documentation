General Terminologies
=====================

-   Preview Mode -- When the Form is being filled up by a respondent

-   Edit Mode -- A Form can be added with Questions in this mode

-   Design Mode -- A Form can be beautified with font, colors, custom background images here

-   Response Mode -- When a Form is filled, records of the responses can be located here

-   Display Engine -- When a Form is published, the Display Engine is the mechanism to run the Form for respondents

-   Flowchart Mode -- When a Form is being neatly displayed in a
    flowchart

-   Moveable Indicator -- 9 dots icon to show that the component is
    moveable

General Panels
==============

## Menu Panel
![Menu Panel](./img/generalPanel1.png)

## Form Details Panel
![Form Details Panel](./img/generalPanel2.png)

## Pages Outline Panel
![Pages Outline Panel](./img/generalPanel3.png)

## Form Control Panel
![Form Control Panel](./img/generalPanel4.png)

## Form Edit Panel
![Form Edit Panel](./img/generalPanel5.png)

## Preview Panel
![Preview Panel](./img/generalPanel6.png)