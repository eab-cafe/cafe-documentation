## Pages under Pages Outline Panel
-   Pages shall appear in list sequence in the Pages Outline Panel
-   When user clicks on any Page in the list, the Form Edit Panel shall jump to that page
-   User cursor should be a hand cursor when hovering over *Pages Outline Panel*

### Add Page in the Pages Outline Panel
-   An Add Page button shall always be visible
    -   at the very beginning of the *Pages Outline Panel* if my Pages list is empty
    -   at the very end of the Pages list if Pages are listed
    -   "sticky" at the bottom of the page if the *Pages Outline Panel* is scrollable
-   Cursor shall jump to the Page Name automatically when Add Page is clicked in the Pages Outline Panel

### Add Page using "hover" in Pages Outline Panel[MVP2]
-   I want to see an Add symbol fade in between Pages when my mouse "hovers" over that area
-   I want for the Add symbol to fade out when my mouse goes out of "hover" over the area

### Page settings in Pages Outline Panel
-   When I hover over the Page clickable area within the Pages Outline
    Panel, Page settings icon shall "fade in"
-   When I hover over the Page area in the Pages Outline Panel, user shall see the
    following "fade in" in the order of:
    -   An intuitive "Delete" icon
    -   An intuitive "Edit" icon

### Edit Page in the Pages Outline Panel
-   Clicking on the "Edit" icon shall make the Page Name editable and focus the cursor on the Name
-   Pressing "enter" or clicking outside shall accept the Page Name Change
-   Page Name can be empty, and have this placeholder visible
    -   "Untitled Page"

### Delete Pages with prompt
-   Clicking on the "Delete" icon, shall prompt
    -   "Are you sure you want to Delete this Page along with its
        sections? Doing so cannot be reverted."
-   I want to still see my Page if I click "no" on the prompt
-   I want for my Page to be deleted if I click "yes" on the prompt

### Visibility Rules applied to a Page [For discussion]
-   When I click the intuitive "Visibility" icon, user shall see a
    Visibility Rule Editor to overlay on top of the *Form Control Panel
    \<see* *Visibility Rule Editor\>*

### Logical Flag to mark Pages complete
-   Pages in code shall be marked complete when all sections/questions within are
    marked complete in Preview Mode
-   Page shall show a "Page Completed" indicator in Preview Mode
    -   E.g.
        -   When progress bar is activated, nodes shall appear
            green/with check mark
        -   When progress bar is disabled, next/submit button will only
            be enabled when all sections within the page are completed

### I want to Create Custom error messages when Certain errors are evident: (Next on incomplete form, Next on incomplete question, etc) \[MVP3\]
-   ...

### Drag/Drop to Reorganize pages [MVP2]
-   I want to see a Moveable Indicator when I "hover" over the clickable area of the page
-   I want to be able to drag and drop on the Moveable Indicator up/down only within the Pages Outline Panel
    -   The Page (and sections if any) shall follow my drag action 
-   I want to be able to only drop Pages to the top, bottom and between
    Pages . I should not be able to drop in between sections.

### Repeat Pages based on derived field values [MVP3]
-   Pages shall have a setting to repeat based on a the value from other fields
-   When pages are repeated, it shall display as an upper tab by default (can add customization later on)
![Pages Repeating](./img/pages_repeat.png)
