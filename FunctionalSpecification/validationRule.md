### Validation Rules on Questions
![Validation](./img/validation.png)
-   Validation Icon shall be seen for Questions in Edit Mode
-   When clicked, Validation Rule Editor shall show
<!-- -->
-   An add button shall be present for adding Validation Rule
    -   A Validation Rule is composed of a Condition, Value, Error
        Message
-   A delete button shall be visible on hover for any Validation Rule

### Components of a Validation Rule

Summary of Validation Rules among Question Types:
|Question Type                 |Condition (always dropdown)              | Value Input Type |
|------------------------------|-----------------------------------------|----------------------------|
| All Questions                                              | Is Visible                         | none (disabled)      |
| All Questions                                              | Has Answer                         | none (disabled)      |
| All Questions                                              | Has No Answer                      | none (disabled)      |
| **Question Types**                                                                                                     |
| Text Input                                                 | Is/IsNot Equals                    | Text Field/DD other qtn           |
| Text Input(Numeric); Calculated Fields; SLDR-Numeric       | Is/IsNot Equals, \>, \>=, \<, \<=  | Text Field (numeric)/DD other qt |
| DD; MC; TGL; SLDR-Custom; DD Cascaded, Conditional Choices | Is/IsNot Equals                    | Dropdown Field/DD other qt       |
| Date or Time                                               | Is/IsNot Equals \>, \>=, \<, \<=   | Date or Time Field/DD other date qt   |

-   Condition: (dropdown)
    -   Contains the conditions
        -   "refer to individual question type"
<!-- -->
-   Value:
    -   Input type and options depends
        -   "refer to individual question type"
    -   Shall be disabled until Condition is chosen
    
### Custom error messages based on conditions
-   Each Validation Rule shall have an Error Message field at the beginning
-   When set, errors that are triggered by not satisfying Validation
    Conditions shall show the custom error message
-   When left blank, Error Message to show is a generated statement
    based on the Validation Rules settings