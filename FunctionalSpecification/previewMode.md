## Preview Mode
![Preview](./img/displayEngine.png)
-   An intuitive Header Action -- Preview Mode icon shall be visible at
    all times in the Header Actions Bar
-   Clicking Preview Mode shall bring the form to Full screen (no
    header, no right/left panel, no Home Panel)
-   A "back" button shall redirect the user back to the previous Mode

### Navigation options within Full Screen Preview
-   A navigation icon shall be present to open up the Pages Outline
    Panel
-   Selecting a page/section in the Pages Outline Panel shall bring the
    Live Preview to the appropriate page/section
    -   Prompt on mandatory questions
        -   "Mandatory questions about to be skipped, doing so will
            disable errors. You will not be able to test Validation
            Rules. Do you want to continue?

### I want to be able to make inputs on the form in Preview Mode
-   Questions shall be interactable in Preview Mode as a respondent would
-   A "next" button shall be visible at the end of every page
-   A "back" button shall be visible only when page is not upper/side tabbed
-   A "submit" button shall be visible only at the end of the form
-   Before "submit", all navigations shall retain inputs unless browser is closed

### I want to be able to Submit to raise errors where applicable
-   I want to be shown an error page when ***Input Data*** payload is
    missing when Form is first triggered
-   I want to be presented with validation errors depending on
    'live'/'upon submit' Error Message settings
-   When errors are visible, the page shall scroll to the first error from the top

### I want Submit to give me a satisfying \"Completed Page\" for good UX \[MVP2\]
-   When a Section/Page is completed, there shall be indicators
    representing it
-   I want the *Pages Outline Panel* to highlight Pages/Section as I move along
-   I want to see a "Completed Page" with a "Start Over" button when testing
