As a User, I want Sections so that I can organize my Questions
--------------------------------------------------------------

![Sections Panel](./img/sections_panel.png)
## Sections under Pages Outline Panel
-   *Sections within Pages shall appear indented like a tree structure in the Pages Outline Panel*
-   When user clicks on any Section in the list, it shall highlight on the selected Section.
    -   Selected section should scroll the Form Edit Panel to that section
-   User cursor should be a hand cursor when hovering over *Pages Outline Panel*

### "hover" actions in Pages Outline Panel
-   I want to see an "intuitive" Moveable indicator for every Section I "hover" over in the Pages Outline Panel
-   I shall be able to move this section with this icon

## Add Section
-   I want to see an "Add Section" at the end of the existing Sections in the *Form Edit Panel*
-   I want my cursor to go jump to the Section Name automatically when I click Add Section in the *Form Edit Panel*

## Section Actions in Form Edit Panel
-   I want to see a "..." icon in the Section when it is folded/collapsed
-   The following "intuitive" icons replace the "..." icon when I "hover" over the Section when folded:
    -   Edit icon
    -   Settings icon
    -   Copy icon
    -   Delete icon
    -   Expand icon
-   I want to see the following icons always visible when a Section is expanded:
    -   Edit icon
    -   Settings icon
    -   Copy icon
    -   Delete icon
    -   Collapse icon

### Edit Section
-   I want to edit the Section Name when I click the "edit" button
-   Section Name can be empty, and have this placeholder visible
    -   "Untitled"

### Section Settings
-   I want to click the 'settings' icon and see the Section Settings page come up

### Copy Section
-   I want to click the copy icon and find the Whole Section copied directly below the current Section
-   Copying a section shall also copy all questions within 
-   I want to stay locked with the Section selected after I copy that section

### Delete Section
-   I want to be prompted "Are you sure you want to delete this section
    along with its questions? Doing so cannot be reverted."
-   I want to still see my Section if I click "no" on the prompt
-   I want for my Section to be deleted if I click "yes" on the prompt

### Collapse/Expand Section
-   I want to collapse/expand a section when I click on the collapse/expand icon
-   I want to collapse/expand a section when I click on the blank space in a section
-   A Question Total count shall be visible beside the Section Name collapsed/expanded
-   User may expand more than one section at a time

## Collapse/Expand All Sections [MVP2]
-   I want to see a Collapse/Expand All button sticky to the top of the page accessible anytime I need it
-   Collapse All shall fold all Sections within the page
-   Expand All shall expand all Sections within the page

## Logical Flag to mark Sections as complete if All mandatory questions within are all answered [MVP2]
-   Sections in code shall be marked complete when all mandatory questions are answered in Preview Mode

### Reorganize Sections using Drag and Drop within the Page [MVP2]
-   I want to see a Moveable Indicator when I "hover" over the whole area of the Section
-   I want to be able to drag and drop on the Moveable Indicator up/down only within the *Form Edit Panel*
-   I want all sections to collapse when I am dragging a section for quicker drop
-   I want to be able to see the Section (and questions if any) follow me while I drag to relocate
-   I want to be able to only drop Sections to the top, bottom and between Sections only. I should not be able to drop in between questions

### Reorganize Sections using Drag and Drop in Pages Outline Panel [MVP2]
-   I want to be able to drag and drop Sections on the Moveable Indicator extended up to the *Pages Outline Panel*
-   I want to see Pages get a highlighted Box while I drag on top of them to indicate where my Section will land
-   I want to be able to only drop Sections onto any Page in the *Pages Outline Panel*
-   I want to be able to drag the Section to the top/bottom edge to scroll to a Page that needs scrolling to get to

### Repeat Sections based on derived field values [MVP3]
-   Sections shall have a setting to repeat based on a derived value
    from other fields
-   When sections are repeated, it shall display as an upper tab by
    default (can add customization later on)
-   The upper tab can be ticked or unticked based on the need of the
    client
![Sections Repeating](./img/sections_repeat.png)