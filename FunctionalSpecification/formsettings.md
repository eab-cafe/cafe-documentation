## I want to have Form Settings appear as a pop up
![Form Settings](./img/formSettings.png)
-   I want to see a "Save" button that when clicked will save and go
    back to the previous page
-   I want to see a "Cancel" button that when clicked will cancel and go
    back to the previous state

## I want to have a Sharing Options sticky at the top in Form Settings
-   There shall be a few options available for sharing the form
    -   "Public link"
    -   "Embed"
    -   "Social Media"

## I want to be able to publish the Form
-   For new forms, Public Link shall be generated automatically
-   The Public Link shall be visible as a label
-   The Public Link shall have a copy button next to it to copy the
    public link
-   The Public Link name shall be customizable [MVP2] \[Chargeable\]
    -   There shall be database checking to prevent duplicates in Link
        name
-   Accessing the public link shall open up a New Browser Window and
    show the Full Screen Preview 
-   There shall be a Generate New Link button to duplicate the public
    link
-   The Manage Public Links shall be available and unique to every
    form[MVP2]
    -   The links listed here shall be related to the form being edited

## I want to be able to copy an Embed Code for my form [MVP2]
-   When this category is selected, there shall be a visible text area
    with the corresponding embed target
-   There shall be 4 embed targets available
    -   iFrame
    -   JSCode
    -   Popup
    -   Full HTML

## Quick links to social media to share the form [MVP3]
-   Intuitive icons shall be visible to cater to social media
    -   Facebook
    -   Twitter
    -   Google +

## Form Settings Sub-categories

### General Settings category [MVP2]
-   I want to gather respondents email address
-   Notification Settings here \[MVP4\]

### Access Rights Settings category \[MVP 3\]
-   As an Administrator, I want to be the only one Granted Form Settings
-   As a Maintainer, I want to only see Form Settings for IO Settings
    when my role permits

### Reference ID Settings [MVP2]
![Form Settings Reference ID Wireframe](./img/formSettings_referenceIdWireframe.png)
![formsettings_ReferenceID](img/formsettings_ReferenceID.png)
- Header Message:
    - "Modifying IDs may affect Third-party application(s) that has been previously linked."
-   Help Text (tooltip):
    -   "Reference ID is a customisable tag unique to all Components
        (Page/Section/Question) in a Form. Reference ID allows for
        easier user input identification when extracting data from
        Cafe."
-   ***Reference ID*** shall show in a table format:
    -   "Type" (label -- uneditable)
        -   This field shall contain: Pages/Sections/Questions
        -   Shall be sort capable
    -   "Content" (label - uneditable)
        -   This field shall contain all Pages/Sections/Questions Name
    -   "Reference ID" (label/textfield)
        -   Programmatically, Café Reference ID can be thought of as a Display ID heavily linked to Unique System ID
            -   Reference ID shall be unique within a form.
            -   Exception: Reference ID can be the same with other forms
        -   Reference ID shall be initially generated based on the Form-Page-Section-Question hierarchal relationship
        -   Default state of this field is as a label
        -   Interaction:
            - There shall be an individual "edit" icon that shows upon hover over each ***Reference ID*** row in label state
            ![formsettings_ReferenceID](img/formsettings_ReferenceID_hover.png)
            - When in "Edit" icon is clicked the ***Reference ID*** will change to textfield state.
                ![formsettings_ReferenceID](img/formsettings_ReferenceID_edit.png)
                - 2 icons shall appear beside text field
                    - "Check" icon
                    - "Refresh" icon
                - When the "Check" icon is clicked, and ***Reference ID*** text is unique, ***Reference ID*** is saved and goes back to Default state: label
                - When the "Check" icon is clicked, and ***Reference ID*** text is not unique, ***Reference ID*** is not saved and stays in Textfield state with an error: "Duplicated Reference ID."
                    ![formsettings_ReferenceID](img/formsettings_ReferenceID_duplicate.png)
                    - When the ***Reference ID*** text is modified, the error message shall disappear
                - When the ***Reference ID*** textfield is deleted/empty/contains space characters only, the "Check" icon shall be disabled. Reenable once textfield is filled in with nonspace characters.
                ![formsettings_ReferenceID](img/formsettings_ReferenceID_empty.png)
                - When the "Refresh" icon is clicked, the ***Reference ID*** will be reverted to the previous ***Reference ID***, and textfield goes back to Default state: label
        - Checking for Unique looks in 2 places
            - In Form Settings > ***Reference ID*** List
            - In Form Settings > Workflow Integration > External Data Format
        - When a ***Reference ID*** is updated, all previous Respondents data shall be accessible by this new ***Reference ID***. Old id will be lost.
-   There shall be a search bar for keywords search
    -   Search Keywords should match:
        -   Café Reference ID
        -   Component Name
        -   Component Type

### Workflow Integration Settings - External Data Format [MVP2]
![Form Settings Workflow Integration](./img/formSettings_workflowIntegration.png)
-   Description
    -   "Defines the structure of incoming data utilized by this form."
-   ***External Data Format*** shall be customizable through a table
    with add/edit/delete functions:
    -   External Reference ID (text)
        -   Help
            -   "External Reference ID is the key id to identify a
                specific data value.\"
    -   Data Format (text/number/date/Resource Type?)
    -   Default Value
        -   Default Value field type shall change depending on the Data
            Format
        -   Help:
            -   "Value assigned will be used when Reference ID is
                unavailable from the request payload. Leaving this field
                empty will make the pair mandatory. An error will be
                returned when request is incomplete."
-   I want to be able to "Import" an external Excel file to my
    ***External Data Format***
    -   imported data shall be loaded into the above table
    -   Help:
        -   "Input data can be imported from an CSV file. Import data
            shall follow the format as specified in this document. "

### Workflow Integration Settings - push form data through [Webhooks][MVP3]
![Form Settings Workflow Integration](./img/formSettings_workflowIntegrationWebhook.png)
-   Description
    -   "Push Form collected data to external URL service"
-   "Webhook URL" field shall accept the correct formatted domain
    -   Help
        -   "Enabling Webhooks will push completed form data to your
            specified URL"
-   "Enable WebHook"
    -   shall be the option to disable/enable this feature
-   "Request Headers" section
    -   shall be customizable through a table with add/edit/delete
        functions:
        -   "Parameter Name" (textfield)
        -   "Parameter Value" (textfield)
-   "Request Payload Parameters"
    -   shall be customizable through table with add/edit/delete
        functions:
        -   "Parameter Name"
            -   Shall be loaded based on "Parameter Value" choice
        -   "Parameter Value (Reference ID)"
            -   Shall have a dropdown picker for the Reference ID/Field
                Name within the form
    -   Above table shall have an "Add all Form Fields" button
        -   Shall add all Form Fields within the form based on
            "Reference ID" settings (excluding pages/sections)

### Export Settings category [MVP2]
-   I want to Export a Form as a Template
-   I want to Export a Form to PDF
\<More settings to come\>