Why do we need this?
====================

Most software projects fail because they **[were not]{.underline}**
focused on delivering actual usable business value to a user on a
regular basis. These are the Agile Principles to guide us by.

Agile Principles

1.  Working software is primary measure of progress (adds Business Value
    for our Users)

2.  Highest priority is to satisfy the customer through EARLY and
    CONTINUOUS delivery of valuable software

3.  Simplicity - the art of maximizing the amount of work not done - is
    essential

We want to build software from the user's perspective, so we have
something to show at every milestone. Build the necessary dependencies
as needed, as you need it along the way. Trivial Rewrites down the road
are alright, it is outweighed by the benefits of early delivery.