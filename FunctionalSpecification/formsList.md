## View All Forms
-   I want to see a 2nd panel "My Forms" (existing saved Forms)
-   I want to be able to change from List View to Grid View on My Forms
-   I want to see a search Bar to help me navigate through My Forms

## Views

### List View
![List View Wireframe](./img/formsList_list.png)
![List View Mockup](./img/formsList_list2.png)
-   I want to see 4 columns
    -   Title
    -   Description
    -   Date last modified
    -   Actions

### Grid View
![Grid View Wireframe](./img/formsList_grid.png)
![Grid View Mockup](./img/formsList_grid2.png)
-   I want to see Form Name and Description, Date last modified and a settings button when My Forms are in Grid View

## Actions

### Create a Form
-   I want to see a panel "Create a New Form"
-   I want to see an "Empty Form" as the first selection
-   I want to see my most recently used templates right beside the
    "Empty Form" to select from
-   I want to select any one of these and be able to create either an
    "Empty Form" or a "Template" and jump to Edit Mode

### Edit an existing Form a Form
-   I want to be able to click an existing Form to go to Edit Mode
-   I want the following behavior when I newly open a form
    -   Go Directly into Edit mode
    -   The first page will be in view
    -   All sections collapsed
    -   Preview Panel open

### Search within My Form
-   I want to see My Forms show filtered results as I type keywords
-   I want the search to match both Form Name and Description
-   I want to see an "x" in the search bar so I can clear my search
    string

### Copy a Form
-   I want to be able to copy a Form from the settings in the Form Icon when in Grid View
-   I want to see the copied Form right beside/below the original Form

### Delete a Form
-   I want to be able to delete a Form from the settings in the Form Icon when in Grid View
-   I want to be prompted confirmation upon clicking delete

### I want to publish Saved Forms to connected client websites
-   I want to be able to Publish a Form straight from the Forms List
-   I want to see a popup page showing my successful publish and the URL where it was published
-   I want to be given a button to jump to the published URL

## Interaction

### I want to see a Live Preview of my Form
![Live Preview](./img/formsList_preview.png)
-   When users click on any existing Form Name, a *Live Preview Panel*
    shall appear
-   The *Live Preview Panel* shall occupy half of the screen so content should be scaled down to fit
-   Quick Actions shall overlay the upper part of the screen of the
    *Live Preview Panel* for quick access
    ![Live Preview](./img/formsList_preview2.png)

### I want to have access to edit Form Settings
-   I want to be able to access Form settings \<see Form Settings\> from the settings (...) when in List view
-   I want to be able to access Form Settings\<see Form Settings\> from the settings (...) in the Form Icon when in Grid View
-   I want to be able to access Form Settings\<see Form Settings\> from quick actions list in the Preview Panel

### I want to be able to create a new form from a Templates List [MVP2]
![Live Preview](./img/formsList_template.png)
-   I want to drop open the "Browse More Templates" to see more
    templates
-   I want the templates list to be categorized by "Recently Used" and other categories
-   I want to be able to search through these templates with Filter functionality
-   I want to see Templates to show filtered results as I type keywords
