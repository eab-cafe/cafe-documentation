## "Summary" and "Individual" view of Form's Responses

## Summary view of Form responses
### Overall Summary
-   Overall Summary can be in form of a fancy label or some statistical charts
    - Device used
    - Time Filled in chart
    - Percentage finished
    - Number of responses
    - Stats - Total Visits
    - Stats - Total Unique Visits
    - Average Time to complete

### Navigation
-   There shall be a sidebar panel with Page/Section/Question to navigate to certain parts of the form easily

### Search and Filter
-   Overall search for the question

### Question Statistical View
-   Each question will be summarized based on the Question Type it contains
    -   Text field, Date, Plain Text
        -   List of answers in sequential format. With link to the Individual Response
    -   Multiple Choice, Dropdown, Toggle
        -   Pie chart showing how many % answered
    -   Slider, Calculated Field
        -   Horizontal Bar Chart, Line graph of the count of the answers

### Export
-   There shall be an option to export the summary of responses to a csv file
    -   Option to include Overall Summary
    -   Option to include all responses in table format

## Individual view of Form Responses
### Individual Responses View
-   There shall be a general overview of the Individual's Response
    -   Completed/Saved/% Completed
    -   Date completed
    -   Other information in [Overall Summary](FunctionalSpecification/responsesMode?id=overall-summary)
-   There shall be a nice UI to display Form Responses Individually
    -   For every question, the ff information shall be included:
        -    Reference ID
        -    Question Type
        -    Question Name
        -    Question Answer

### Navigation
-   There shall be a sidebar panel with Page/Section/Question to navigate to certain parts of the form easily
-   There shall be pagination control to navigate between Individual Answers with ease
    -   Each individual answer has to be sorted sequentially based on creation time in the pagination controls

### Search and Filter
-   There shall be a search bar that searches for individual or for all forms
-   There shall be a filter that may be chosen to search a specific Value

### Export
-   There shall be an option to export an individual response to a csv file