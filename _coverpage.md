<!-- ![logo](_media/icon.svg) -->

# Cafe Forms <small>1.0.0</small>

> Documentation  
> <sub>--- [powered by EAB systems](https://eabsystems.com/) ---<sub>

Cafe Forms provides an interface to build interactive, business-centric forms for data gathering.

[Visit Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/)
[Get Started](/README)