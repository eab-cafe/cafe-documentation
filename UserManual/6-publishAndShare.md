Publish and send your form in just a few easy steps!

Once your form is ready, you can send it to other people to collect their Sharing your form can be through email or social media, or you can embed it into a webpage.

## Publish form
1. Open a form in [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/).
2. In the upper left of your form, click `Publish`.
3. On the popup, click `Publish` or press Ctrl + p (Windows) or ⌘ + p (Mac) on your keyboard.  

![publish](img/publish-publish.png)
4. Bottom snackbar will confirm successfull action: "Form Saved and Published"

?> Publishing a form will be published to everyone viewing your form at a certain point in time. So be sure your form does not have significant changes that mess up the responses you receive.

### View Published form

View your published form through a chat or email message, you can get a link to the form.

!> When a form is viewed and filled in and submitted, a response will be already be saved.

1. Open a form in [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/).
2. In the top left, click Settings.
3. At the top of the window, beside `Share Public Link`, click on the `view` icon

A new browser tab will open with the Form you just published!

## Share your form

### Get Form Link
If you want to share a form through a chat or email message, you can get a link to the form.

1. Open a form in [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/).
2. In the top left, click Settings.
3. At the top of the window, beside `Share Public Link`, click on the `copy` icon
4. Alternatively, clicking on the link shall also copy the link to your clipboard

?> Paste this link on chat or anywhere to share the form to other people

### Email a form
!> Phase 4
1. Open a form in [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/).
2. In the top left, click Settings.
3. At the top of the window, beside `Share Public Link`, click the `email` icon
4. Add the email addresses you want to send the form to, along with the email subject and message.
5. Click Send.


### Share a form on social media
!> Phase 4
1. Open a form in [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/).
2. In the top left, click Settings.
3. At the top of the window, beside `Share Public Link`, click on any social media icon (Google+, Twitter, or Facebook)
4. Follow the instructions to share the form.