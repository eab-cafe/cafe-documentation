Add a title and questions for your form in just a few easy steps!

## Create a new form

1. Go to [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/).
2. Click Forms Menu > Add icon.  

![empty-form](img/3-createForm/create-emptyform.png ':size=1000%')
4. Click on `Empty Form`
4. A new form will open.
5. Give a Title and a Description to your form.  

![form-title](img/3-createForm/create-title.png ':size=1000%')
6. At the upper left, click `Save`

?> You will be brought to Cafe Mode: <b>Edit Mode</b> Here you can add, edit, or format text, images, or GIFs in a form.
</br>
</br>
(Learn more about modes in [Cafe Modes](UserManual/2-quickStart.md#cafe-modes))

## Add form content

After you've created a form, you can add and edit endless pieces of content, like questions, plain text, signature fields and images. You can organize your form by topics and sub-topics by adding pages and sections.

### Add a Page
1. In [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/), open a form.
2. In the left panel, Click Add Page.
3. Name the new page.  

![page](img/3-createForm/create-page.png ':size=1000%')

### Add a Section
Sections group questions together to make your form easier to read.

1. In [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/), open a form.
2. Click Add Section.
3. Name the new section.  

![section](img/3-createForm/create-section.png ':size=1000%')

### Add a Question
1. In [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/), open a form.
2. In the center panel, go into a section.
3. Click Add Question.  

![question](img/3-createForm/create-question.png ':size=1000%')
4. A question card will appear.
5. Type in your Question.
6. To the right of the question card, choose the type of question you want. (Learn how to choose a [Question Type](UserManual/4-questionTypes.md))
7. To the left of the question card, turn on Mandatory to require answers to this question.
8. Input the possible answers to your question (if applicable).  

![question-edit](img/3-createForm/create-question-add.png ':size=1000%')

## Edit form content
To edit a question, header, or description, click the text you want to change.

### Copy content

| Content Item &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;| How to copy? &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |
| ------------- | ---------------- |
| Questions or images      | 1. Click a question or image.<br>2. Click Duplicate icon. |
| Sections     | 1. Click a section header.<br>2. Hover over More icon.<br>3. Click Duplicate icon. |
| Pages      |  1. Click a Page header.<br>2. Hover over More icon.<br>3. Click Duplicate icon.|


### Delete content

| Content Item &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;| How to delete? &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |
| ------------- | ---------------- |
| Questions or images      | 1. Click a question or image.<br>2. Click Delete icon. |
| Sections     | 1. Click a section header.<br>2. Hover over More icon.<br>3. Click Delete icon. |
| Pages      |  1. Click a Page header.<br>2. Hover over More icon.<br>3. Click Delete icon.|

### Reorder Pages or Sections
1. If you have more than one section/pages, you can change the order.
2. At the left panel of your form, hover over the section/page you would like to move  

![question-page-hover](img/3-createForm/create-page-hover.png ':size=1000%')
3. Click and Drag Moving icon to your desired location only within the left panel  

![question-page-move](img/3-createForm/create-page-move.png ':size=1000%')
4. Drop to apply the location for your page/section.

### Undo and Redo an action
!> MVP4
1. If you want to undo a recent change:
2. At the top right of your form, you are presented an Undo icon and a Redo icon.
3. Click Undo to Undo.
4. Click Redo to Redo.

## Save your form
After making your changes, it is important to save your changes.

1. Open a form in [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/).
2. In the upper left of your form, click `Save` or press Ctrl + s (Windows) or ⌘ + s (Mac) on your keyboard.  

![publish](img/overview-save.png ':size=1000%')
3. Bottom snackbar will confirm successfull action: "Form Saved"  

!> Saving a form will not publish the form. Learn more about [Publish](UserManual/6-publishAndShare.md#publish-form) 