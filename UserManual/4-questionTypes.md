You can include up to 2,000 total choices and collect several types of answers in your form or quiz.  

Learn how to [Add a question to the form](UserManual/3-createForm.md#add-a-question)

## Standard Question Types

### Text Input
Users respond to this question by typing words or numeric on this field.  
![question-textinput](/img/4-questionTypes/question-textinput.png)
1. In [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/), open a form.
2. Add a question.
3. In the upper right of the Question Card, select Question type: `Text Input`  

![question-type-textinput](img/4-questionTypes/question-type-textinput.png)

4. Select an `Input Type`
 - <b>Single Line</b>
<img style="float:right;" width="50%" alt="question-textinput-single" src="UserManual/img/4-questionTypes/question-textinput-single.png">
<br><br><br><br><br><br><br><br>
 - <b>Multiple Line</b>
<img style="float: right;" width="50%" alt="question-textinput-multiple" src="UserManual/img/4-questionTypes/question-textinput-multiple.png">
<br><br><br><br><br><br><br><br><br>
5. Settings to further customize your field:

| Setting &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; | Effect &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |
| ------------- | ---------------- |
| Prefix      | Text to show as a prefix on the textfield |
| Postfix     | Text to show as a postfix on the textfield |
| Format      |  A choice of the format:<br> * `Numeric only` - only accepts numeric input<br> * `Characters` - only accepts alphabetical input<br> * `E-mail` - shows an error when input is not a valid email<br> * `NRIC` - shows an error when input is not a valid NRIC<br> * `HKID` - shows an error when input is not a valid HKID<br> * `Phone Number` - shows text input as a Phone Number Picker<br>|
| Max Length  | Input count will be restricted to this number |
| Placeholder | Text to show when field is empty |

### Date 
Users respond to this question by picking a date or time.  
![question-date](/img/4-questionTypes/question-date.png)

1. In [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/), open a form.
2. Add a question.
3. In the upper right of the Question Card, select Question type: `Date`  

![question-type-date](img/4-questionTypes/question-type-date.png)

4. Select a `Date/Time Type`
 - <b>Date</b>
<img style="float:right;" alt="question-type-date" src="UserManual/img/4-questionTypes/question-date-date.png">
<br><br><br><br><br><br><br><br>
 - <b>Time</b>
<img style="float: right;" alt="question-date-time" src="UserManual/img/4-questionTypes/question-date-time.png">
<br><br><br><br><br><br><br><br>
 - <b>Date & Timee</b>
<img style="float: right;" alt="question-date-datetime" src="UserManual/img/4-questionTypes/question-date-date&time.png">
<br><br><br><br><br><br><br>

5. Settings to further customize your field:

| Setting &nbsp; &nbsp; &nbsp; &nbsp; | Effect &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |
| ------------- | ---------------- |
| Date Format | Display format of the Date field |
| Min Date | Minimum date user can pick |
| Max Date | Maximum date user can pick |
| Min Time | Minimum time user can pick |
| Max Time | Maximum time user can pick |

6. Check `Ranged Input` to show the field with 2 inputs  
![date-ranged](img/4-questionTypes/question-date-ranged.png)

## Choices Question Types
### Multiple Choices
Users respond to this question by picking a pre-defined list of choices.  
![question-multiplechoice](/img/4-questionTypes/question-multiplechoice.png)

1. In [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/), open a form.
2. Add a question.
3. In the upper right of the Question Card, select Question type: `Multiple Choice`  

![question-type-multiplechoice](img/4-questionTypes/question-type-multiplechoice.png)

4. Select an `Input Type`
 - <b>Single Choice</b>
<img style="float:right;" alt="question-multiple-single" src="UserManual/img/4-questionTypes/question-multiple-single.png">
<br><br><br><br><br><br><br><br>
 - <b>Multiple Choice</b>
<img style="float: right;" alt="question-multiple-multiple" src="UserManual/img/4-questionTypes/question-multiple-multiple.png">
<br><br><br><br><br><br><br><br><br>
5. Click `+ Choices` and type in your choice
  * To the left of any choice, drag&drop to reorder choices
  * To the right of any choice, click `X` icon to remove
6. Check `Show other options` to allow a custom input from the user  

![multiple-others](img/4-questionTypes/question-multiple-others.png)

#### Add Image to a Questions or Answer

You can add an image to a question or an answer for multiple choice or checkbox questions.

1. In [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/), open a form.
2. Click a Multiple Choices type question.
3. To the right of any choice, click the Image icon.  

![multiple-image](img/4-questionTypes/question-multiple-image.png)

4. Upload or choose an image.
5. Crop the image.  

![multiple-image-crop](img/4-questionTypes/question-multiple-image-crop.png)
6. Click Choose.  

![multiple-image-uploaded](img/4-questionTypes/question-multiple-image-uploaded.png)

Your choice now has a Graphic icon that can be chosen my the user  
    ![multiple-image-uploaded-choices](img/4-questionTypes/question-multiple-image-uploaded-choices.png)

?> GIF choice upload is also supported!

### Dropdown
Users respond to this question by a dropdown of choices.  
![question-dropdown](/img/4-questionTypes/question-dropdown.png)

1. In [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/), open a form.
2. Add a question.
3. In the upper right of the Question Card, select Question type: `Dropdown`  

![question-type-dropdown](img/4-questionTypes/question-type-dropdown.png)

4. Select `Import list from Resource` to load in existing lists. See [Resources](UserManual/6-publishAndShare.md.md)
5. Click `+ Choices` and type in your choice
  * To the left of any choice, drag&drop to reorder choices
  * To the right of any choice, click `X` icon to remove  

### Slider
Users respond to this question by sliding on a bar of choices.  
![question-slider](/img/4-questionTypes/question-slider.png)

1. In [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/), open a form.
2. Add a question.
3. In the upper right of the Question Card, select Question type: `Slider`  

![question-type-slider](img/4-questionTypes/question-type-slider.png)
4. Select a `Slider style`
 - <b>Basic</b>
<img style="float:right;" width="50%" alt="question-slider-basic" src="UserManual/img/4-questionTypes/question-slider-basic.png">
<br><br><br><br><br><br><br><br>
 - <b>Basic + Label </b>
<img style="float:right;" width="50%" alt="question-slider-basiclabel" src="UserManual/img/4-questionTypes/question-slider-basiclabel.png">
<br><br><br><br><br><br><br><br>
 - <b>Basic + Label + Value Input</b>
<img style="float:right;" width="50%" alt="question-slider-basiclabelvalue" src="UserManual/img/4-questionTypes/question-slider-basiclabelvalue.png">
<br><br><br><br><br><br><br><br>
 - <b>Scales</b>
<img style="float:right;" width="50%" alt="question-slider-scales" src="UserManual/img/4-questionTypes/question-slider-scales.png">
<br><br><br><br><br><br><br><br>
 - <b>Custom Values</b>
<img style="float:right;" width="50%" alt="question-slider-custom" src="UserManual/img/4-questionTypes/question-slider-custom.png">
<br><br><br><br><br><br><br><br>
5. Settings to further customize your field:

| Setting &nbsp; &nbsp; &nbsp; &nbsp; | Effect &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |
| ------------- | ---------------- |
| Range From | Numeric input to set the minimum slider value |
| Range To | Numeric input to set the maximum slider value |
| Label Min | Text to show on the left part of the slider |
| Label Max | Text display on the right part of the slider |
| Input Prefix | Text to show as a prefix to the `Value Input` |
| Input Postfix | Text to show as a postfix to the `Value Input` |
| Input Postfix | Text to show as a postfix to the `Value Input` |

6. For Custom Values Click `+ Choices` and type in your choice
  * To the left of any choice, drag&drop to reorder choices
  * To the right of any choice, click `X` icon to remove

### Toggle
Users respond to this question by clicking on a button group of choices.  
![question-toggle](/img/4-questionTypes/question-toggle.png)

1. In [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/), open a form.
2. Add a question.
3. In the upper right of the Question Card, select Question type: `Toggle`  

![question-type-toggle](img/4-questionTypes/question-type-toggle.png)

4. Select a `Toggle Step`
 - <b>2 steps</b>
<img style="float:right;" width="50%" alt="question-toggle-2steps" src="UserManual/img/4-questionTypes/question-toggle-2steps.png">
<br><br><br><br><br><br><br><br>
 - <b>3 steps</b>
<img style="float:right;" width="50%" alt="question-toggle-3steps" src="UserManual/img/4-questionTypes/question-toggle-3steps.png">
<br><br><br><br><br><br><br><br>
5. Type in your choice in the answer fields generated
  * To the left of any choice, drag&drop to reorder choices  

![toggle-answer-fields](img/4-questionTypes/question-toggle-answer-fields.png)

## Static Question Types
### Plain Text
Users are shown rich text in this field.  
![question-plaintext](/img/4-questionTypes/question-plaintext.png)

1. In [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/), open a form.
2. Add a question
3. In the upper right of the Question Card, select Question type: `Plain Text`  

![question-type-plaintext](img/4-questionTypes/question-type-plaintext.png)

4. Type and style your text on the Text field  

![plaintext-textfield](img/4-questionTypes/question-plaintext-textfield.png)

### Button
Users can click this button and be redirected to a specific page / URL.  
![question-button](/img/4-questionTypes/question-button.png)

1. In [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/), open a form.
2. Add a question.
3. In the upper right of the Question Card, select Question type: `Button`  

![question-type-button](img/4-questionTypes/question-type-button.png)

4. Settings to further customize your field:

| Setting &nbsp; &nbsp; &nbsp; &nbsp; | Effect &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |
| ------------- | ---------------- |
| Button Text | Text to show on the button |
| Behaviour | When button is clicked, what action to make:<br> * Redirect to:: Form Page - Navigates to a Page in the form directly<br> * Redirect to:: External Page - Opens a new page to redirect to a URL Link |
| Target Destination | Accepts the Page or the URL Link to redirect to |

## Chart Question Types
### Bar Chart
A graphical bar chart can be shown based on answers in the form.  
![question-bar](/img/4-questionTypes/question-bar.png)

1. In [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/), open a form.
2. Add a question.
3. In the upper right of the Question Card, select Question type: `Bar Chart`  

![question-type-bar](img/4-questionTypes/question-type-bar.png)

4. Select an `Input Type`
 - <b>Single Value</b>
<img style="float:right;" width="50%" alt="question-bar-single" src="UserManual/img/4-questionTypes/question-bar-single.png">
<br><br><br><br><br><br><br><br>
 - <b>Multiple Values</b>
<img style="float: right;" width="50%" alt="question-ber-multiple" src="UserManual/img/4-questionTypes/question-bar-multiple.png">
<br><br><br><br><br><br><br><br>

4. Settings to further customize your field:

| Setting &nbsp; &nbsp; &nbsp; &nbsp; | Effect &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |
| ------------- | ---------------- |
| Legend | A chart legend will show when the option is `ON` |
| Orientation | The orientation setting of the chart |
| X Axis Label | Text to show as a Chart label in the X Axis (Bottom of the Chart) |
| Y Axis Label | Text to show as a Chart label in the Y Axis (Left of the Chart) |

5. Add `Bars` to your chart. A `Bar` has to be given a name and a source of its value.

| Setting &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; | Effect &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |
| ------------- | ---------------- |
| Bar Name | Name assigned to the `Bar` |
| Value | Where the value of the `Bar` is retrieved from<br> * Custom Numeric Value - Input a numeric as a static value.<br> * Question Reference - Pick a question from the list to retrieve value in real time|
| Value Group | Shows only when `Input Type` selected is `Multiple Values`. Allows multiple `Values` for each `Bar` |
| Value Group Name | Name assigned to every `Bar Group`. A `Bar Group` consists of a set of all `Bars` in the order it was set up. |

### Pie Chart
A graphical pie chart can be shown based on answers in the form.
![question-pie](/img/4-questionTypes/question-pie.png)

1. In [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/), open a form.
2. Add a question.
3. In the upper right of the Question Card, select Question type: `Pie Chart`  

![question-type-pie](img/4-questionTypes/question-type-pie.png)

4. Settings to further customize your field:

| Setting &nbsp; &nbsp; &nbsp; &nbsp; | Effect &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |
| ------------- | ---------------- |
| Legend | A chart legend will show when the option is `ON` |
| Slice Text | Slice label will show depending on this setting: <br>Slice Name - Name of the slice<br>Percentage - % share out of 100 of the slice<br> * Value - Exact numeric value of the slice<br> * None - none (you guessed it) |

5. Add `Slices` to your Pie chart. A `slice` has to be given a name and a source of its value.

| Setting &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; | Effect &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |
| ------------- | ---------------- |
| Slice Name | Name assigned to the `Slice` |
| Value | Where the value of the `Slice` is retrieved from<br> * Custom Numeric Value - Input a numeric as a static value.<br> * Question Reference - Pick a question from the list to retrieve value in real time.|

## Media Question Types
### Handwriting Field
Users respond to this question by drawing on a canvas.  

![question-handwriting](img/4-questionTypes/question-handwriting.png)

1. In [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/), open a form.
2. Add a question.
3. In the upper right of the Question Card, select Question type: `Handwriting Field`  

![question-type-handwriting](img/4-questionTypes/question-type-handwriting.png)

4. Settings to further customize your field:  

![question-handwriting-settings](img/4-questionTypes/question-handwriting-settings.png)

| Setting &nbsp; &nbsp; &nbsp; &nbsp; | Effect &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |
| ------------- | ---------------- |
| Placeholder | Text shown as a placeholder on the handwriting field |

### Profile image upload
Users respond to this question by uploading an image.

![question-profileimage](img/4-questionTypes/question-profileimage.png)

1. In [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/), open a form.
2. Add a question.
3. In the upper right of the Question Card, select Question type: `Profile Image upload`  

![question-type-profileimage](img/4-questionTypes/question-type-profileimage.png)

4. Settings to further customize your field:

| Setting &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  | Effect &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |
| ------------- | ---------------- |
| Image dimension setting | Dimension of the actual profile photo<br> * Rectangle (Vertical)<br> * Rectangle (Horizontal)<br> * Square<br> * Circle |
| Image Size | Image size to accept for upload<br> * Small<br> * Medium<br> * Large |

## Advanced Question Types
### Calculated Field
Users are shown a numeric label that is calculated in real time based on their responses. Calculations are customizable.

![question-calculatedfield](img/4-questionTypes/question-calculatedfield.png)

1. In [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/), open a form.
2. Add a question.
3. In the upper right of the Question Card, select Question type: `Calculated Field`  

![question-type-profileimage](img/4-questionTypes/question-type-calculatedfield.png)

4. Select your preferred `Calculation Type`

| Setting &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  | Effect &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |
| ------------- | ---------------- |
| Basic Calculation | Calculator interface for fast calculations that supports `+` `-` `x` and `/` |
| Advanced Formulas | Text field that accepts Excel mathematical functions and text formulas for calculation <br>e.g.<br>&nbsp; &nbsp; &nbsp; &nbsp;1+2 // returns 3.<br>&nbsp; &nbsp; &nbsp; &nbsp;AVERAGE(1,2,3) // returns 2.<br>&nbsp; &nbsp; &nbsp; &nbsp;AVERAGE(1,2,3)+1+2 // returns 5.<br>|

5. Settings to further customize your field:

| Setting &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  | Effect &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |
| ------------- | ---------------- |
| Prefix | Text to show as a prefix on the calculated result |
| Postfix | Text to show as a postfix on the calculated result |

#### Using `Basic Calculation`

![question-calculatedfield-basic](img/4-questionTypes/question-calculatedfield-basic.png)

1. Select `Basic Calculation` in a `Calculated Field` question  
2. In the first row, select a question to `Insert a question to calculation` 
3. Click `Add` button
4. Choose a calculation operator (`+` `-` `x` and `/`)
5. In the newly added row, select a question to `Insert a question to calculation` 
6. Repeat steps 2-4 to add more calculation rows
7. At any time, click `x` at the end of each row to delete the row

!> Be sure to always select a question for each calculation row so your formula calculates correctly

#### Using `Advanced Formulas`

Excel Formulas are supported 

![question-calculatedfield-advanced](img/4-questionTypes/question-calculatedfield-advanced.png)

?> Type the `#` keyword to include questions in your formula

1. Select `Advanced Formulas` in a `Calculated Field` question  
2. Under `Input Formula`, click on the text field
3. Type in the formula
4. To include a question in your calculation, type the keyword `#`
5. Pick a question from the questions list
6. Click outside the text field to check/accept your formula

!> Sometimes, a space " " is needed before the `#` to open up the questions list

### Conditional Choices
Users respond to this question by picking from a set of choices that are visible based on other questions. More than once choice can be selected.

![question-conditionalchoices](img/4-questionTypes/question-conditionalchoices.png)

1. In [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/), open a form.
2. Add a question.
3. In the upper right of the Question Card, select Question type: `Conditional Choices`  

![question-type-conditionalchoices](img/4-questionTypes/question-type-conditionalchoices.png)

4. Download the `Conditional Choices CSV template` as a guide for upload later.
5. Define your choices in the CSV file

The first column in the `CSV template` contains keywords that shall be modified with care. Each column that follows is a choice provided to the user to respond with.

| <div style="width:120px; text-align:left">First Column</div> | <div style="width:100px; text-align:left">Desription</div> | <div style="width:300px; text-align:left">Value in succeeding columns</div> | 
| ------------- | ---------------- | ---------------- |
| Choice Number | Do not modify. | Assign a number for Admin reference only |
| Choices Name | Do not modify. | The choice name shown for the user to select |
| Group tag | Do not modify. | Group name for the choices. Each group will display in a separate section. |
| Description | Do not modify. | Descriptive text to show as a helper icon when the Choice is visible. Supports html styling! |
| URL | Do not modify. | Assign a URL redirect link to the Choice |

6. Add rules to the visibility of the choices you just defined

Each Q<#> of the same # is part of the same `choice group` which gets an answer from another Question for visibility checking.

| <div style="width:120px; text-align:left">First Column</div> | <div style="width:100px; text-align:left">Desription</div> | <div style="width:300px; text-align:left">Value in succeeding columns</div> | 
| ------------- | ---------------- | ---------------- |
| Q<#> Reference | Assign a number to the Q<#> to define a `choice group`<br> ex: <br> - Q1 Reference<br> - Q2 Reference<br>  and so on... | Put in the Reference ID of the question to get answer from. This Reference ID will be linked to Q<#><br><br> Only supports `Multiple Choice Question` |
| Q<#> Description | Use the same numeric Q<#> of your `choice group`<br> ex: <br> - Q1 Description<br> - Q2 Description<br>  and so on...  | Description of the question for Admin reference only |
| Q<#> Choice<#> | Use the same numeric Q<#> of your `choice group`. <br><br>Assing a numeric to the Choice <#> to indicate which answer to do comparison on<br> ex: <br> - Q1 Choice 1<br> - Q1 Choice 2<br>  and so on...| Assing a Y/N to define rules of the `choice group` visiblity |

?> When this question is published, choices are only shown when at least one of the `Q<#> Choice<#>` for every `choice group` is selected by the respondent. If an `N` is selected, the choice will be hidden

6. Upload your modified CSV Template and see your choices in the table list

![question-conditionalchoices-settings](img/4-questionTypes/question-conditionalchoices-settings.png)


### Question Reference
Users respond to this question by picking a pre-defined list of choices based on `Conditional Choices`

![question-questionreference](img/4-questionTypes/question-questionreference.png)

1. In [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/), open a form.
2. Add a question.
3. In the upper right of the Question Card, select Question type: `Question Reference`  

![question-type-questionreference](img/4-questionTypes/question-type-questionreference.png)

4. Pick a `Conditional Choices` question from the dropdown list

?> When this question is published, choices are only shown when at least one of the choices from the referenced `Conditional Choices` is selected.

</br></br>

Thats it! To add style to your question, head on to [Design and Preview](UserManual/createForm.md#add-question-pages-and-sections).