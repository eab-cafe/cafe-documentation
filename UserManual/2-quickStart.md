## Installation

No installation needed! Just go to our official [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/) to get started.

## Topics
- [Create your form](UserManual/3-createForm.md)
- [Choose Question Type](UserManual/4-questionTypes.md)
- [Design and Preview your form](UserManual/5-designAndPreview.md)
- [Publish and Share](UserManual/6-publishAndShare.md)
- [Analyze form responses](UserManual/7-analyzeResponses.md)
- [Advanced form settings](UserManual/8-rulesAndValidation.md)

## Cafe Menu

The left bar is where the Cafe Menu resides.
![menu-resource](img/cafe-menu.png)

### Home 

!> Phase 4 

### Forms
Create and manage forms your here. Your forms list will reside here. 

Hover over each row to quickly access functions such as `Copy` and `Delete`

?> Use search to quickly navigate through the lists

### History

!> Phase 4 

### Members

!> Phase 4 

### Resources
The Resources manages resources available for a form. Learn more about [resources](UserManual/9-advancedSettings.md#resources).

## Cafe Modes

At any time when a form is open, Cafe Modes play a big role in direction. There are a total of 3 Modes in Cafe: Edit Mode, Design Mode, and Responses mode.

### Edit Mode

This is the default mode when a form is created/opened. This mode allows for any text editing and control.

1. In [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/), open a form.
2. In the top menu, click Edit.  

![menu-edit](img/menu-edit.png ":size=1000%")

### Design Mode
This mode allows for any color and styling functions within a form.

1. In [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/), open a form.
2. In the top menu, click Design.  

![menu-design](img/menu-design.png)

### Responses Mode
This mode gathers the responses of a published form.

1. In [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/), open a form.
2. In the top menu, click Response.  

![menu-response](img/menu-response.png)