Advanced settings are available for your form to extend and increase your form's usability and reach!

## Integration

### Reference ID

`Reference ID`s are generated for all Pages, Sections, Questions and Answers. The `Reference ID` is used to uniquely identify each field for export in later use.

1. Open a form in [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/).
2. On the top left, click the `Form Settings` icon

  <center><b><i> -- A Form settings pop-up shall appear -- </i></b></center>

3. On the left navigation bar, click on the `Reference ID` menu
4. Column definitions

| Column &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; | Description &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |
| ------------- | ---------------- |
| Type | Type of the component<br> * Page<br> * Section<br> * Question<br> * Answer |
| Content | Name of the component |
| ID | Reference ID assigned to the component |
| Pen icon | Visible upon hover. When clicked will put `Reference ID` into edit mode |

5. You can modify each `Reference ID` to your preference by clicking the `pen` icon to the right of every row

![advanced-reference-id](img/9-advancedSettings/advanced-reference-id.png)

?> When modifying the `Reference ID`, it is important to make sure it is unique within the whole form. 

?> `Reference ID` of answers can repeat as these are uniquely identified by the Question that contains it. 

### Import Reference ID

`Import Reference ID`s are custom unique `Reference ID`s you can set to accept data in your form. Imported data can used within your form and be exported later too! 

!> When `Import Reference ID` is enabled, import data is required before the form can be opened. 

1. Open a form in [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/).
2. On the top left, click the `Form Settings` icon

<center><b><i> -- A Form settings pop-up shall appear -- </i></b></center>

3. On the left navigation bar, click on the `Import Reference ID` menu.  

![advanced-import-id](img/9-advancedSettings/advanced-import-id.png)

4. Switch `Import Reference ID` to `ON`. This will enable the table underneath.  

![advanced-import-id-on](img/9-advancedSettings/advanced-import-id-on.png)

4. Column definitions

| Column &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; | Description &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |
| ------------- | ---------------- |
| Import Reference ID | Reference ID assigned to the import data |
| Type | Type of the import data<br> * Text<br> * Numeric<br> * Date (dd/mm/yyyy) |
| "..." icon | Visible upon hover. When clicked will put Reference ID into edit mode<br> * Pen icon - Click to edit<br> * Bin icon - Click to delete |

5. You can add `Import Reference ID`s by clicking the Add icon

6. You can modify or delete `Import Reference ID` to your preference by clicking the `pen` icon to the right of every row  

![advanced-import-id-add](img/9-advancedSettings/advanced-import-id-add.png)

?> When modifying the `Import Reference ID`, it is important to make sure it is unique within the whole form.

## Multilanguage
!> Phase 4

## Resources
!> Phase 4

### Send a form with pre-filled answers
You can send respondents a form with some fields already filled in.

1. Open a form in [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/).
2. In the top right, click More More.
3. Choose Get pre-filled link.
4. Fill in any answer fields you want to pre-populate.
5. Click Submit.
6. To send the pre-populated form to respondents, copy and send the link at the top.

### Embed a form on a website or blog
1. Open a form in [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/).
2. In the top right, click Send.
3. At the top of the window, click Embed Embed.
4. To copy the HTML that appears, click Copy or press Ctrl + c (Windows) or ⌘ + c (Mac) on your keyboard.
5. Paste the HTML into your website or blog.