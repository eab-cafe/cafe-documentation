When you send a form, you can gather the responses inside the form, through an external endpoint or separately in CSV File.

1. In [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/), open a form.
2. In the top menu, click Response.  

![design-response](img/menu-response.png)

?> You will be brought to Cafe Mode: <b>Response Mode</b>
</br>
(Learn more about modes in [Cafe Modes](UserManual/2-quickStart.md#cafe-modes))

## Manage reponses

### View responses of a form
1. Open a form in [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/).
2. Go to `Response Mode`
3. In the top right panel click `Individual`.
4. Use any of the filter options to find the response you want to view
5. Under the filter options, a response list represents the responses received in a form. Click on the Response you want to view
6. The response selected will be shown in the center panel 
7. Use the Left Navigation bar to navigation between sections and pages

### Delete individual responses 
1. Open a form in [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/).
2. Go to `Response Mode`
3. In the top right panel click `Individual`.
4. In the Response List, hover along the Response you want to delete. 
4. Click the Delete icon to delete.

### Delete all responses from a form

!> Phase 4

1. Open a form in [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/).
2. In the top right panel click Summary.
3. In the top right, click More More and then Delete all responses Delete .
4. Click OK.

## Select responses save destination

!> Phase 4

1. Open a form in [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/).
2. In the top left under “Responses,” click Summary.
3. In the top right, click More and then Select response destination.
4. Choose an option: 
  - CSV file: Creates a CSV Response file for responses for download
  - Select existing CSV Response file: Choose from your existing CSV Response files to store responses
5. Click Create or Select.

## Print a form and responses
!> Phase 4

