
You can set up a form to that are reactive to each of your respondent's input. Make questions reflexive and validated based on other answers in real time.

### Question Visibility Rule
A Question is by default visible. Question Visibility Rule allows questions to show/hide based on other answers.

1. Open a form in [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/).
2. At the bottom right of a question, click the `Settings` icon
  - If the question is collapsed, hover over the '...' to gain access to the `Settings` icon  

![rule-question-settings](img/8-rulesAndValidation/rule-question-settings.png)
<center><b><i> -- A Question settings pop-up shall appear -- </i></b></center>

3. On the left, click on the `Visibility` menu  

![rule-question-settings-visibility](img/8-rulesAndValidation/rule-question-settings-visibility.png)
4. Select the conditional behaviour on `Show only when ___ conditions are met`

| Logical option &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;| Logical Operator equivalent &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |
| ------------- | ---------------- |
| All | An `AND` logical operator. Evaluates as true when all fields are true. |
| Any | An `OR` logical operator. Evaluates as true when at least one field is true. |

5. Set the `Visibility Rule`  

![rule-question-settings-visibility-rule](img/8-rulesAndValidation/rule-question-settings-visibility-rule.png)  

| Column &nbsp; &nbsp; | Description &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |
| ------------- | ---------------- |
| Field | The question to make the comparison on. Dropdown selection refers to all comparable questions within the form. |
| Condition | The method of comparison between the `Field` and the `Value` <br> * `is visible` <br> * `is equal` <br> * `is not equal` <br> * `has answer` <br> * `has no answer` |
| Value | The value compared to the `Field`. Appears as a dropdown if the `Field` selected is of a Question Type with predefined values. |
6. Once you're happy with the settings, click `Apply`

?> A Question with `Visibility Rule` set is ONLY visible when rules are evaluated as true. Hidden questions' answers will not be stored in the responses.

### Section Visibility Rule
A Section is by default visible. Section Visibility Rule allows sections to show/hide based on other answers.

1. Open a form in [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/).
2. At the upper right of a section, click the `Settings` icon
  - If the section is collapsed, hover over the '...' to gain access to the `Settings` icon  

![rule-section-settings](img/8-rulesAndValidation/rule-section-settings.png)

<center><b><i> -- A Section settings pop-up shall appear -- </i></b></center>

3. On the left, click on the `Visibility` menu  

![rule-section-settings-visibility](img/8-rulesAndValidation/rule-section-settings-visibility.png)
4. Select the conditional behaviour on `Show only when ___ conditions are met`

| Logical option &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;| Logical Operator equivalent &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |
| ------------- | ---------------- |
| All | An `AND` logical operator. Evaluates as true when all fields are true. |
| Any | An `OR` logical operator. Evaluates as true when at least one field is true. |

5. Set the `Visibility Rule`  

![rule-section-settings-visibility-rule](img/8-rulesAndValidation/rule-section-settings-visibility-rule.png)  

| Column &nbsp; &nbsp; | Description &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |
| ------------- | ---------------- |
| Field | The question to make the comparison on. Dropdown selection refers to all comparable questions within the form. |
| Condition | The method of comparison between the `Field` and the `Value` <br> * `is visible` <br> * `is equal` <br> * `is not equal` <br> * `has answer` <br> * `has no answer` |
| Value | The value compared to the `Field`. Appears as a dropdown if the `Field` selected is of a Question Type with predefined values. |
6. Once you're happy with the settings, click `Apply`

?> A Section with `Visibility Rule` set is ONLY visible when evaluated rules are true. Hidden sections also hides the questions it contains.

### Page Visibility Rule
A Page is by default visible. Page Visibility Rule allow Pages to show/hide based on their answers.  

!> Page `Visibility Rule` is supported when Cafe Form is integrated to other systems and contains `Import Reference IDs`. See [Integration](#Integration) for more information.

1. Open a form in [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/).
2. On the top left, click the `Form Settings` icon

<center><b><i> -- A Form settings pop-up shall appear -- </i></b></center>

3. Enable Page Visibility  

![rule-page-visibility-enable](img/8-rulesAndValidation/rule-page-visibility-enable.png)  

4. Click outside the popup to go back to your form
5. On the left navigation bar, hover over the '...' of a Page and click the `Settings` icon  

![rule-page-visibility](img/8-rulesAndValidation/rule-page-visibility.png)  

<center><b><i> -- A Page settings pop-up shall appear -- </i></b></center>

6. On the left navigation bar, click on the `Visibility` menu  

![rule-page-visibility-popup](img/8-rulesAndValidation/rule-page-visibility-popup.png)
7. Select the conditional behaviour on `Show only when ___ conditions are met`

| Logical option &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;| Logical Operator equivalent &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |
| ------------- | ---------------- |
| All | An `AND` logical operator. Evaluates as true when all fields are true. |
| Any | An `OR` logical operator. Evaluates as true when at least one field is true. |

8. Set the `Visibility Rule`  

![rule-page-visibility-rule](img/8-rulesAndValidation/rule-page-visibility-rule.png)  

| Column &nbsp; &nbsp; | Description &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |
| ------------- | ---------------- |
| Field | The question to make the comparison on. Dropdown selection refers to all Import IDs passed into the form. |
| Condition | The method of comparison between the `Field` and the `Value` <br> * `is visible` <br> * `is equal` <br> * `is not equal` <br> * `has answer` <br> * `has no answer` |
| Value | The value compared to the `Field`. Appears as a dropdown if the `Field` selected is of a Question Type with predefined values. |
9. Once you're happy with the settings, click `Apply`

?> A Page with `Visibility Rule` set is ONLY visible when rules are evaluated as true. Hidden pages also hides the sections it contains.

### Question Validation Rules
A Question's answers can be validated by other fields.

1. Open a form in [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/).
2. At the bottom right of a question, click the `Settings` icon
  - If the question is collapsed, hover over the '...' to gain access to the `Settings` icon  

![rule-question-settings](img/8-rulesAndValidation/rule-question-settings.png)
  
<center><b><i> -- A Question settings pop-up shall appear -- </i></b></center>

3. On the left, click on the `Validation` menu  

![rule-question-settings-validation](img/8-rulesAndValidation/rule-question-settings-validation.png)
4. Select a message group.

![rule-question-settings-validation-message-group](img/8-rulesAndValidation/rule-question-settings-validation-message-group.png)

?> :pencil2: Create more message groups to show different validation messages according to a certain set of rules

5. Choose `type` and key in your message. The `type` customizes how it the message looks like.  

![rule-question-settings-validation-message](img/8-rulesAndValidation/rule-question-settings-validation-message.png)  

| Type &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;| Effect &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |
| ------------- | ---------------- |
| Error Message | Shows as red error message. Will mark question as incomplete and restrict page navigation |
| Warning Message | Shows as a yellow warning message. Will not restrict page navigation |

6. Set the `Validation Rule` under `Rules apply here:`

![rule-question-settings-validation-rule](img/8-rulesAndValidation/rule-question-settings-validation-rule.png)  

| Column &nbsp; &nbsp; | Description &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |
| ------------- | ---------------- |
| Field | The question to make the comparison on. Dropdown selection refers to all comparable questions within the form. |
| Condition | The method of comparison between the `Field` and the `Value` <br> * `is visible` <br> * `is equal` <br> * `is not equal` <br> * `has answer` <br> * `has no answer` |
| Value | The value compared to the `Field`. Appears as a dropdown if the `Field` selected is of a Question Type with predefined values. |

To achieve complex rules, you may cascade `Validation Rules` of up to two(2) levels with the appropriate 'Logical option`.

| Logical option &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;| Logical Operator equivalent &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |
| ------------- | ---------------- |
| All | An `AND` logical operator. Evaluates as true when all fields are true. |
| Any | An `OR` logical operator. Evaluates as true when at least one field is true. |

7. Once you're happy with the settings, click `Apply`

?> A Question's `Validation Rule` Message will ONLY be visible when rules of that Message group evaluates to true. 

### Question Relationship Rules
A Question's answer can be automatically filled in based on other fields. 

1. Open a form in [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/).
2. At the bottom right of a question, click the `Settings` icon
  - If the question is collapsed, hover over the '...' to gain access to the `Settings` icon  

![rule-question-settings](img/8-rulesAndValidation/rule-question-settings.png)
  
<center><b><i> -- A Question settings pop-up shall appear -- </i></b></center>

3. On the left, click on the `Relationship` menu  

![rule-question-settings-relationship](img/8-rulesAndValidation/rule-question-settings-relationship.png)
4. Select a message group.

![rule-question-settings-relationship-group](img/8-rulesAndValidation/rule-question-settings-relationship-group.png)

?> :pencil2: Create more groups to automatically fill different answers based on different sets of rules

5. Customize your Relationship Action

![rule-question-settings-relationship-action](img/8-rulesAndValidation/rule-question-settings-relationship-action.png)  

| Setting &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;| Effect &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |
| ------------- | ---------------- |
| Edtiable | Makes the field read only when value is `No` |
| Fill Value | Accepts text and Mentions (#) to retrieve the answer from other fields. Shows as a dropdown field depending on the Question Type |

?> Try typing in `#` in the Fill Value field, you have to type down the Question Name whose answer you will want to auto fill in this Question. Multiple mentions are supported as well!

6. Set the `Validation Rule` under `Rules apply here:`

![rule-question-settings-relationship-rule](img/8-rulesAndValidation/rule-question-settings-relationship-rule.png)  

| Column &nbsp; &nbsp; | Description &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |
| ------------- | ---------------- |
| Field | The question to make the comparison on. Dropdown selection refers to all comparable questions within the form. |
| Condition | The method of comparison between the `Field` and the `Value` <br> * `is visible` <br> * `is equal` <br> * `is not equal` <br> * `has answer` <br> * `has no answer` |
| Value | The value compared to the `Field`. Appears as a dropdown if the `Field` selected is of a Question Type with predefined values. |

To achieve complex rules, you may cascade `Relationship Rules` of up to two(2) levels with the appropriate 'Logical option`.

| Logical option &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;| Logical Operator equivalent &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |
| ------------- | ---------------- |
| All | An `AND` logical operator. Evaluates as true when all fields are true. |
| Any | An `OR` logical operator. Evaluates as true when at least one field is true. |

7. Once you're happy with the settings, click `Apply`

?> A Question's `Relationship Rule` auto-fill mechanism will ONLY take effect when rules of that relationship group evaluates to true. 