# Cafe Forms

![Cafe Forms](UserManual/img/1-introduction/cafe-form.png)

In EAB Systems, we value user experience feedback. With every feedback, we come closer to understanding our users’ vast needs and how software iteration/rebranding is a constant. <b>Café Forms</b> is created just for that purpose. We bring customization up the corporate ladder with ease and comfort. Café Forms transfer ownership from our codebase to you. 

## System Overview  

<img style="float: left;padding:10px" alt="system-overview" src="UserManual/img/1-introduction/system-overview.png">

Café Forms is a web-based form management and responses administration system. With Café Forms, you are equipped to easily Build, Design and Publish a form for data collection and reports generation.

You can create forms, quizzes, RSVPs, and more with Cafe Forms. Share a form with people and track responses.
<br><br>

## Building a Form

<img style="float: left;padding:10px" alt="building-a-form" src="UserManual/img/1-introduction/building-a-form.png">

Out of the box, form creation should be a breeze. Café Forms provides an extensive library of Form themes to jumpstart your form building process. From there, each content can be modified to our 20+ Question Types and easily drag-and-dropped for repositioning. With Multilanguage support. Visibility and Validation rules are critical aspects that to flow control. A Quick Preview panel follows you every step of the way.
<br>

## Designing a form  

<img style="float: left;padding:10px" alt="designing-a-form" src="UserManual/img/1-introduction/designing-a-form.png">

<!-- <div style="text-align: justify; font:var(--theme-font)"> -->
Your users’ experience, is also our priority. Having created the content, jump into Design Mode to fine tune the form. Style your forms to the exact look and feel of your brand. Effortlessly reposition questions within our drag-and-drop canvas. Keep your users engaged with interactive steppers and form navigation behaviors settings. For advanced styling, we allow custom CSS to take your styles up a notch.
<br>

## Publishing a form  

<img style="float: left;padding:10px" alt="publishing-a-form" src="UserManual/img/1-introduction/publishing-a-form.png">

Publishing your form couldn’t be any easier. Hitting the “Publish” button gives you an array of sharing options through the right channel to reach your target audience. You may share through a standalone web link, through social media, embed the form in your webpage or instantly update a previously integrated form in your Web/Mobile app.
<br><br>

## System Level Integration  

<img style="float: left;padding:10px" alt="system-level-integration" src="UserManual/img/1-introduction/system-level-integration.png">

Café Forms integrates well to your existing systems with code-level integrations. If opted, we will work closely with your team to embed Café Forms seamlessly on any existing system.
<br><br><br><br>

## Responses and Reporting  

<img style="float: left;padding:10px" alt="responses-and-reporting" src="UserManual/img/1-introduction/responses-and-reporting.png">

Data is the most fundamental driver of any system. Café Forms retrieves and consolidates all Form responses in an organized and visual collection. Navigation is greatly assisted with our complex search functionality in a simple UI. Graphs and charts are drawn automatically to help summarize the responses. Furthermore, information can be exported in your choice of report for further data analysis.