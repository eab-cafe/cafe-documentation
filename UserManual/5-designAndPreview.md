
Design and preview your form in just a few easy steps!

To customize your form according to your audience, you can head to design mode and choose a theme or fine tune background colors, font styles and navigation settings to your preference. The combinations are endless.

## Navigate to Design Mode
1. In [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/), open a form.
2. In the top menu, click Design.  
![design-menu](img/menu-design.png)

?> You will be brought to Cafe Mode: <b>Design Mode</b>
</br>
</br>
(Learn more about modes in [Cafe Modes](UserManual/2-quickStart.md#cafe-modes))

## Apply an existing Theme
1. In [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/), open a form.
2. Go to [Design Mode](UserManual/2-quickStart.md#design-mode)
3. Click on `Default Theme`. A Theme menu will drop open:  
![design-theme](img/5-designAndPreview/design-theme.png ':size=1000%')
4. Choose a theme for your form.
5. When you're happy with the theme, click `Confirm`.  
Your new selected theme will now be applied!

## Personalize Theme

1. In [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/), open a form.
2. Go to [Design Mode](UserManual/2-quickStart.md#design-mode)
3. On the `Design setting` right panel, click on `Personalize Theme`  
![design-personalize-theme](img/5-designAndPreview/design-personalize-theme.png ':size=1000%')
4. To change Font styles and color, click on the item (Form Title, Description, Pages, Sections, Questions)
5. Under Header Background, choose color/upload image to customize your form's header style
6. Under Background, choose color/upload image to customize your form's body content style
7. Under Form Message, choose color to customize your form's messages color style

## Layout Properties
1. In [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/), open a form.
2. Go to [Design Mode](UserManual/2-quickStart.md#design-mode)
3. On the `Design setting` right panel, click on `Layout Properties`  
![design-layout-properties](img/5-designAndPreview/design-layout-properties.png ':size=1000%')

Settings available:

| Setting &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; | Effect &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |
| ------------- | ---------------- |
| Hide Header | Header hides when this setting is switched `on` |
| Section Ordinal Type | Assigns an ordinal to all sections in the form |
| Question Ordinal Type | Assigns an ordinal to all questions in the form. Ordinal starts from 0/A for new section |
| Question alignment in section | Text display on the right part of the slider |

## Section Settings
Section settings can be modified to apply to all sections of a form.

1. In [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/), open a form.
2. Go to [Design Mode](UserManual/2-quickStart.md#design-mode)
3. On the `Design setting` right panel, click on `Section Settings`  
![design-section-setting](img/5-designAndPreview/design-section-setting.png ':size=1000%')

Settings available:

| Setting &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; | Effect &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |
| ------------- | ---------------- |
| Hide Section Header | All section headers in the form hides when this setting is switched `on` |
| Title Alignment | All section titles in the form aligns according to this setting<br> * `left`<br> * `center`<br> * `right` |
| Header Background - Solid Color | Chosen color applies to all section headers in the form |
| Header Background - Image | Uploaded image applies to all section headers in the form |
| Field Background | Chosen color applies to all section backgrounds in the form |

## Individual Section Settings
Section settings can be modified to apply to one section at a time. By first selecting a section, settings changed will ONLY apply to that section in focus.

1. In [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/), open a form.
2. Go to [Design Mode](UserManual/2-quickStart.md#design-mode)
3. On the center panel, click on any section
4. A new section `Individual Section Settings` will appear in the `Design setting` right panel  
![design-indiv-section-setting](img/5-designAndPreview/design-indiv-section-setting.png ':size=1000%')

Settings available:

| Setting &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; | Effect &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |
| ------------- | ---------------- |
| Hide Section Header | Section header hides when this setting is switched `on` |
| Title Alignment | Section title aligns according to this setting <br> * `left`<br> * `center`<br> * `right` |
| Header Background - Solid Color | Chosen color applies to the section header |
| Header Background - Image | Uploaded image applies to the section header |
| Field Background | Chosen color applies to the section background |

## Question Style
Question styles can be modified to apply to all questions of the same type within the form.

1. In [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/), open a form.
2. Go to [Design Mode](UserManual/2-quickStart.md#design-mode)
3. On the `Design setting` right panel, click on `Question Style`  
![design-question-style](img/5-designAndPreview/design-question-style.png ':size=1000%')

Settings available:

| Setting | Effect &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |
| ------------- | ---------------- |
| Variant | Question style can be changed among the few variants available.  Variant selected applies to all questions of the same question type within the form. |

## Individual Question Style
Question styles can be modified to apply to one question at a time. By first selecting a question, settings changed will ONLY apply to that question in focus.

1. In [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/), open a form.
2. Go to [Design Mode](UserManual/2-quickStart.md#design-mode)
3. On the center panel, click on any question
4. A new section `Individual Question Style` will appear in the `Design setting` right panel  
![design-indiv-question-style](img/5-designAndPreview/design-indiv-question-style.png ':size=1000%')

Settings available:

| Setting | Effect &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |
| ------------- | ---------------- |
| Variant | Question style can be changed among the few variants available. Variant selected applies ONLY to the question in focus |

## Navigation Properties

1. In [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/), open a form.
2. Go to [Design Mode](UserManual/2-quickStart.md#design-mode)
3. On the `Design setting` right panel, click on `Navigation Properties`  
![design-navigation-properties](img/5-designAndPreview/design-navigation-properties.png ':size=1000%')

Settings available:

| Setting &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; | Effect &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |
| ------------- | ---------------- |
| Progress Bar Style | Style of the progress bar can be chosen |
| Progress Bar Style - Alignment | Aligns the progress bar according to the setting |
| When answers are modified, mark these pages incomplete | Chosen option will mark pages incomplete based on the settting chosen |
| Page Navigation Button | Text and color can be set for the button in focus (`Back` `Next` `Submit`) |
| Page Navigation Button Alignment | Alignment of the Page Navigation Buttons (`Left` `Center` `Right` `Separate`)|
| Sticky | Navigation buttons will be fixed in place even when scrolling when this setting is `on` |

## Error Behaviors

1. In [Cafe Forms](http://admin-prod.eab-cafe.k8s-inno1/), open a form.
2. Go to [Design Mode](UserManual/2-quickStart.md#design-mode)
3. On the `Design setting` right panel, click on `Error Behaviors`  
![design-error-behaviors](img/5-designAndPreview/design-error-behaviors.png ':size=1000%')

Settings available:

| Setting &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; | Effect &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |
| ------------- | ---------------- |
| Progress Bar Style | Style of the progress bar can be chosen |
| Progress Bar Style - Alignment | Aligns the progress bar according to the setting |
| When answers are modified, mark these pages incomplete | Chosen option will mark pages incomplete based on the settting chosen |
| Page Navigation Button | Text and color can be set for the button in focus<br> * `Back`<br> * `Next`<br> * `Submit` |
| Page Navigation Button Alignment | Alignment of the Page Navigation Buttons<br> *`Left`<br> * `Center`<br> * `Right`<br> * `Separate`|
| Sticky | Navigation buttons will be fixed in place even when scrolling when this setting is `on` |

## Preview your form

### Desktop and Mobile view
!> Phase 4