# Cafe Docs



## Overview

This documentation is based on [docsify](https://docsify.js.org)

## How to run it?

```bash
// Install the CLI globally if you didn't
npm i docsify-cli -g

// Run the dev server
docsify serve .
```