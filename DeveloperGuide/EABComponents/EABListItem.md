# EABListItem

!> **Docs may not up-to-date**

| Last updated | Author | Version | Remarks |
|:------:|:------:|:------:|:------:|
| - | Don To | - | Init |
| 4 Sep 2019 | Felix Chum | 1.0.0 | - |

## Features

A component for List item. Currently there is 1 styles.

## Properties

> - className // CSS class that the root container used
> - [classes](#classes)
> - [isInput](#isInput)
> - [inputProps](#inputProps)
> - [icon](#icon)
> - [value](#value)
> - [label](#label)
> - [isDraggable](#isDraggable)
> - ...otherProps // which is supported by Material-UI

## classes

!> **Following classname you may use for overriding the components**

> - inputField // for overriding the ``<EABTextField>`` root style
> - selected // NOT used, reserved

## isInput

> **Optional**  
> Default value = ``false``

Indicate this listItem is ``TextField`` or not

## inputProps

> **Optional**  
> Default value = ``{}``

Props that will passed to ``EABTextField``

## icon

> **Optional**  
> Default value = ``null``

Icon that used for ``ListItem``, appended at last

## value

!> **ONLY Applicable if isInput === ``true``**  

> **Optional**  
> Default value = ``""``

``ListItem``'s > ``TextField``'s value

## label

!> **ONLY Applicable if isInput === ``false``**

> **Optional**  
> Default value = ``""``

``ListItem``'s value

## isDraggable

> **Optional**
> Default value = ``false``

Activate the dragging style or not  
