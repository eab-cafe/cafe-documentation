# EABCheckBoxGroup

!> **Docs may not up-to-date**

!> **Deprected**  
This will be replaced by [EABCheckBoxGroupImg](EABCheckBoxGroupImg.md)

| Last updated |   Author   | Version |       Remarks        |
| :----------: | :--------: | :-----: | :------------------: |
|  8 Aug 2019  | Felix Chum |  1.0.0  |          -           |
| 29 Aug 2019  | Felix Chum |  1.0.1  | Align Doc in Project |
| 23 Jul 2020  | Felix Chum |  2.0.0  |      Add Deprected      |

## Features

Pending to edit

## Properties

> - className // CSS class that the root container used
> - [classes](#classes)
> - [menuList](#menuList)
> - [value](#checkBoxGroupValue)
> - [additionCheckBox](#additionCheckBox)
> - onChange // Event Listener
> - ...checkBoxGroupProps // which is supported by Material-UI

## classes

Please refer to the CSS section from `styleType`

!> **Following classname you may use for overriding the components**

> - checkBox // [FormControl](https://material-ui.com/api/form-control/#formcontrol-api)
> - checkBoxContainer // [FormControlLabel](https://material-ui.com/api/form-control-label/#formcontrollabel-api)

## menuList

The menuList must be an **Array** and consists the following properties

!> The Control component is using [CheckBox](https://material-ui.com/api/checkbox/) by **default**

> - label // React.node, a text
> - value // the menuItem represent value
> - onClick // onClick EventListener

## value: checkBoxGroupValue

The value is same as **menuList >> menuItem** value

## additionCheckBox

Activate when it required the optional
