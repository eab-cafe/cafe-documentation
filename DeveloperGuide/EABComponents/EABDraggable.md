# EABDraggable

!> **Docs may not up-to-date**

| Last updated |   Author   | Version | Remarks |
| :----------: | :--------: | :-----: | :-----: |
| 22 Jul 2020  | Felix Chum |  1.0.0  | Revamp  |

## Brief

| Type | Description                                        |
| :--- | :------------------------------------------------- |
| Name | EABDraggable.js                                    |
| Path | src/components/eabComponents/EABDraggable/index.js |

!> This is HOC

Create a Draggable with variety of features.

## Example

```js
<EABDraggable
  key={choice.displayId}
  draggableId={choice.displayId}
  index={index}
>
  {rowData}
</EABDraggable>
```

## Properties

| Property                    | Types (propsTypes) | default Value (defaultProps) | Available Options | Hidden |
| :-------------------------- | :----------------- | :--------------------------- | :---------------- | :----: |
| [ref](#ref)                 | React.createRef()  |                              |                   |   ✅   |
| [children](#children)       | React.Child        | `<div />`                    |                   |
| [classes](#classes)         | object             | `{ root: "" }`               |                   |
| [draggableId](#draggableId) | string             | -                            |                   |        |
| [index](#index)             | number             | -                            |                   |

### children

Pass in as the content of the Component to wrap it to be draggable.

### draggableId

?> This property follow the logic on the based component. `react-beautiful-dnd`.  
Please visit to the component description [site](https://github.com/atlassian/react-beautiful-dnd/blob/master/docs/api/draggable.md#draggable-).

For the identification of this component

### index

?> This property follow the logic on the based component. `react-beautiful-dnd`.  
Please visit to the component description [site](https://github.com/atlassian/react-beautiful-dnd/blob/master/docs/api/draggable.md#draggable-).

For the identification of this component.
