# EABErrorBoundary

!> **Docs may not up-to-date**

| Last updated |   Author   | Version | Remarks |
| :----------: | :--------: | :-----: | :-----: |
| 23 Jul 2020  | Felix Chum |  1.0.0  |    -    |

## Brief

| Type | Description                                        |
| :--- | :------------------------------------------------- |
| Name | EABErrorBoundary.js                                |
| Path | src/components/eabComponents/EABErrorBoundary/index.js |

Collect all the errors in the following children

?> Please visit to the component description [site](https://reactjs.org/docs/error-boundaries.html).  