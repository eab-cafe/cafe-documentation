# EABDateTime

!> **Docs may not up-to-date**

| Last updated |   Author   | Version | Remarks |
| :----------: | :--------: | :-----: | :-----: |
|  8 Aug 2019  | Felix Chum |  1.0.0  |    -    |
| 22 Jul 2020  | Felix Chum |  2.0.0  | Revamp  |

## Brief

| Type | Description                                       |
| :--- | :------------------------------------------------ |
| Name | EABDateTime.js                                    |
| Path | src/components/eabComponents/EABDateTime/index.js |

Create a datePicker with variety of features

## Example

```js
<EABDateTime
  ref={timeRef}
  value={inputValue}
  onChange={valueOnChange}
  // displayFormat={dateFormat}
  // showClearIcon
  showClockIcon
  disabled={valueDisable || subConditionDisable}
/>
```

## Properties

| Property                        | Types (propsTypes) | default Value (defaultProps) | Available Options                | Hidden |
| :------------------------------ | :----------------- | :--------------------------- | :------------------------------- | :----: |
| [variant](#variant)             | string             | "standard"                   | "standard", "outlined", "filled" |
| [range](#range)                 | bool               | false                        |                                  |
| [dateProps](#dateProps)         | object             | -                            |                                  |
| [dateEndProps](#dateEndProps)   | object             | -                            |                                  |
| [timeProps](#timeProps)         | object             | -                            |                                  |
| [timeEndProps](#timeEndProps)   | object             | -                            |                                  |
| [clearIcon](#clearIcon)         | React.Element      | `<ClearIcon />`              |                                  |
| [showClearIcon](#showClearIcon) | bool               | true                         |                                  |
| [error](#error)                 | bool               | true                         |                                  |
| [disabled](#disabled)           | bool               | -                            |                                  |

### variant

!> Under Construction

Select the styles would like to use.

### range

If it is range, it will generated more parts with `divider` according to the type.

| Type         | Description |
| :----------- | :---------- |
| dateEndProps | Date Range  |
| timeEndProps | Time Range  |

### dateProps

Supply the props to [#EABDate](EABDate.md)

### dateEndProps

Supply the props to [#EABDate](EABDate.md)

### timeProps

Supply the props to [#EABTime](EABTime.md)

### timeEndProps

Supply the props to [#EABTime](EABTime.md)

### clearIcon

The `Icon` Element to be shown at `Clear` position and trigger to clear the value.

### showClearIcon

Control the visibility of the `clearIcon`. If not shown also indicates that the value cannot be cleared.

### error

Indicates as error

### disabled

Readonly
