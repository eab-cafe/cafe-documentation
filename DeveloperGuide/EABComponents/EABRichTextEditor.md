# EABRichTextEditor

!> **Docs may not up-to-date**

| Last updated |   Author   | Version | Remarks |
| :----------: | :--------: | :-----: | :-----: |
| 23 Jul 2019  | Felix Chum |  1.0.0  |    -    |

## Brief

| Type | Description                                             |
| :--- | :------------------------------------------------------ |
| Name | EABRichTextEditor.js                                    |
| Path | src/components/eabComponents/EABRichTextEditor/index.js |

Create a RichTextEditor with variety of features

## Example

```js
<EABRichTextEditor
  className={styles.editor}
  value={value}
  textAlignment={alignment}
  readOnly
/>
```

## Properties

| Property                                | Types (propsTypes) | default Value (defaultProps) | Available Options | Hidden |
| :-------------------------------------- | :----------------- | :--------------------------- | :---------------- | :----: |
| [ref](#ref)                             | React.createRef()  |                              |                   |
| [className](#className)                 | string             | ""                           |                   |
| [pluginSet](#pluginSet)                 | Array              | null                         |                   |
| [onChange](#onChange)                   | func               | () => {}                     |                   |
| [onChangeAlignment](#onChangeAlignment) | func               | () => {}                     |                   |
| [value](#value)                         | ContentState       |                              |                   |
| [nextValue](#value)                     | string             | null                         |                   |
| [readOnly](#readOnly)                   | bool               | false                        |                   |
| [onNextValueChange](#onNextValueChange) | func               | () => {}                     |                   |
| [...otherProps](#otherProps)            | -                  | -                            | -                 |

### className

CSS classes from stylessheet

### pluginSet

The plugins of draft.js you would like to use.

```js
// Inside the pluginSet Array, the object should be like that
// Details classification of plugins will be in `PluginGenerator`
// It will forward to createMentionPlugin/ createToolbarPlugin
[
    {
        type: MENTION || TOOLBAR,
        config: {
            theme: {},
        },
        options: {},
    },
    ...
]
```

?> This property follow the logic on the based component. `draft-js-static-toolbar-plugin`.  
Please visit to the component description [site](https://www.draft-js-plugins.com/plugin/static-toolbar).

?> This property follow the logic on the based component. `draft-js-mention-plugin`.  
Please visit to the component description [site](https://www.draft-js-plugins.com/plugin/mention).

### onChange

If the contents inside the Editor changed, it will handled by `onStateChange` inside the `js`  
It will convert it current state to storable format.

### onChangeAlignment

If the contents alignment inside the Editor changed, it will handled by [onChangeAlignment](#onChangeAlignment) inside the `js`
Then it will callback the for alignment changed and the alignment status in `LOWER` case.

### value

Must be a valid `ContentState`. Then it will convert the value to the `EditorState`.

### nextValue

!> Deprecated. Should be replaced by ref

If there are new values (nextValue), it will set to the value afterward and callback the [onNextValueChange](#onNextValueChange)

### onNextValueChange

If the nextValue changed, it will callback

### otherProps

?> Any properties supply to this Component (Child).  
They will be forwarded to [draft-js-plugins-editor](https://www.draft-js-plugins.com/).  