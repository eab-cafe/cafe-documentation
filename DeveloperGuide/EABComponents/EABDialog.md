# EABDialog

!> **Docs may not up-to-date**

| Last updated | Author | Version | Remarks |
|:------:|:------:|:------:|:------:|
| - | Don To | - | Init |
| 29 Aug 2019 | Felix Chum | 1.0.0 | - |

## Features

Pending to edit

## Properties

> - className // CSS class that the root container used
> - [classes](#classes)
> - [header](#header)
> - [content](#content)
> - [bottomBar](#bottomBar)
> - ...otherProps // which is supported by Material-UI

## classes

!> **Following classname you may use for overriding the components**

> - root // for overriding the classes.paper
> - header // for overriding the DialogTitle
> - content // for overriding the DialogContent
> - bottomBar // for overriding the DialogActions
> - paperWidthMd  // for overriding the classes.paperWidthMd

## header

The header component, which will show in the ``DialogTitle``

```javascript
<DialogTitle>
/** Your header component will appear in here */
</DialogTitle>
```

## content

The contnet component, which will show in the ``DialogContent``

```javascript
<DialogContent>
/** Your content component will appear in here */
</DialogContent>
```

## bottomBar

The bottomBar component, which will show in the ``DialogActions``

```javascript
<DialogActions>
/** Your bottomBar component will appear in here */
</DialogActions>
```
