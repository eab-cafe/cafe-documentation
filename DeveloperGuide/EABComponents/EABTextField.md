# EABTextField

!> **Docs may not up-to-date**

| Last updated |   Author   | Version | Remarks |
| :----------: | :--------: | :-----: | :-----: |
| 22 Jul 2020  | Felix Chum |  1.0.0  |    -    |

## Brief

| Type | Description                                        |
| :--- | :------------------------------------------------- |
| Name | EABTextField.js                                    |
| Path | src/components/eabComponents/EABTextField/index.js |

Create an `<input />` with variety of features

## Example

```js
<EABTextField
    // styleType="CLICK_TO_EDIT"
    fontSize={MEDIUM}
    className={classnames(styles.textInputField)}
    classes={{
    inputClass: styles.textInput
    }}
    placeholder={placeholder}
    disableUnderline
    error={hasErr}
    autoFocus={autoFocus}
    onChange={onEditInputValue(cellKey)}
    value={value}
/>
```

## Properties

| Property                                                 | Types (propsTypes) | default Value (defaultProps) | Available Options           |
| :------------------------------------------------------- | :----------------- | :--------------------------- | :-------------------------- |
| [styleType](#styleType)                                  | string             | ""                           | CLICK_TO_EDIT , null        |
| [className](#className)                                  | string             | ""                           |
| [fontSize](#fontSize)                                    | string             | "medium"                     | "small" ,"medium" , "large" |
| [classes](#classes)                                      | shape              | `{ inputClass: "" }`         |                  |
| [value](#value)                                          | string             | null                         |
| [defaultValue](#defaultValue)                            | string             | ""                           |
| [multiline](#multiline)                                  | bool               | false                        | true, false                 |
| [rowsMin](#rowsMin)                                      | number             | undefined                    |
| [rowsMax](#rowsMax)                                      | number             | undefined                    |
| [disableUnderline](#disableUnderline)                    | bool               | false                        |
| [onChange](#onChange)                                    | func               | () => {}                     |
| [onTextEnter](#onTextEnter)                              | func               | () => {}                     |
| [onTextTab](#onTextTab)                                  | func               | () => {}                     |
| [onTextShiftTab](#onTextShiftTab)                        | func               | () => {}                     |
| [onEnterLineBreaks](#onEnterLineBreaks)                  | func               | () => {}                     |
| [onTextShiftTab](#onTextShiftTab)                        | func               | () => {}                     |
| [formatType](#formatType)                                | constant   | ANY        | ANY, NUMERIC, CHARACTER      |
| [maxLength](#maxLength)                                  | number             | undefined                    |
| [...otherProps](#otherProps) | -                  | -                            |

### styleType

| StyleType     | Behaviours                                       |
| :------------ | :----------------------------------------------- |
| CLICK_TO_EDIT | `const clickToEdit = () => {` will be used       |
| null          | `<TextField />` from `@material-ui` will be used |

### className

CSS classes from stylessheet

### fontSize

Following CSS classes will be used

```CSS
.small {
  font-size: var(--fontSizeS);
}

.medium {
  font-size: var(--fontSizeM);
}

.large {
  font-size: var(--fontSizeL);
}
```

### classes

Following parameters of property can be used to inject the styles of various components

| Property   | Description |
| :--------- | :---------- |
| inputClass | `<input />` |

### value

The `str` that used in `<input />`

### defaultValue

When there is no [value](#value), this property's value will be used

### multiline

Control the number of line inputs to be shown

### rowsMin

Based on [multiline](#multiline), its control number of rows will be appear at **least** for this `<input />`

### rowsMax

Based on [multiline](#multiline), its control number of rows will be appear at **MOST** for this `<input />`

### disableUnderline

Show the `underline` or not

### onChange

If the [value](#value) inside `<input />` changed, callback this function.

### onTextEnter

If pressed `Enter` key on the Keyboard, callback this function.

### onTextTab

If pressed `Tab` key on the keyboard, callback this function.

### onTextShiftTab

If pressed `Tab` and `Shift` keys on the keyboard **simultaneously**, callback this function.

### formatType

| Type      | Description                                     |
| :-------- | :---------------------------------------------- |
| ANY       | Any characters, number and symbol can be typed  |
| NUMERIC   | Only numeric can be typed, which is [0-9]       |
| CHARACTER | Only characters can be typed, which is [a-zA-Z] |

### maxLength

Maximum number of characters can be typed.

### otherProps

?> Any properties supply to this Component (Child).  
They will be forwarded to `@material-ui` [TextField](https://material-ui.com/api/text-field/#props).  

!> Make sure the properties used are compatible to our [@material-ui version](/DeveloperGuide/dev-overview.md#notes-for-devs)