# EABNavigate

!> **Docs may not up-to-date**

| Last updated | Author | Version | Remarks |
|:------:|:------:|:------:|:------:|
| 4 Sep 2019 | Felix Chum | 1.0.0 | - |

## Features

Interceptor for Navigation.  
Use it instead of ``@react/router`` > navigate as it will  pre-check the any prerequisites to make a navigation  

## Properties

!> **No properties should be assigned for this component!**  
