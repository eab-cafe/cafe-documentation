# EABExpansionPanel

!> **Docs may not up-to-date**

| Last updated | Author | Version | Remarks |
|:------:|:------:|:------:|:------:|
| - | Don To | - | Init |
| 4 Sep 2019 | Felix Chum | 1.0.0 | - |

## Features

A component for List item. Currently there is 2 styles.

## Properties

> - className // CSS class that the root container used
> - [styleType](#styletype)
> - [classes](#classes)
> - [header](#header)
> - [content](#content)
> - [expansionBottom](#expansionBottom)
> - [disableBorder](#disableborder)
> - [expanded](#expanded)
> - [expandIcon](#expandicon)
> - [onExpandChange](#onexpandchange)
> - ...otherProps // which is supported by Material-UI

## styleType

The component supported the following types.  
Each of the following variable is a constant imported from ``constants/eabComponent``

!> **Unsupported / Non-defined type** will throw an exception

> - FORM_SECTION // Form-specified
> - FORM_QUESTION // Form-specified
> - PANEL_EXPERSION

## classes

!> **Following classname you may use for overriding the components** <br/> **ONLY Applicable if styleTypes === ``FORM_QUESTION``**

> - header // for overriding the ExpansionPanelSummary
> - content // for overriding the ExpansionPanelDetails
> - expansionBottom // appended div for ExpansionPanel, in the bottom

## header

The header component, which will show in the ``ExpansionPanelSummary``

```javascript
<ExpansionPanelSummary>
/** Your header component will appear in here */
</ExpansionPanelSummary>
```

## content

The contnet component, which will show in the ``ExpansionPanelDetails``

```javascript
<ExpansionPanelDetails>
/** Your content component will appear in here */
</ExpansionPanelDetails>
```

## expansionBottom

> **Optional**  
> Default value = ``null``

!> **ONLY Applicable if styleTypes === ``FORM_QUESTION``**  

The expansionBottom component, which will be appended after the ``ExpansionPanelDetails``, wrapped by ``div``  

```javascript
</ExpansionPanelDetails>
<div>
/** Your expansionBottom component will appear in here */
</div>
```

## disableBorder

> **Optional**  
> Default value =  ``false``

!> **ONLY Applicable if styleTypes === ``FORM_QUESTION``**  

Show the border or not in ``<ExpansionSummary>``, applying style ``.questionExpansionContainerBorderless``

## expanded

> Default value =  ``false``

Control the ``ExpansionPanel`` expand or not

## expandIcon

> **Optional**  
> Default value = ``<div />``

!> **ONLY Applicable if styleTypes === ``PANEL_EXPERSION``**  

The icon that represent expanded or not, used in ``<ExpansionSummary>``  

## onExpandChange

> **Optional**  
> Default value = ``() => {}``
> 
Callback function if the ``ExpansionPanel`` changed.
