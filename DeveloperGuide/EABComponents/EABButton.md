# EABButton

!> **Docs may not up-to-date**

| Last updated |   Author   | Version |       Remarks        |
| :----------: | :--------: | :-----: | :------------------: |
|      -       |   Don To   |    -    |         Init         |
|  8 Aug 2019  | Felix Chum |  1.0.0  |          -           |
| 29 Aug 2019  | Felix Chum |  1.0.1  | Align Doc in Project |

This Component is controller of type, and default size of the button. Other than that, it is only Material UI's FAB, Button, IconButton

## Features

1. Resize all the size represent

## Properties

> - className // CSS class that the root container used
> - [styleType](#styleType)
> - [classes](#classes)
> - [children](#children)
> - [size](#size)
> - ...otherProps // which is supported by Material-UI

## styleType

The component supported the following types.  
Each of the following variable is a constant imported from `constants/eabComponent`

!> **Unsupported / Non-defined type** will throw an exception

> - FAB  
>   from [Material-UI Fab](https://material-ui.com/ru/api/fab/)
> - ICON_BUTTON  
>   from [Material-UI IconButton](https://material-ui.com/ru/api/icon-button/#iconbutton-api)
> - BUTTON  
>   from [Material-UI Button](https://material-ui.com/ru/api/button/#button-api)

## size

Just like what size Material-UI defined.  
Each of the following variable is a constant imported from `constants/eabComponent`

> - SMALL
> - MEDIUM
> - LARGE

## classes

Please refer to the CSS section by each `styleType`

!> **Following classname is reserved**

> - Button // styleType === BUTTON
>   - contained

## children

Child component wrapped within the above-mentioned `styleType`
