# EABLoading

!> **Docs may not up-to-date**

| Last updated | Author | Version | Remarks |
|:------:|:------:|:------:|:------:|
| 4 Sep 2019 | Felix Chum | 1.0.0 | - |

## Features

Simple Circular Loading Component

## Properties

> - className // CSS class that the root container used  
