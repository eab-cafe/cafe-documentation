# EABTime

!> **Docs may not up-to-date**

| Last updated |   Author   | Version | Remarks |
| :----------: | :--------: | :-----: | :-----: |
|  8 Aug 2019  | Felix Chum |  1.0.0  |    -    |
| 22 Jul 2020  | Felix Chum |  2.0.0  | Revamp  |

## Brief

| Type | Description                                   |
| :--- | :-------------------------------------------- |
| Name | EABTime.js                                    |
| Path | src/components/eabComponents/EABTime/index.js |

Create a `<input />` to input the time with variety of features

## Example

```js
<EABTime
  ref={timeRef}
  value={inputValue}
  onChange={valueOnChange}
  // displayFormat={dateFormat}
  // showClearIcon
  showClockIcon
  disabled={valueDisable || subConditionDisable}
/>
```

## Properties

| Property                                | Types (propsTypes)                               | default Value (defaultProps) | Available Options     | Hidden |
| :-------------------------------------- | :----------------------------------------------- | :--------------------------- | :-------------------- | :----: |
| [className](#className)                 | string                                           | ""                           |                       |
| [calendarClassName](#calendarClassName) | string                                           | ""                           |                       |
| [onChange](#onChange)                   | func                                             | () => {}                     |                       |
| [value](#value)                         | string                                           | ""                           |                       |
| [clockIcon](#clockIcon)                 | React.Element                                    | `<AccessTimeIcon />`              |                       |
| [clearIcon](#clearIcon)                 | React.Element                                    | `<ClearIcon />`              |                       |
| [showClockIcon](#showClockIcon)   | bool                                             | false                        |                       |
| [showClearIcon](#showClearIcon)         | bool                                             | false                        |                       |
| [locale](#locale)                       | string                                           | "en-US"                      | Any IETF Language str |
| [maxDate](#maxDate)                     | [ISO Date](#https://www.w3.org/TR/NOTE-datetime) |                              |                       |
| [minDate](#minDate)                     | [ISO Date](#https://www.w3.org/TR/NOTE-datetime) |                              |                       |
| [showLeadingZeros](#showLeadingZeros)   | bool                                             | true                         |                       |   ✅   |
| [...otherProps](#otherProps)            | -                                                | -                            | -                     |

### className

?> This property follow the logic on the based component. `react-time-picker`.  
Please visit to the component description [site](https://github.com/wojtekmaj/react-time-picker#props).

CSS classes from stylessheet

### calendarClassName

?> This property follow the logic on the based component. `react-time-picker`.  
Please visit to the component description [site](https://github.com/wojtekmaj/react-time-picker#props).

CSS classes from stylessheet for **calendar**

### onChange

?> This property follow the logic on the based component. `react-time-picker`.  
Please visit to the component description [site](https://github.com/wojtekmaj/react-time-picker#props).

If the [value](#value) inside datePicker changed, callback this function.

### value

?> This property follow the logic on the based component. `react-time-picker`.  
Please visit to the component description [site](https://github.com/wojtekmaj/react-time-picker#props).

The `isoDate` that used in the object

### clockIcon

?> This property follow the logic on the based component. `react-time-picker`.  
Please visit to the component description [site](https://github.com/wojtekmaj/react-time-picker#props).

The `Icon` Element to be shown at `clock` position and trigger to open the calendar.

### clearIcon

?> This property follow the logic on the based component. `react-time-picker`.  
Please visit to the component description [site](https://github.com/wojtekmaj/react-time-picker#props).

The `Icon` Element to be shown at `clock` position and trigger to clear the value.

### showClockIcon

Control the visibility of the `clockIcon`. If not shown also indicates that the calendar cannot open/close.

### showClearIcon

Control the visibility of the `clearIcon`. If not shown also indicates that the value cannot be cleared.

### locale

?> This property follow the logic on the based component. `react-time-picker`.  
Please visit to the component description [site](https://github.com/wojtekmaj/react-time-picker#props).

?> For available IETF Local List, you may visit [here](https://datahub.io/core/language-codes/r/3.html)

### maxDate

?> This property follow the logic on the based component. `react-time-picker`.  
Please visit to the component description [site](https://github.com/wojtekmaj/react-time-picker#props).

Maximum Date available to select.

### minDate

?> This property follow the logic on the based component. `react-time-picker`.  
Please visit to the component description [site](https://github.com/wojtekmaj/react-time-picker#props).

Minimum Date available to select.

### showLeadingZeros

?> This property follow the logic on the based component. `react-time-picker`.  
Please visit to the component description [site](https://github.com/wojtekmaj/react-time-picker#props).

Append the `0` if there are no `tens-digit`.

### otherProps

?> Any properties supply to this Component (Child).  
They will be forwarded to [react-time-picker](https://github.com/wojtekmaj/react-time-picker#props).
