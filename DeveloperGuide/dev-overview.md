# Quick Start

!> **Docs may not up-to-date**

| Last updated |   Author   | Version | Remarks |
| :----------: | :--------: | :-----: | :-----: |
|  8 Jul 2020  | Felix Chum |  1.0.0  |    -    |

Welcome to the cafe project!

## Notes for Devs

Project Essentials:

- Spec/Design Document: smb://192.168.222.56/Cafe Project/v16
- Cafe Admin Repo: [http://192.168.222.60:8001/eab-cafe/Cafe](http://192.168.222.60:8001/eab-cafe/Cafe)

| Server name | Role    | Link                                                                         |
| ----------- | ------- | ---------------------------------------------------------------------------- |
| Dev         | For Dev | [http://admin-dev.eab-cafe.k8s-inno1/](http://admin-dev.eab-cafe.k8s-inno1/) |
| Sit         | For QA  | [http://admin-sit.eab-cafe.intraeab/](http://admin-sit.eab-cafe.intraeab/)   |
| Prod        | Stable  | [http://admin-prod.eab-cafe.intraeab/](http://admin-prod.eab-cafe.intraeab/) |

Cafe is consist of **5** **_SMALL_** projects, including

| Name                         |                                 Link                                  |                     QuickStart                     | Package.json                                                                            | Swagger                                                                                                                  |
| :--------------------------- | :-------------------------------------------------------------------: | :------------------------------------------------: | --------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------ |
| Cafe-Admin-Configurator      |         [Click Me](http://192.168.222.60:8001/eab-cafe/Cafe)          |  [HERE](/DeveloperGuide/QuickStart/cafe-admin.md)  | [HERE](http://192.168.222.60:8001/eab-cafe/Cafe/blob/DEV/package.json)                  |
| Cafe-Display-Interface       | [Click Me](http://192.168.222.60:8001/eab-cafe/cafe-interface-engine) | [HERE](/DeveloperGuide/QuickStart/cafe-display.md) | [HERE](http://192.168.222.60:8001/eab-cafe/cafe-interface-engine/blob/DEV/package.json) |
| Cafe-API                     |       [Click Me](http://192.168.222.60:8001/eab-cafe/cafe-api)        |   [HERE](/DeveloperGuide/QuickStart/cafe-api.md)   | [HERE](http://192.168.222.60:8001/eab-cafe/cafe-api/blob/DEV/package.json)              | [HERE](http://docs.swagger.k8s-inno1/?location=http://docs.swagger.k8s-inno1/yaml?path=@eab-cafe/api/documentation.yaml) |
| Cafe-Common                  | [Click Me](http://192.168.222.60:8001/eab-cafe/cafe-helper-functions) | [HERE](/DeveloperGuide/QuickStart/cafe-common.md)  | [HERE](http://192.168.222.60:8001/eab-cafe/common/blob/DEV/package.json)                |
| Cafe-Docs **(This Project)** |     [Click Me](http://192.168.222.60:8001/eab-documentation/cafe)     |                         -                          | -                                                                                       |

**If you cannot access the projects, feel free to ask [@Samuel](https://teams.microsoft.com/l/chat/0/0?users=samuel.cheng@eabsystems.com.hk) to grant the access for you**

<!-- **Should you have any technical difficulties, you may seek help from [@Felix](https://teams.microsoft.com/l/chat/0/0?users=felix.chum@eabsystems.com.hk)** -->

**Any design concerns should go to [@Spacey](https://teams.microsoft.com/l/chat/0/0?users=spacey.ho@eabsystems.com.hk) or [@Samuel](https://teams.microsoft.com/l/chat/0/0?users=samuel.cheng@eabsystems.com.hk)**

- Gitlab Issue Board: [http://192.168.222.60:8001/eab-cafe/Cafe/boards](http://192.168.222.60:8001/eab-cafe/Cafe/boards)
- Design source:

  - OLD XD [https://xd.adobe.com/spec/bae35be1-9902-4ac2-981a-32862c5c3d80-efb0/](https://xd.adobe.com/spec/bae35be1-9902-4ac2-981a-32862c5c3d80-efb0/)
  - Zeplin [https://app.zeplin.io/projects](https://app.zeplin.io/projects)

- Cafe Admin Configurator has these technologies setup:
  - Sentry http://192.168.222.60:8001/eab-cafe/Cafe/issues/102

## Notes for Desigers

For unifying the styles within Cafe projects,  
we are using mostly identical stylesheet which you can easily find it in our UI projects, Admin and Display.  
It would be greatly helpful if you can following the variable naming convention.  
If you don't find anyone of them matches your needs, feel free to add one, and notice to Devs if possible.

Attached the direct link to the stylesheet of our projects.

As as DEVs,
**If you cannot access the projects, feel free to ask [@Samuel](https://teams.microsoft.com/l/chat/0/0?users=samuel.cheng@eabsystems.com.hk) to grant the access for you**

Generally, the common theme of Cafe are stored in the `default.css` among projects.  
You may find the exact path according to the following table.

| Project | URL                                                                                                     |
| :------ | :------------------------------------------------------------------------------------------------------ |
| Admin   | [default.css](http://192.168.222.60:8001/eab-cafe/Cafe/blob/DEV/src/theme/default.css)                  |
| Display | [default.css](http://192.168.222.60:8001/eab-cafe/cafe-interface-engine/blob/DEV/src/theme/default.css) |
| Common  | -                                                                                                       |
| API     | -                                                                                                       |

Thank you very much for your cooperation and making the Devs life easier :)!

## Object Structure Matrix

?> This part is moved to [Features](/DeveloperGuide/Features/overview.md)
