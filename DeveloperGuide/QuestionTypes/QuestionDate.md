# Date (QuestionDate)

[Back To QuestionList Outline](DeveloperGuide/QuestionTypes/overview.md?id=outline)

!> **Docs may not up-to-date**

| Last updated |   Author   | Version | Remarks |
| :----------: | :--------: | :-----: | :-----: |
|  8 Jul 2020  | Felix Chum |  1.0.0  |    -    |

## Overview

To generate a `<input type="date" />` inside the form with customization

### Advance Features Availability

?> Know more in [Features](DeveloperGuide/Features/overview.md)

| Features (Rule)    | Can be used as rules? | Can be activated? |
| :----------------- | :-------------------: | :---------------: |
| Visibility Rules   |          ✅           |        ✅         |
| Validation Rules   |          ✅           |        ✅         |
| Relationship Rules |          ✅           |        ✅         |

| Features (Non-Rule)   | Applicable |
| :-------------------- | :--------: |
| Design Setting        |     ✅     |
| Dynamic Styles        |     ❔     |
| Multi Language        |     ✅     |
| Import Reference ID   |     ✅     |
| Internal Reference ID |     ✅     |
| Able be Sub Question  |     ✅     |

## Cafe Admin

| Type | Description                                                        |
| :--- | :----------------------------------------------------------------- |
| Name | QuestionButton.js                                                  |
| Path | src/routes/Form/FormEdit/components/questionType/QuestionButton.js |

QuestionButton (Admin) uses the following EABComponent(s)

| Components       | Link                                                      | Deprecated |
| :--------------- | :-------------------------------------------------------- | :--------: |
| EABCheckBoxGroup | [Click Me](DeveloperGuide/EABComponents/EABCheckBoxGroup) |
| EABPopoverList   | [Click Me](DeveloperGuide/EABComponents/EABPopoverList)   |
| EABSegmentButton | [Click Me](DeveloperGuide/EABComponents/EABSegmentButton) |
| EABTextField     | [Click Me](DeveloperGuide/EABComponents/EABTextField)     |
| OLDEABDateTime   | [Click Me](DeveloperGuide/EABComponents/OLDEABDateTime)   |     ⚠️     |

QuestionButton (Admin) uses the following Util(s)

| Utils         | Link                                           |
| :------------ | :--------------------------------------------- |
| questionUtils | [Click Me](DeveloperGuide/Utils/questionutils) |
| formService   | [Click Me](DeveloperGuide/Utils/formService)   |

### Functionality

![questionDate_layout](media/questionDate_01.png)

#### Select Date/Time Type

| Dropdown Option | Variable            | Description                          |
| :-------------- | :------------------ | :----------------------------------- |
| Date            | `INPUT_DATE`        | Input Format(Date), Min and Max Date |
| Time            | `INPUT_TIME`        | Input Min and Max Time               |
| Date & Time     | `INPUT_DATE_N_TIME` | Input the above                      |

#### Format (Date)

!> This part change depends on the [Select Date/Time Type](#select-datetime-type) you've chosen

| Dropdown Option | Variable       | Description                      |
| :-------------- | :------------- | :------------------------------- |
| dd/mm/yyyy      | `DATE_FORMAT1` | Show that Date like `20/06/2020` |
| mm/dd/yyyy      | `DATE_FORMAT2` | Show that Date like `06/20/2020` |
| mm/yyyy         | `DATE_FORMAT3` | Show that Date like `06/2020`    |
| yyyy/mm/dd      | `DATE_FORMAT4` | Show that Date like `2020/06/20` |

#### Min Date / Max Date

!> This part change depends on the [Select Date/Time Type](#select-datetime-type) you've chosen

![questionDate_MinDate](media/questionDate_02.png)

| Dropdown Option   | Variable            | Description                                       |
| :---------------- | :------------------ | :------------------------------------------------ |
| None              | `DATE_NONE`         | No special Rule                                   |
| Form Filling Date | `DATE_FORM_FILLING` | Set the date correspond to that form filling date |
| Set Specific Date | `DATE_SET_SPECIFIC` | Set the exact date                                |

##### Min Date / Max Date - Operator

!> This part only applicable if the `Min Date` OR `Max Date` is `DATE_FORM_FILLING`

| Dropdown Option | Variable        | Description                                  |
| :-------------- | :-------------- | :------------------------------------------- |
| N/A             | `DATE_NA`       | No special Rule                              |
| +               | `DATE_ADD`      | Set the date more than the form filling date |
| -               | `DATE_SUBTRACT` | Set the date less than the form filling date |

##### Min Date / Max Date - Value

!> This part only applicable if the `Min Date` OR `Max Date` is `DATE_FORM_FILLING`

Number of [unit](#min-date-max-date-unit) +/- to the form filling date

##### Min Date / Max Date - Unit

!> This part only applicable if the `Min Date` OR `Max Date` is `DATE_FORM_FILLING`

| Dropdown Option | Variable      | Description       |
| :-------------- | :------------ | :---------------- |
| Days            | `DATE_DAYS`   | Set unit as Day   |
| Weeks           | `DATE_WEEKS`  | Set unit as Week  |
| Months          | `DATE_MONTHS` | Set unit as Month |
| Years           | `DATE_YEARS`  | Set unit as Year  |

#### Min Time / Max Time

!> This part change depends on the [Select Date/Time Type](#select-datetime-type) you've chosen

Set the `Min Time` OR `Max Time` by exact time

#### Ranged Input

!> This part change depends on the [Select Date/Time Type](#select-datetime-type) you've chosen

If checked, the input would be in range input. From date A to date B.

## Cafe Display

| Type | Description                                                           |
| :--- | :-------------------------------------------------------------------- |
| Name | QuestionDate.js                                                  |
| Path | src/routes/Form/FormEdit/components/questionType/QuestionDate.js |

QuestionDate (Admin) uses the following EABComponent(s)

| Components    | Link                                                   |
| :------------ | :----------------------------------------------------- |
| EABDateTime | [Click Me](DeveloperGuide/EABComponents/EABDateTime) |

QuestionDate (Admin) uses the following Util(s)

| Utils         | Link                                           |
| :------------ | :--------------------------------------------- |
| questionUtils | [Click Me](DeveloperGuide/Utils/questionutils) |

Once the user types, it will handled by the following function.



## Cafe Common
