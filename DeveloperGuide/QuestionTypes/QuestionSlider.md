# Slider (QuestionSlider**Field**)

[Back To QuestionList Outline](DeveloperGuide/QuestionTypes/overview.md?id=outline)

!> **Docs may not up-to-date**

| Last updated |   Author   | Version | Remarks |
| :----------: | :--------: | :-----: | :-----: |
| 21 Jul 2020  | Felix Chum |  1.0.0  |    -    |

## Overview

To generate a `Slider` inside the form with customization

### Advance Features Availability

?> Know more in [Features](DeveloperGuide/Features/overview.md)

| Features (Rule)    | Can be used as rules? | Can be activated? |
| :----------------- | :-------------------: | :---------------: |
| Visibility Rules   |          ✅           |        ✅         |
| Validation Rules   |          ✅           |        ✅         |
| Relationship Rules |          ✅           |        ✅         |

| Features (Non-Rule)   | Applicable |
| :-------------------- | :--------: |
| Design Setting        |     ✅     |
| Dynamic Styles        |     ❔     |
| Multi Language        |     ✅     |
| Import Reference ID   |     ✅     |
| Internal Reference ID |     ✅     |
| Able be Sub Question  |     ❌     |

## Cafe Admin

| Type | Description                                                                 |
| :--- | :-------------------------------------------------------------------------- |
| Name | QuestionSlider**Field**.js                                                  |
| Path | src/routes/Form/FormEdit/components/questionType/QuestionSlider**Field**.js |

QuestionSlider**Field** (Admin) uses the following EABComponent(s)

| Components       | Link                                                      |
| :--------------- | :-------------------------------------------------------- |
| EABButton        | [Click Me](DeveloperGuide/EABComponents/EABButton)        |
| EABSegmentButton | [Click Me](DeveloperGuide/EABComponents/EABSegmentButton) |
| EABListItem      | [Click Me](DeveloperGuide/EABComponents/EABListItem)      |
| EABTextField     | [Click Me](DeveloperGuide/EABComponents/EABTextField)     |

| Utils             | Link                                               |
| :---------------- | :------------------------------------------------- |
| questionUtils     | [Click Me](DeveloperGuide/Utils/questionutils)     |
| formService       | [Click Me](DeveloperGuide/Utils/formService)       |
| langUtils         | [Click Me](DeveloperGuide/Utils/langUtils)         |
| visibilityService | [Click Me](DeveloperGuide/Utils/visibilityService) |

### Functionality

![questionSlider_layout](media/questionSlider_01.png)

#### Slider Styles

|        Toggle steps         |    Variable     |                 Description                 | Range | Label | Input |
| :-------------------------: | :-------------: | :-----------------------------------------: | ----- | ----- | ----- |
|            Basic            |  SLIDER_BASIC   |                Basic Slider                 | ✅    | ❌    | ❌    |
|        Basic + Label        | SLIDER_BASIC_L  | Users may modify the labels (e.g., min max) | ✅    | ✅    | ❌    |
| Basic + Label + Value Input | SLIDER_BASIC_LV |     Users may enter the value directly      | ✅    | ✅    | ✅    |
|           Scales            |  SLIDER_SCALES  |                 Auto scales                 | ✅    | ❌    | ❌    |
|        Custom Values        |    SLIDER_CV    |  Custom defined values (No auto increment)  | -     | -     | -     |

#### Range

Adjust the slider range from min to Maximum

Look for the following variables:

```javascript
const [rangeFrom, setRangeFrom] = useState(
  _.get(formatProps, "range.start", "0")
);
const [rangeTo, setRangeTo] = useState(_.get(formatProps, "range.end", "10"));
```

### Label

Add other description more than the range

Look for the following variables:

```javascript
const [labelMin, setLabelMin] = useState(_.get(formatProps, "label.start", ""));
const [labelMax, setLabelMax] = useState(_.get(formatProps, "label.end", ""));
```

### Scales

With scales, there will be label insert below each step.

### Question Data Structure

?> For Base Question Strucutre, please head to [QuestionBaseStructure](DeveloperGuide/Structures/QuestionBaseStructure)

This questionType will store the question by the following format

```javascript
/* UPPER CASE WORDS ARE CONSTANTS */
answers.update({
  type: answersStyle,
  value: newValue,
  formatProps: {
    range: {
      start: rangeFrom,
      end: rangeTo,
    },
    label: {
      start: labelMin,
      end: labelMax,
    },
  },
  styleProps: {
    textDecoration: {
      prefix,
      postfix,
    },
  },
  helpTextProps,
});
```

## Cafe Display

| Type | Description                                                                        |
| :--- | :--------------------------------------------------------------------------------- |
| Name | QuestionSlider**Field**.js                                                         |
| Path | src/components/eabComponents/FormDisplayCard/components/QuestionSlider**Field**.js |

QuestionSlider**Field** (Display) uses the following EABComponent(s)

| Components  | Link                                                 |
| :---------- | :--------------------------------------------------- |
| EABDropdown | [Click Me](DeveloperGuide/EABComponents/EABDropdown) |
| EABSlider   | [Click Me](DeveloperGuide/EABComponents/EABSlider) |

QuestionSlider**Field** (Display) uses the following Util(s)

| Utils         | Link                                           |
| :------------ | :--------------------------------------------- |
| questionUtils | [Click Me](DeveloperGuide/Utils/questionutils) |
| langUtils     | [Click Me](DeveloperGuide/Utils/langUtils)     |

Once the users moved, it will handled by the following function.

?> Resources may override the exist settings

```javascript
  const menuOnClick = newValue => {
```

### Answer Data Structure

?> For Base Answer Structure, please head to [AnswerBaseStructure](DeveloperGuide/Structures/AnswerBasestructure)

```javascript
/* UPPER CASE WORDS ARE CONSTANTS */
  const onChangeInput = value => {
    setResponseText(`${value}`);
  };

  propsValue.value = responseText ? Del_Comma(responseText) : "";
```

## Cafe Common

N/A
