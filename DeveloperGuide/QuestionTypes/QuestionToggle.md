# Toggle (QuestionToggle)

[Back To QuestionList Outline](DeveloperGuide/QuestionTypes/overview.md?id=outline)

!> **Docs may not up-to-date**

| Last updated |   Author   | Version | Remarks |
| :----------: | :--------: | :-----: | :-----: |
|  8 Jul 2020  | Felix Chum |  1.0.0  |    -    |

## Overview

To generate a toggle button (switch) inside the form with customization.

### Advance Features Availability

?> Know more in [Features](DeveloperGuide/Features/overview.md)

| Features (Rule)    | Can be used as rules? | Can be activated? |
| :----------------- | :-------------------: | :---------------: |
| Visibility Rules   |          ✅           |        ✅         |
| Validation Rules   |          ✅           |        ✅         |
| Relationship Rules |          ✅           |        ✅         |

| Features (Non-Rule)   | Applicable |
| :-------------------- | :--------: |
| Design Setting        |     ✅     |
| Dynamic Styles        |     ❔     |
| Multi Language        |     ✅     |
| Import Reference ID   |     ❌     |
| Internal Reference ID |     ✅     |
| Able be Sub Question  |     ❌     |

## Cafe Admin

| Type | Description                                                        |
| :--- | :----------------------------------------------------------------- |
| Name | QuestionToggle.js                                                  |
| Path | src/routes/Form/FormEdit/components/questionType/QuestionToggle.js |

QuestionToggle (Admin) uses the following EABComponent(s)

| Components       | Link                                                      |
| :--------------- | :-------------------------------------------------------- |
| EABListItem      | [Click Me](DeveloperGuide/EABComponents/EABListItem)      |
| EABSegmentButton | [Click Me](DeveloperGuide/EABComponents/EABSegmentButton) |

QuestionToggle (Admin) uses the following Util(s)

| Utils         | Link                                           |
| :------------ | :--------------------------------------------- |
| questionUtils | [Click Me](DeveloperGuide/Utils/questionutils) |

### Functionality

![questionToggle_layout](media/questionToggle_01.png)

#### Toggle steps

?> Once the user change the `Toogle steps`,  
the out scoped data (3 rows of 2 steps) will be discarded.

User can choose the number of steps inside the button.

| Toggle steps |    Variable     | Minimum No. of Labels |
| :----------: | :-------------: | :-------------------: |
|   2 steps    |  INPUT_TWOSTEP  |           2           |
|   3 steps    | INPUT_THREESTEP |           3           |

#### Labels

User can insert the label in the boxes, the behaviour work as same as `EABListItem`

### Question Data Structure

?> For Base Question Strucutre, please head to [QuestionBaseStructure](DeveloperGuide/Structures/QuestionBaseStructure)

This questionType will store the question by the following format

```javascript
/* UPPER CASE WORDS ARE CONSTANTS */
const choices = [
    {
        id: /* the displayId */,
        type: CHOICES,
        createAt: moment().unix()
    },
    ...
];

const question = {
    ...,
    answers: {
        value: {
            [VALUEARRAY]: choices
        },
        type: INPUT_TWOSTEP || INPUT_THREESTEP,
        helpTextProps: {
            /* The HelperText */
        }
    }
}
```

## Cafe Display

| Type | Description                                                               |
| :--- | :------------------------------------------------------------------------ |
| Name | QuestionToggle.js                                                         |
| Path | src/components/eabComponents/FormDisplayCard/components/QuestionToggle.js |

QuestionToggle (Display) uses the following EABComponent(s)

| Components       | Link                                                      |
| :--------------- | :-------------------------------------------------------- |
| EABSegmentButton | [Click Me](DeveloperGuide/EABComponents/EABSegmentButton) |

QuestionToggle (Display) uses the following Util(s)

| Utils         | Link                                           |
| :------------ | :--------------------------------------------- |
| langUtils     | [Click Me](DeveloperGuide/Utils/langUtils)     |
| questionUtils | [Click Me](DeveloperGuide/Utils/questionutils) |

Once the user clicked the button, it will handled by the following function.

```javascript
/* The value is passed wrapped as e.target.value */
const onChange = value => {
```

### Answer Data Structure

?> For Base Answer Structure, please head to [AnswerBaseStructure](DeveloperGuide/Structures/AnswerBasestructure)

```javascript
/* UPPER CASE WORDS ARE CONSTANTS */

// If it is clicked, change the exact one answer to the value selected
propsValue.value = [value];
```

## Cafe Common

N/A
