# Button (QuestionButton)

[Back To QuestionList Outline](DeveloperGuide/QuestionTypes/overview.md?id=outline)

!> **Docs may not up-to-date**

| Last updated |   Author   | Version | Remarks |
| :----------: | :--------: | :-----: | :-----: |
|  8 Jul 2020  | Felix Chum |  1.0.0  |    -    |

## Overview

To generate a button inside the form with customization.

### Advance Features Availability

?> Know more in [Features](DeveloperGuide/Features/overview.md)

| Features (Rule)    | Can be used as rules? | Can be activated? |
| :----------------- | :-------------------: | :---------------: |
| Visibility Rules   |          ❌           |        ✅         |
| Validation Rules   |          ❌           |        ✅         |
| Relationship Rules |          ❌           |        ✅         |

| Features (Non-Rule)   | Applicable |
| :-------------------- | :--------: |
| Design Setting        |     ✅     |
| Dynamic Styles        |     ❔     |
| Multi Language        |     ✅     |
| Import Reference ID   |     ❌     |
| Internal Reference ID |     ✅     |
| Able be Sub Question  |     ❌     |

## Cafe Admin

| Type | Description                                                        |
| :--- | :----------------------------------------------------------------- |
| Name | QuestionButton.js                                                  |
| Path | src/routes/Form/FormEdit/components/questionType/QuestionButton.js |

QuestionButton (Admin) uses the following EABComponent(s)

| Components     | Link                                              |
| :------------- | :------------------------------------------------ |
| EABPopover     | [Click Me](DeveloperGuide/EABComponents/c_button) |
| EABPopoverList | [Click Me](DeveloperGuide/EABComponents/c_button) |
| EABScrollBar   | [Click Me](DeveloperGuide/EABComponents/c_button) |
| EABTextField   | [Click Me](DeveloperGuide/EABComponents/c_button) |

QuestionButton (Admin) uses the following Util(s)

| Utils         | Link                                           |
| :------------ | :--------------------------------------------- |
| questionUtils | [Click Me](DeveloperGuide/Utils/questionutils) |

### Functionality

![questionButton_layout](media/questionButton_01.png)

#### ButtonText

Replace the text inside button by the inputValue

#### Behaviour

?> _Subject To Change_  
A behaviour stack is in plan

Insert button action by the selected behaviour.

| Behavior Option             | Variable       | Description                                          |
| :-------------------------- | :------------- | :--------------------------------------------------- |
| Redirect to:: Form Page     | `REDIRECT_IN`  | If clicked, proceed to next Cafe' form internal page |
| Redirect to:: External Page | `REDIRECT_OUT` | If clicked, proceed to external website              |

#### Target Destination

!> This part change depends on the [Behaviour](#Behaviour) you've chosen

| Behaviour                   | Display as | Description                             |
| :-------------------------- | :--------- | :-------------------------------------- |
| Redirect to:: Form Page     | Dropdown   | Available page list in this form        |
| Redirect to:: External Page | TextField  | External URL, e.g., `http://google.com` |

### Question Data Structure

?> For Base Question Strucutre, please head to [QuestionBaseStructure](DeveloperGuide/Structures/QuestionBaseStructure)

This questionType will store the question by the following format

```javascript
/* UPPER CASE WORDS ARE CONSTANTS */
const question = {
    ...,
    answers: {
        value: {
            [OBJECT]: {
                item: xxx,
                id: displayId,
                type: CHOICES
            }
        },
        type: REDIRECT_IN || REDIRECT_OUT,
        helpTextProps: {
            /* The HelperText */
        }
        styleProps: {
            targetHref: "/* the targetURL */",
            placeholder: "/* the buttonText */",
        }
    }
}
```

## Cafe Display

| Type | Description                                                               |
| :--- | :------------------------------------------------------------------------ |
| Name | QuestionButton.js                                                         |
| Path | src/components/eabComponents/FormDisplayCard/components/QuestionButton.js |

QuestionButton (Display) uses the following EABComponent(s)

| Components | Link                                              |
| :--------- | :------------------------------------------------ |
| EABButton  | [Click Me](DeveloperGuide/EABComponents/c_button) |

QuestionButton (Display) uses the following Util(s)

| Utils         | Link                                            |
| :------------ | :---------------------------------------------- |
| langUtils     | [Click Me](DeveloperGuide/Utils/langUtils)      |
| behaviourUtil | [Click Me](DeveloperGuide/Utils/behaviourUtils) |
| questionUtils | [Click Me](DeveloperGuide/Utils/questionutils)  |

Once the user clicked the button, it will handled by the following function to forward the action to `behaviourUtil`.

```javascript
 const onButtonClick = () => {
```

### Answer Data Structure

?> For Base Answer Structure, please head to [AnswerBaseStructure](DeveloperGuide/Structures/AnswerBasestructure)

```javascript
/* UPPER CASE WORDS ARE CONSTANTS */

// If it is clicked
propsValue.value = [answers.value[0].displayId];
// If it is not clicked (usually it is not happen)
propsValue.value = [];
```

## Cafe Common

N/A
