# Plain Text (QuestionPlainText)

[Back To QuestionList Outline](DeveloperGuide/QuestionTypes/overview.md?id=outline)

!> **Docs may not up-to-date**

| Last updated |   Author   | Version | Remarks |
| :----------: | :--------: | :-----: | :-----: |
| 21 Jul 2020  | Felix Chum |  1.0.0  |    -    |

## Overview

To generate a WYSIWYG `<textarea />` inside the form with customization

### Advance Features Availability

?> Know more in [Features](DeveloperGuide/Features/overview.md)

| Features (Rule)    | Can be used as rules? | Can be activated? |
| :----------------- | :-------------------: | :---------------: |
| Visibility Rules   |          ❔           |        ✅         |
| Validation Rules   |          ❔           |        ✅         |
| Relationship Rules |          ❔           |        ✅         |

| Features (Non-Rule)   | Applicable |
| :-------------------- | :--------: |
| Design Setting        |     ❔     |
| Dynamic Styles        |     ❔     |
| Multi Language        |     ✅     |
| Import Reference ID   |     ❔     |
| Internal Reference ID |     ❔     |
| Able be Sub Question  |     ❌     |

## Cafe Admin

| Type | Description                                                           |
| :--- | :-------------------------------------------------------------------- |
| Name | QuestionPlainText.js                                                  |
| Path | src/routes/Form/FormEdit/components/questionType/QuestionPlainText.js |

QuestionPlainText (Admin) uses the following EABComponent(s)

| Components        | Link                                                       |
| :---------------- | :--------------------------------------------------------- |
| EABRichTextEditor | [Click Me](DeveloperGuide/EABComponents/EABRichTextEditor) |

| Utils             | Link                                               |
| :---------------- | :------------------------------------------------- |
| questionUtils     | [Click Me](DeveloperGuide/Utils/questionutils)     |
| formService       | [Click Me](DeveloperGuide/Utils/formService)       |
| langUtils         | [Click Me](DeveloperGuide/Utils/langUtils)         |
| visibilityService | [Click Me](DeveloperGuide/Utils/visibilityService) |

### Functionality

![questionPlainText_layout](media/questionPlainText_01.png)

### Question Data Structure

?> For Base Question Strucutre, please head to [QuestionBaseStructure](DeveloperGuide/Structures/QuestionBaseStructure)

This questionType will store the question by the following format

```javascript
/* UPPER CASE WORDS ARE CONSTANTS */
answers.update({
  value: {
    [OBJECT]: question.answers.value,
  },
});
```

## Cafe Display

| Type | Description                                                                  |
| :--- | :--------------------------------------------------------------------------- |
| Name | QuestionPlainText.js                                                         |
| Path | src/components/eabComponents/FormDisplayCard/components/QuestionPlainText.js |

QuestionPlainText (Display) uses the following EABComponent(s)

| Components        | Link                                                       |
| :---------------- | :--------------------------------------------------------- |
| EABRichTextEditor | [Click Me](DeveloperGuide/EABComponents/EABRichTextEditor) |

QuestionPlainText (Display) uses the following Util(s)

| Utils         | Link                                           |
| :------------ | :--------------------------------------------- |
| questionUtils | [Click Me](DeveloperGuide/Utils/questionutils) |
| langUtils     | [Click Me](DeveloperGuide/Utils/langUtils)     |
| formUtils     | [Click Me](DeveloperGuide/Utils/formUtils)     |

The questionType will only show text depends on other questions.  
Once it retreived new data, it will update its value.

```javascript
const onStateChange = newRawState => {
```

### Answer Data Structure

?> For Base Answer Structure, please head to [AnswerBaseStructure](DeveloperGuide/Structures/AnswerBasestructure)

```javascript
/* UPPER CASE WORDS ARE CONSTANTS */

// Search for the following words
propsValue.value = editorState.getCurrentContent().getPlainText();
```

## Cafe Common

N/A
