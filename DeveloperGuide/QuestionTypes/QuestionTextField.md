# Text Input (QuestionTextField)

[Back To QuestionList Outline](DeveloperGuide/QuestionTypes/overview.md?id=outline)

!> **Docs may not up-to-date**

| Last updated |   Author   | Version |       Remarks       |
| :----------: | :--------: | :-----: | :-----------------: |
|  8 Jul 2020  | Felix Chum |  1.0.0  |          -          |
| 21 Jul 2020  | Felix Chum |  1.0.1  | Variable name fixed |

## Overview

To generate a `<input />` inside the form with customization

### Advance Features Availability

?> Know more in [Features](DeveloperGuide/Features/overview.md)

| Features (Rule)    | Can be used as rules? | Can be activated? |
| :----------------- | :-------------------: | :---------------: |
| Visibility Rules   |          ✅           |        ✅         |
| Validation Rules   |          ✅           |        ✅         |
| Relationship Rules |          ✅           |        ✅         |

| Features (Non-Rule)   | Applicable |
| :-------------------- | :--------: |
| Design Setting        |     ✅     |
| Dynamic Styles        |     ✅     |
| Multi Language        |     ✅     |
| Import Reference ID   |     ✅     |
| Internal Reference ID |     ✅     |
| Able be Sub Question  |     ✅     |

## Cafe Admin

| Type | Description                                                           |
| :--- | :-------------------------------------------------------------------- |
| Name | QuestionTextField.js                                                  |
| Path | src/routes/Form/FormEdit/components/questionType/QuestionTextField.js |

QuestionTextField (Admin) uses the following EABComponent(s)

| Components       | Link                                                      |
| :--------------- | :-------------------------------------------------------- |
| EABPopoverList   | [Click Me](DeveloperGuide/EABComponents/EABPopoverList)   |
| EABSegmentButton | [Click Me](DeveloperGuide/EABComponents/EABSegmentButton) |
| EABTextField     | [Click Me](DeveloperGuide/EABComponents/EABTextField)     |

QuestionTextField (Admin) uses the following Util(s)

| Utils             | Link                                               |
| :---------------- | :------------------------------------------------- |
| questionUtils     | [Click Me](DeveloperGuide/Utils/questionutils)     |
| formService       | [Click Me](DeveloperGuide/Utils/formService)       |
| langUtils         | [Click Me](DeveloperGuide/Utils/langUtils)         |
| editQuestionUtils | [Click Me](DeveloperGuide/Utils/editQuestionUtils) |

### Functionality

![questionTextField_layout](media/questionTextField_01.png)

#### Input Type

|  Toggle steps  |  Variable   | Minimum No. of Labels |
| :------------: | :---------: | :-------------------: |
|  Single Line   | INPUT_LINE  |           2           |
| Multiple Lines | INPUT_FIELD |           3           |

#### Prefix

The characters that append _**BEFORE**_ the TextField.

#### Postfix

The characters that append _**AFTER**_ the TextField.

#### Format

?> _Subject To Change_  
The format may apply as `Validation Rules`

As as last updated date, format available as

| Name         | Variable       | Description                          |
| :----------- | :------------- | :----------------------------------- |
| Any          | `ANY`          | No formatting rule                   |
| Numeric      | `NUMERIC`      | Input value must be numeric          |
| Charcters    | `CHARACTER`    | Input value must be character        |
| E-mail       | `EMAIL`        | Input value must be an email address |
| NRIC         | `NRIC`         | Input value must be a NRIC           |
| HKID         | `HKID`         | Input value must be a HKID           |
| Phone Number | `PHONE_NUMBER` | Input value must be a phone number   |

#### Max Length

Maximum characters of the TextField

#### Placeholder

Wordings to show before users' input anything

### Question Data Structure

?> For Base Question Strucutre, please head to [QuestionBaseStructure](DeveloperGuide/Structures/QuestionBaseStructure)

This questionType will store the question by the following format

```javascript
/* UPPER CASE WORDS ARE CONSTANTS */
const valueObject = {
    displayId: "/* the displayId */",
    type: CHOICES,
}

const question = {
    ...,
    answers: {
        value: {
          [OBJECT]: valueObject
        },
        type: answersType,  /* the Input Type */
        formatProps: {
          type: format, /* the Format */
          maxLength /* the Max Length */
        },
        styleProps: {
          textDecoration: {
            prefix: langUtils.setLangComponent({
              language,
              componentId: `${question.displayId}_textDecoration_prefix`,
              value: prefix, /* the prefix */
              languageMap
            }),
            postfix: langUtils.setLangComponent({
              language,
              componentId: `${question.displayId}_textDecoration_postfix`,
              value: postfix, /* the postfix */
              languageMap
            }),
            placeholder: langUtils.setLangComponent({
              language,
              componentId: `${question.displayId}_textDecoration_placeholder`,
              value: placeholder, /* the placeholder */
              languageMap
            })
          }
        },
        helpTextProps
    }
}
```

## Cafe Display

| Type | Description                                                           |
| :--- | :-------------------------------------------------------------------- |
| Name | QuestionTextField.js                                                  |
| Path | src/components/eabComponents/FormDisplayCard/components/QuestionTextField.js |

QuestionTextField (Display) uses the following EABComponent(s)

| Components    | Link                                                   |
| :------------ | :----------------------------------------------------- |
| EABPhoneInput | [Click Me](DeveloperGuide/EABComponents/EABPhoneInput) |
| EABTextField  | [Click Me](DeveloperGuide/EABComponents/EABTextField)  |

QuestionTextField (Display) uses the following Util(s)

| Utils         | Link                                           |
| :------------ | :--------------------------------------------- |
| questionUtils | [Click Me](DeveloperGuide/Utils/questionutils) |
| langUtils     | [Click Me](DeveloperGuide/Utils/langUtils)     |

Once the user types, it will handled by the following function.

```javascript
  const onChangeInput = (formatType = null) => e => {
```

### Answer Data Structure

?> For Base Answer Structure, please head to [AnswerBaseStructure](DeveloperGuide/Structures/AnswerBasestructure)

```javascript
/* UPPER CASE WORDS ARE CONSTANTS */

// If it is typed
propsValue.value = newResponseText;
```

## Cafe Common

N/A
