# Multiple Choices (QuestionSelector)

[Back To QuestionList Outline](DeveloperGuide/QuestionTypes/overview.md?id=outline)

!> **Docs may not up-to-date**

| Last updated |   Author   | Version | Remarks |
| :----------: | :--------: | :-----: | :-----: |
| 21 Jul 2020  | Felix Chum |  1.0.0  |    -    |

## Overview

To generate a `<input type="checkbox|| radio" />` inside the form with customization

### Advance Features Availability

?> Know more in [Features](DeveloperGuide/Features/overview.md)

| Features (Rule)    | Can be used as rules? | Can be activated? |
| :----------------- | :-------------------: | :---------------: |
| Visibility Rules   |          ✅           |        ✅         |
| Validation Rules   |          ✅           |        ✅         |
| Relationship Rules |          ✅           |        ✅         |

| Features (Non-Rule)   | Applicable |
| :-------------------- | :--------: |
| Design Setting        |     ✅     |
| Dynamic Styles        |     ❔     |
| Multi Language        |     ✅     |
| Import Reference ID   |     ✅     |
| Internal Reference ID |     ✅     |
| Able be Sub Question  |     ❌     |

## Cafe Admin

| Type | Description                                                                 |
| :--- | :-------------------------------------------------------------------------- |
| Name | Question**Img**Selector.js                                                  |
| Path | src/routes/Form/FormEdit/components/questionType/Question**Img**Selector.js |

Question**Img**Selector (Admin) uses the following EABComponent(s)

| Components       | Link                                                      |
| :--------------- | :-------------------------------------------------------- |
| EABButton        | [Click Me](DeveloperGuide/EABComponents/EABButton)        |
| EABCheckBoxGroup | [Click Me](DeveloperGuide/EABComponents/EABCheckBoxGroup) |
| EABDialog        | [Click Me](DeveloperGuide/EABComponents/EABDialog)        |
| EABListItem      | [Click Me](DeveloperGuide/EABComponents/EABListItem)      |
| EABPopover       | [Click Me](DeveloperGuide/EABComponents/EABPopover)       |
| EABPopoverList   | [Click Me](DeveloperGuide/EABComponents/EABPopoverList)   |
| EABSegmentButton | [Click Me](DeveloperGuide/EABComponents/EABSegmentButton) |
| EABTextField     | [Click Me](DeveloperGuide/EABComponents/EABTextField)     |

| Utils             | Link                                               |
| :---------------- | :------------------------------------------------- |
| questionUtils     | [Click Me](DeveloperGuide/Utils/questionutils)     |
| formService       | [Click Me](DeveloperGuide/Utils/formService)       |
| langUtils         | [Click Me](DeveloperGuide/Utils/langUtils)         |
| editQuestionUtils | [Click Me](DeveloperGuide/Utils/editQuestionUtils) |
| visibilityService | [Click Me](DeveloperGuide/Utils/visibilityService) |

### Functionality

![questionSelector_layout](media/questionSelector_01.png)

#### InputType

|   Toggle steps   |    Variable     |     Description      |
| :--------------: | :-------------: | :------------------: |
|  Single Choice   |  SINGLE_ANSWER  | Select 1 answer only |
| Multiple Choices | MUITIPLE_ANSWER |  Select 1+ answers   |

#### Input Row(s)

The number of `<input />` will be created`. Refer to the followng codes. The dragging is handled by`<Draggable />`

```javascript
  const choicesListJSX = choices.map((choice, index) => {
```

##### Input Rows(s) - Draggable

Users may drag by the `6 dots` icon to reorder the options.

```javascript
<DragIcon />
```

##### Input Row(s) - Image Upload Button

Users may upload the images as the options

```javascript
const returnImgComponent = displayId => {
```

##### Input Row(s) - Reovable

Users may remove the options by clicking the remove icon

```javascript
<ClearIcon />
```

#### Button - + Choices

Users may add optinos by clicking that `<button />`
Once clicked, it will trigger the following function.

```javascript
  const addAnswer = () => {
```

### Question Data Structure

?> For Base Question Strucutre, please head to [QuestionBaseStructure](DeveloperGuide/Structures/QuestionBaseStructure)

This questionType will store the question by the following format

```javascript
/* UPPER CASE WORDS ARE CONSTANTS */
      answers.update({
        type: answerType,
        value: {
          [ARRAY]: choices
        },
        optionalProps,
        helpTextProps
      });
}
```

## Cafe Display

| Type | Description                                                                 |
| :--- | :-------------------------------------------------------------------------- |
| Name | QuestionSelector.js                                                         |
| Path | src/components/eabComponents/FormDisplayCard/components/QuestionSelector.js |

QuestionSelector (Display) uses the following EABComponent(s)

| Components          | Link                                                         |
| :------------------ | :----------------------------------------------------------- |
| EABCheckBoxGroup    | [Click Me](DeveloperGuide/EABComponents/EABCheckBoxGroup)    |
| EABCheckBoxGroupImg | [Click Me](DeveloperGuide/EABComponents/EABCheckBoxGroupImg) |
| EABRadioGroup       | [Click Me](DeveloperGuide/EABComponents/EABRadioGroup)       |
| EABRadioGroupImg    | [Click Me](DeveloperGuide/EABComponents/EABRadioGroupImg)    |
| EABTextField        | [Click Me](DeveloperGuide/EABComponents/EABTextField)        |

QuestionSelector (Display) uses the following Util(s)

| Utils         | Link                                           |
| :------------ | :--------------------------------------------- |
| questionUtils | [Click Me](DeveloperGuide/Utils/questionutils) |
| langUtils     | [Click Me](DeveloperGuide/Utils/langUtils)     |

Once the user clicked, it will handled by the following function.

```javascript
// If it is single choices,
    const onChangeSelector = event => {

// If it is multiple choices
    const onChangeOptionBox = event => {
```

### Answer Data Structure

?> For Base Answer Structure, please head to [AnswerBaseStructure](DeveloperGuide/Structures/AnswerBasestructure)

```javascript
/* UPPER CASE WORDS ARE CONSTANTS */

// Search for the following words
propsValue.value =
```

## Cafe Common

N/A
