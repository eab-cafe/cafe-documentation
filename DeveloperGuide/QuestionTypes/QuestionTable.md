# Slider (QuestionTable)

[Back To QuestionList Outline](DeveloperGuide/QuestionTypes/overview.md?id=outline)

!> **Docs may not up-to-date**

| Last updated |   Author   | Version | Remarks |
| :----------: | :--------: | :-----: | :-----: |
| 21 Jul 2020  | Felix Chum |  1.0.0  |    -    |

## Overview

To generate a `Slider` inside the form with customization

### Advance Features Availability

?> Know more in [Features](DeveloperGuide/Features/overview.md)

| Features (Rule)    | Can be used as rules? | Can be activated? |
| :----------------- | :-------------------: | :---------------: |
| Visibility Rules   |          ❌           |        ❌         |
| Validation Rules   |          ❌           |        ❌         |
| Relationship Rules |          ✅           |        ❌         |

| Features (Non-Rule)   | Applicable |
| :-------------------- | :--------: |
| Design Setting        |     ✅     |
| Dynamic Styles        |     ❔     |
| Multi Language        |     ✅     |
| Import Reference ID   |     ✅     |
| Internal Reference ID |     ✅     |
| Able be Sub Question  |     ❌     |

## Cafe Admin

| Type | Description                                                       |
| :--- | :---------------------------------------------------------------- |
| Name | QuestionTable.js                                                  |
| Path | src/routes/Form/FormEdit/components/questionType/QuestionTable.js |

QuestionTable (Admin) uses the following EABComponent(s)

| Components     | Link                                                    |
| :------------- | :------------------------------------------------------ |
| EABButton      | [Click Me](DeveloperGuide/EABComponents/EABButton)      |
| EABDraggable   | [Click Me](DeveloperGuide/EABComponents/EABDraggable)   |
| EABDroppable   | [Click Me](DeveloperGuide/EABComponents/EABDroppable)   |
| EABListItem    | [Click Me](DeveloperGuide/EABComponents/EABListItem)    |
| EABPopoverList | [Click Me](DeveloperGuide/EABComponents/EABPopoverList) |
| EABTextField   | [Click Me](DeveloperGuide/EABComponents/EABTextField)   |

| Utils             | Link                                               |
| :---------------- | :------------------------------------------------- |
| assetUtils        | [Click Me](DeveloperGuide/Utils/assetUtils)        |
| formService       | [Click Me](DeveloperGuide/Utils/formService)       |
| langUtils         | [Click Me](DeveloperGuide/Utils/langUtils)         |
| visibilityService | [Click Me](DeveloperGuide/Utils/visibilityService) |

### Functionality

![questionTable_layout](media/questionTable_01.png)

#### Default rows

Minimum number of answer rows

```javascript
const [defaultRow, setDefaultRow] = useState(
  _.get(answers, `formatProps.row.min`, 1)
);
```

#### Max row inputs

Maximum number of answer rows

```javascript
const [maxRow, setMaxRow] = useState(_.get(answers, `formatProps.row.max`, 4));
```

### Label

Add other description more than the range

Look for the following variables:

```javascript
const [labelMin, setLabelMin] = useState(_.get(formatProps, "label.start", ""));
const [labelMax, setLabelMax] = useState(_.get(formatProps, "label.end", ""));
```

### Column Header

Column Header Name would like to use

### Field Type

Sub-Question Types

| QuestionTypes     | Variables  | Link                                                       |
| :---------------- | :--------- | :--------------------------------------------------------- |
| QuestionTextField | TEXT_INPUT | [Click Me](DeveloperGuide/QuestionTypes/QuestionTextField) |
| QuestionDate      | DATE       | [Click Me](DeveloperGuide/QuestionTypes/QuestionDate)      |
| QuestionDropDown  | DROP_DOWN  | [Click Me](DeveloperGuide/QuestionTypes/QuestionDropDown)  |

### Question Data Structure

?> For Base Question Strucutre, please head to [QuestionBaseStructure](DeveloperGuide/Structures/)

This questionType will store the question by the following format

```javascript
/* UPPER CASE WORDS ARE CONSTANTS */
    answers.update({
    value: {
        [ARRAYQUESTION]: choices
    },
    formatProps: {
        row: {
        default: defaultRow,
        max: maxRow
        }
    }
    });
```

## Cafe Display

| Type | Description                                                              |
| :--- | :----------------------------------------------------------------------- |
| Name | QuestionTable.js                                                         |
| Path | src/components/eabComponents/FormDisplayCard/components/QuestionTable.js |

QuestionTable (Display) uses the following Util(s)

| Utils         | Link                                           |
| :------------ | :--------------------------------------------- |
| questionUtils | [Click Me](DeveloperGuide/Utils/questionutils) |
| langUtils     | [Click Me](DeveloperGuide/Utils/langUtils)     |

!> The logic to store answers follow the sub-questions.

## Cafe Common

N/A
