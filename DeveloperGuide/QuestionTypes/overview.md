# Overview

!> **Docs may not up-to-date**

| Last updated |   Author   | Version | Remarks |
| :----------: | :--------: | :-----: | :-----: |
|  8 Jul 2020  | Felix Chum |  1.0.0  |    -    |

## Outline

- Standard
  - [Text Input](DeveloperGuide/QuestionTypes/QuestionTextField.md)
  - [Date](DeveloperGuide/QuestionTypes/QuestionDate.md)
- Choices
  - [Multiple Choices](DeveloperGuide/QuestionTypes/QuestionSelector.md)
  - [Drop Down](DeveloperGuide/QuestionTypes/QuestionDropDown.md)
  - [Slider](DeveloperGuide/QuestionTypes/QuestionSlider.md)
  - [Toggle](DeveloperGuide/QuestionTypes/QuestionToggle.md)
- Static
  - [Plain Text](DeveloperGuide/QuestionTypes/QuestionToggle.md)
  - [Button](DeveloperGuide/QuestionTypes/QuestionButton.md)
- Charts
  - [Bar Chart](DeveloperGuide/QuestionTypes/QuestionBarChart.md)
  - [Pie Chart](DeveloperGuide/QuestionTypes/QuestionPieChart.md)
- Media
  - [Handwriting Field](DeveloperGuide/QuestionTypes/QuestionHandwritingField.md)
  - [Profile image upload](DeveloperGuide/QuestionTypes/QuestionImageUpload.md)
- Advance
  - [Calculated Field](DeveloperGuide/QuestionTypes/QuestionCalculatedField.md)
  - [Conditional Choices](DeveloperGuide/QuestionTypes/QuestionConditionalChoices.md)
  - [Question Reference](DeveloperGuide/QuestionTypes/QuestionConditionalChoices2nd.md)
  - [Random Number](DeveloperGuide/QuestionTypes/QuestionRandomNumber.md)
  - [Table](DeveloperGuide/QuestionTypes/QuestionTable.md)

<!-- ## Before you start (Prerequisite) -->