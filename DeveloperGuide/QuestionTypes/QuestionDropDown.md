# Drop Down (QuestionDropDown)

[Back To QuestionList Outline](DeveloperGuide/QuestionTypes/overview.md?id=outline)

!> **Docs may not up-to-date**

| Last updated |   Author   | Version | Remarks |
| :----------: | :--------: | :-----: | :-----: |
| 21 Jul 2020  | Felix Chum |  1.0.0  |    -    |

## Overview

To generate a `<select />` inside the form with customization

### Advance Features Availability

?> Know more in [Features](DeveloperGuide/Features/overview.md)

| Features (Rule)    | Can be used as rules? | Can be activated? |
| :----------------- | :-------------------: | :---------------: |
| Visibility Rules   |          ✅           |        ✅         |
| Validation Rules   |          ✅           |        ✅         |
| Relationship Rules |          ✅           |        ✅         |

| Features (Non-Rule)   | Applicable |
| :-------------------- | :--------: |
| Design Setting        |     ✅     |
| Dynamic Styles        |     ❔     |
| Multi Language        |     ✅     |
| Import Reference ID   |     ✅     |
| Internal Reference ID |     ✅     |
| Able be Sub Question  |     ✅     |

## Cafe Admin

| Type | Description                                                          |
| :--- | :------------------------------------------------------------------- |
| Name | QuestionDropDown.js                                                  |
| Path | src/routes/Form/FormEdit/components/questionType/QuestionDropDown.js |

QuestionDropDown (Admin) uses the following EABComponent(s)

| Components     | Link                                                    |
| :------------- | :------------------------------------------------------ |
| EABButton      | [Click Me](DeveloperGuide/EABComponents/EABButton)      |
| EABDialog      | [Click Me](DeveloperGuide/EABComponents/EABDialog)      |
| EABListItem    | [Click Me](DeveloperGuide/EABComponents/EABListItem)    |
| EABPopover     | [Click Me](DeveloperGuide/EABComponents/EABPopover)     |
| EABPopoverList | [Click Me](DeveloperGuide/EABComponents/EABPopoverList) |

| Utils             | Link                                               |
| :---------------- | :------------------------------------------------- |
| questionUtils     | [Click Me](DeveloperGuide/Utils/questionutils)     |
| formService       | [Click Me](DeveloperGuide/Utils/formService)       |
| langUtils         | [Click Me](DeveloperGuide/Utils/langUtils)         |
| editQuestionUtils | [Click Me](DeveloperGuide/Utils/editQuestionUtils) |
| visibilityService | [Click Me](DeveloperGuide/Utils/visibilityService) |
| resourceUtils     | [Click Me](DeveloperGuide/Utils/resourceUtils)     |
| formUtils         | [Click Me](DeveloperGuide/Utils/formUtils)         |

### Functionality

![questionDropDown_layout](media/questionDropDown_01.png)

#### Input from Resources

| Toggle steps | Variable |     Description     |
| :----------: | :------: | :-----------------: |
|     NONE     |   NONE   | Not using resources |
|     ???      |   ???    |    Any resource     |

#### Input Row(s)

The number of `<input />` will be created. Refer to the followng codes. The dragging is handled by`<Draggable />`

```javascript
  const choicesListJSX = choices.map((choice, index) => {
```

### Button - +

Users may add optinos by clicking that `<button />`
Once clicked, it will trigger the following function.

```javascript
  const addAnswer = () => {
```

### Question Data Structure

?> For Base Question Strucutre, please head to [QuestionBaseStructure](DeveloperGuide/Structures/QuestionBaseStructure)

This questionType will store the question by the following format

```javascript
/* UPPER CASE WORDS ARE CONSTANTS */
      answers.update({
        type: answerType,
        value: {
          [ARRAY]: choices
        },
        optionalProps,
        helpTextProps
      });
}
```

## Cafe Display

| Type | Description                                                                 |
| :--- | :-------------------------------------------------------------------------- |
| Name | QuestionDropDown.js                                                         |
| Path | src/components/eabComponents/FormDisplayCard/components/QuestionDropDown.js |

QuestionDropDown (Display) uses the following EABComponent(s)

| Components  | Link                                                 |
| :---------- | :--------------------------------------------------- |
| EABDropdown | [Click Me](DeveloperGuide/EABComponents/EABDropdown) |

QuestionDropDown (Display) uses the following Util(s)

| Utils         | Link                                           |
| :------------ | :--------------------------------------------- |
| questionUtils | [Click Me](DeveloperGuide/Utils/questionutils) |
| langUtils     | [Click Me](DeveloperGuide/Utils/langUtils)     |
| resourceUtils | [Click Me](DeveloperGuide/Utils/resourceUtils) |

Once the user clicked, it will handled by the following function.

?> Resources may override the exist settings

```javascript
  const menuOnClick = newValue => {
```

### Answer Data Structure

?> For Base Answer Structure, please head to [AnswerBaseStructure](DeveloperGuide/Structures/AnswerBasestructure)

```javascript
/* UPPER CASE WORDS ARE CONSTANTS */

// Search for the following words
propsValue.value = newValue.value;
```

## Cafe Common

N/A
