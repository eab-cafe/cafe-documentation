# Relation Rules(s)

!> **Docs may not up-to-date**

| Last updated |   Author   | Version | Remarks |
| :----------: | :--------: | :-----: | :-----: |
| 23 Jul 2020  | Felix Chum |  1.0.0  |    -    |

## Overview

For the functionality this feature should work like,
click on to the relevant section in the following table.

| Documents                |                     Shortcut                     |
| :----------------------- | :----------------------------------------------: |
| Functional Specification | [Click](FunctionalSpecification/relationRule.md) |

## Demo

If you would like to try out the features, you may try it out by the following links.

!> Under Construction

!> **Remember not to modify anything** and Keep it clean.

?> If the demo not workable, please consult [@Samuel](https://teams.microsoft.com/l/chat/0/0?users=samuel.cheng@eabsystems.com.hk)

| Role           | FormID (Link)                                                                                      |
| :------------- | :------------------------------------------------------------------------------------------------- |
| Builder        | [5f19107338dc0a00196555e1](http://admin-dev.eab-cafe.k8s-inno1/form/5f19107338dc0a00196555e1)      |
| Preview        | [5f19107338dc0a00196555e1](http://display-dev.eab-cafe.k8s-inno1/preview/5f19107338dc0a00196555e1) |
| Published Form | [5f19107a38dc0a00196555e5](http://display-dev.eab-cafe.k8s-inno1/5f19107a38dc0a00196555e5)         |

## Path(s) and Relevant file(s)

?> CSS files always postfix as `.module.css`, `.styles.js` or `.style.js`

!> The relationSetting is shared the concept of [validationRules](DeveloperGuide/Features/validationRules).  
Components used inside relationSetting are [validationRules](DeveloperGuide/Features/validationRules) stuffs

| Role  | Name                     | Path                                                                          | Description            |
| :---- | :----------------------- | :---------------------------------------------------------------------------- | ---------------------- |
| UI    | `RelationshipSetting.js` | `src/routes/Form/components/advanceSetting/components/RelationshipSetting.js` | Main Entry Dialog      |
| hooks | `useRelationship.js`     | `src/hooks/styles/useRelationship.js`                                         | Maintain relationRules of the Question |

## Data Structures

Relation Rules(s) store by a much easily-readable structure compare with [visibilityRules](visibilityRules).

| Property                           | Types (propsTypes) | default Value (defaultProps) | Available Options |
| :--------------------------------- | :----------------- | :--------------------------- | :---------------- |
| [type](#message)                   | cosntants, string  | WRITE_ANSWER                 |
| [content](#message-content)        | string             | CLEAR                        |
| [editable](#message-editable)      | bool               | false                        |
| [operator](#rules-groups-operator) | cosntants, stirng  | "AND", "OR"                  |
| [groups](#rules-groups)            | array              | ValidationRule               |

It is now stored by an array and with the `operator` to show what the group for.

```javascript
const relationRule = [
  {
    type: WRITE_ANSWER,
    content: CLEAR,
    editable: true,
    operator: "AND",
    groups: [
      {
        operator: "OR",
        rules: [
          qtnDisplayId: "displayId",
          condition: "",
        ]
      },
      ...
    ]
  },
  ...
]
```

## How it works?

Relation Rules(s) have flexibility on choosing this rules apply as **BOTH** `AND` (ALL) or `OR` (Any) conditions.

![relationRules_1](/media/relationRules_1.png)

Relation Rules(s) consists of [Message Group](#message-group), [Message](#message), [Rules](#rules)

### Message Group

The Message group is used for add another rule**Set** to validationRules array.  
Once the `3-dots` clicked, users may add another rule**set** with different type of [message](#message).  
Users may auto `onHover` on the message group to delete that rules**Set** if the number of rule**Set** is not equal to 0.

### Message

?> In future, users may possible to set the relationRules type in here

Message is the whole section of [Message Type](#message-canGoNextPage) and [Message Content](#message-content)

#### Message (Editable)

Users may specify the type of error message here. There are **2** options available in total.
If the `canGoNextPage` is `true`, users may bypass the error any proceed to next page.

| Option        | Boolean | Description                   |
| :------------ | :------ | ----------------------------- |
| true          | `true`  | User able to modify the value |
| Error Message | `false` | readonly                      |

#### Message (content)

Different Actions would like to perform.

### Rules

The rule**Set** setting.

#### Rules (Groups) (Operator)

To set how to treat [groups](#rules-groups) as `AND` or `OR` condition

#### Rules (Groups)

To store the rules ruleGroup in here.

#### otherStuffs

?> This rule inside the group are share the concept of [visibilityRules](/DeveloperGuide/Features/visibilityRules).

Noted that there are another operator to control the inner group logic for `AND` or `OR` condition
