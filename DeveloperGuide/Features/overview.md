# Features

## Overview

Cafe form provide the flexibility to customize their form with variety of options.

## Outline

| Abbreviation | Name                                                  | Description                                            |
| :----------: | :---------------------------------------------------- | :----------------------------------------------------- |
|     visR     | [Visibility Rules](/Features/visibilityRules.md)      | Control the question's visibility by rules             |
|     valR     | [Validation Rules](/Features/validationRules.md)      | Control the question's mandatory or not by rules       |
|     relR     | [Relationship Rules](/Features/relationRules.md)  | Auto-Fill question's answer by rules                   |
|     desS     | [Design Setting](/Features/designSetting.md)          | Customization of `Form Theme`                          |
|     dynS     | [Dynamic Styles](/Features/dynamicQuestionStyle.md)   | Individual paths customization (Page/Section/Question) |
|     mulL     | [Multi Language](/Features/multiLanguage.md)          | i18N support of `Form` **(NOT System)**                |
|     exID     | [Import Reference ID](/Features/importReferenceId.md) | ID's for data imported for external sources            |
|     irID     | [Internal Reference ID](/Features/referenceId.md)     | ID's for answer belongs to the form                    |
|     subQ     | [SubQuestion](Features/subQuestion.md)                | Child question of a question                           |

## Object Structure Matrix

This is a basic matrix which shown the `Form` object structure  
Some of the keyfields are re-used as it is specified for that level.  
For **details** and the **API implementation**, you should look in to the [API documentation](http://192.168.222.60:8001/eab-cafe/cafe-api/tree/DEV/docs).

| Status | Description             |
| :----: | ----------------------- |
|   ✅   | Done                    |
|   🚧   | Under Construction      |
|   🛠️   | Limited Functionalities |

Level under `Form` hireachy

|  -   |  -   |    -    |    -     |   -    | visR | valR | relR | desS | mulL | exID | irID | dynS |
| :--: | :--: | :-----: | :------: | :----: | :--: | :--: | :--: | :--: | :--: | :--: | :--: | :--: |
| Form |  -   |    -    |    -     |   -    |  -   |  -   |  🛠️  |  ✅  |  🛠️  |  ✅  |  -   |  🛠️  |
|  \|  | Page |    -    |    -     |   -    |  -   |  -   |  🛠️  |  ✅  |  🛠️  |  -   |  ✅  |  🛠️  |
|  -   |  \|  | Section |    -     |   -    |  ✅  |  -   |  🛠️  |  ✅  |  🛠️  |  -   |  ✅  |  🛠️  |
|  -   |  -   |   \|    | Question |   -    |  ✅  |  🛠️  |  🛠️  |  ✅  |  🛠️  |  -   |  ✅  |  🛠️  |
|  -   |  -   |    -    |    \|    | Answer |  -   |  🛠️  |  🛠️  |  -   |  🛠️  |  -   |  🛠️  |  -   |
