# Visibility Rule(s)

!> **Docs may not up-to-date**

| Last updated |   Author   | Version | Remarks |
| :----------: | :--------: | :-----: | :-----: |
|      -       |    Don     |  0.0.1  |         |
|  8 Jul 2020  | Felix Chum |  1.0.0  |    -    |

## Overview

For the functionality this feature should work like,
click on to the relevant section in the following table.

| Documents                |                      Shortcut                      |
| :----------------------- | :------------------------------------------------: |
| Functional Specification | [Click](FunctionalSpecification/visibilityRule.md) |

## Demo

If you would like to try out the features, you may try it out by the following links.

!> Under Construction

!> **Remember not to modify anything** and Keep it clean.

?> If the demo not workable, please consult [@Samuel](https://teams.microsoft.com/l/chat/0/0?users=samuel.cheng@eabsystems.com.hk)

| Role           | FormID (Link)                                                                                      |
| :------------- | :------------------------------------------------------------------------------------------------- |
| Builder        | [5f05755d38dc0a0019654ab2](http://admin-dev.eab-cafe.k8s-inno1/form/5f05755d38dc0a0019654ab2)      |
| Preview        | [5f05755d38dc0a0019654ab2](http://display-dev.eab-cafe.k8s-inno1/preview/5f05755d38dc0a0019654ab2) |
| Published Form | [5f05785e38dc0a0019654ab7](http://display-dev.eab-cafe.k8s-inno1/5f05785e38dc0a0019654ab7)         |

## Path(s) and Relevant file(s)

?> CSS files always postfix as `.module.css`, `.styles.js` or `.style.js`

| Role    | Name                    | Path                                                                                           | Description                                       |
| :------ | :---------------------- | :--------------------------------------------------------------------------------------------- | ------------------------------------------------- |
| Service | `visibilityService.js`  | `src/utils/visibilityService.js`                                                               | [Details](DeveloperGuide/Utils/visibilityService) |
| UI      | `VisibilitySetting.js`  | `src/routes/Form/components/advanceSetting/components/VisibilitySetting/VisibilitySetting.js`  | Main Entry Dialog                                 |
| UI      | `VisibilityAndLogic.js` | `src/routes/Form/components/advanceSetting/components/VisibilitySetting/VisibilityAndLogic.js` | And Logic                                         |
| UI      | `VisibilityOrLogic.js`  | `src/routes/Form/components/advanceSetting/components/VisibilitySetting/VisibilityOrLogic.js`  | Or Logic                                          |
| hooks   | `useVisibility.js`      | `src/hooks/styles/useVisibility.js`                                                            | Control Visibility of Question                    |

## Data Structures

> The Data Structure of Validation Rule is using a 2D array.  
> The first layout is “Or” condition and second layout is “And” condition.  
> Using this kind of 2D array will be able to achieve checking “All” and “Any” condition.  
> And it also achieves checking the mixed condition.

Visibility Rule(s) store by a special structure in the idea of array and realtionship of multi-dimension array.

| Property                | Types (propsTypes)      | default Value (defaultProps) | Available Options                                                                                      |
| :---------------------- | :---------------------- | :--------------------------- | :----------------------------------------------------------------------------------------------------- |
| [condition](#condition) | string                  | -                            | IS_VISIBLE, IS_EQUAL, IS_NOT_EQUAL, LESS_THAN, GREATER_THAN, LESS_THAN_OR_EQUAL, GREATER_THAN_OR_EQUAL |
| [id](#field-id)         | displayId               |                              |                                                                                                        |
| [value](#actions-value) | displayId, string, null |                              |                                                                                                        |

```javascript
// AND
const visibilityRule = [
  [
    [
      {
        condition: "IS_EUQAL",
        id: "123",
        value: "value",
      },
    ],
    [
      {
        condition: "IS_EUQAL",
        id: "123",
        value: "value",
      },
    ],
  ],
];
```

```javascript
// OR
const visibilityRule = [
  [
    [
      {
        condition: "IS_EUQAL",
        id: "123",
        value: "value",
      },
    ],
  ],
  [
    [
      {
        condition: "IS_EUQAL",
        id: "123",
        value: "value",
      },
    ],
  ],
];
```

In short, the data structures for `AND` and `OR` are in the following formats respectively.

```javascript
// Notice the Array difference
// AND
const visibilityRule = [[[{}], [{}]]];

// OR
const visibilityRule = [[[{}]], [[{}]]];
```

## How it works?

Visibility Rule(s) have flexibility on choosing this rules apply as **either** `AND` (ALL) or `OR` (Any) conditions.

![visibilityRules_1](/media/visibilityRules_1.png)

Visibility Rule(s) consists of **3** parts and **2** actions, including

- Parts
  - [Field](#field-id)
  - [Condition](#Condition)
  - [Value](#actions-value)
- Actions (Button)
  - [Delete](#delete)
  - [Add](#add)

which you can find it in the above screen capture.

### Parts

#### Field (id)

`Field` is the available questions list,
`THIS` question is excluded from the list.

The displayId of the selected question.
The questions is generated by the following code.

```javascript
// AdvanceSetting.js
// Exclude THIS question
const questionList = visibilityService.getQuestionList(question);

<VisibilitySetting
    questionList={questionList}
    ...
/>

// VisibilitySetting.js
// The props (questionList) is directly refer from props
const { className, dataGroup, updateData, questionList } = props;

<VisibilityOrLogic
    questionList={questionList}
/>
```

#### Condition

Available option(s) of condition depends on the `questionType`of the field's question. For details, available conditions can be found in `constants/advanceSetting`.

### Actions (value)

The value selected to be compare. It depends on the questionType to use what kind of methodlogy to do the comparison.  
If it is `SELECTOR`, it will compare using `displayId` of options.  
If it is `TEXT`, it will compare using the `string` aligned with what the condition behave or not.

#### Delete

This button is for deleting the selected rule.

!> UI will always have at least 1 rule (placeholder) no matter you have no rule or not.

#### Add

This button will create a new EMPTY rule.
