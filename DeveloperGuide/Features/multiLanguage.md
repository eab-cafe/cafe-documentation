# Multi Language

!> **Docs may not up-to-date**

| Last updated |   Author   | Version | Remarks |
| :----------: | :--------: | :-----: | :-----: |
| 24 Jul 2020  | Felix Chum |  1.0.0  |    -    |

## Overview

For the functionality this feature should work like,
click on to the relevant section in the following table.

| Documents                |                     Shortcut                      |
| :----------------------- | :-----------------------------------------------: |
| Functional Specification | [Click](FunctionalSpecification/multiLanguage.md) |

# Path(s) and Relevant file(s)

?> CSS files always postfix as `.module.css`, `.styles.js` or `.style.js`

| Role | Name                     | Path                                                                       | Description |
| :--- | :----------------------- | :------------------------------------------------------------------------- | ----------- |
| UI   | `formSettingLanguage.js` | `src/routes/Form/components/formSetting/components/FormSettingLanguage.js` | Main Dialog |
| UI   | `Language.js`            | `src/routes/components/language/Language.js`                               | Language Panel |
| UI   | `LanguageItem.js`        | `src/routes/components/language/LanguageItem.js`                           | Each Language Item |

## Data Structures

It will store the languages activated in to `form` and store each questions langauge data to another record.

![multiLanguage_3](media/multiLangauge_3.png)
![multiLanguage_4](media/multiLangauge_4.png)

## How it works?

Visibility Rule(s) have flexibility on choosing this rules apply as **either** `AND` (ALL) or `OR` (Any) conditions.

![multiLanguage_1](/media/multiLanguage_1.png)
![multiLanguage_2](/media/multiLanguage_2.png)