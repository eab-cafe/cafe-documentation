# Import Reference ID

!> **Docs may not up-to-date**

| Last updated |   Author   | Version | Remarks |
| :----------: | :--------: | :-----: | :-----: |
| 24 Jul 2020  | Felix Chum |  1.0.0  |    -    |

## Overview

!> This is not [referenceId](referenceId.md).

The id for reference the `data` user `imported` from external system.  
It split in to **2** parts.

| Part            | Description                                                       |
| :-------------- | :---------------------------------------------------------------- |
| [Rules](#rules)           | To import the rules that later on accept per response importation |
| [Response Import](#response-import) | Import the data per response basis                                |

Please look for the [swagger](DeveloperGuide/dev-overview.md) for more details.

### Rules

| Swagger Version | Path                            |
| :-------------- | :------------------------------ |
| V1              | /form/{formId}/importDataRules  |
| V2              | /forms/{formId}/importDataRules |

### Response Import

| Swagger Version | Path                            |
| :-------------- | :------------------------------ |
| V1              | /response/importData  |
| V2              | /publish-forms/{publishFormId}/response/external-data |
