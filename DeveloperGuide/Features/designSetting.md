# Design Setting(s)

!> **Docs may not up-to-date**

| Last updated |   Author   | Version | Remarks |
| :----------: | :--------: | :-----: | :-----: |
| 24 Jul 2020  | Felix Chum |  1.0.0  |    -    |

## Overview

For the functionality this feature should work like,
click on to the relevant section in the following table.

| Documents                |                    Shortcut                    |
| :----------------------- | :--------------------------------------------: |
| Functional Specification | [Click](FunctionalSpecification/designMode.md) |

## Path(s) and Relevant file(s)

?> CSS files always postfix as `.module.css`, `.styles.js` or `.style.js`

?> If `hidden`, click the `Design Setting` (title) 5 times

| Role  | Name                        | Path                                                                         | Description            | Hidden |
| :---- | :-------------------------- | :--------------------------------------------------------------------------- | ---------------------- | ------ |
| UI    | `DesignForm.js`             | `src/routs/Form/FormDesign/components/DesignForm.js`                         | ~ to Form              |
| UI    | `DesignPanel.js`            | `src/routs/Form/FormDesign/components/DesignPanel.js`                        | Control the settings   |
| UI    | `DesignSection.js`          | `src/routs/Form/FormDesign/components/DesignSection.js`                      | ~ to ForSectionm       |
| UI    | `DesignQuestion.js`         | `src/routs/Form/FormDesign/components/DesignQuestion.js`                     | ~ to FormQuestion      |
| UI    | `DeveloperModes.js`         | `src/routs/Form/FormDesign/components/designPanel/DeveloperModes.js`         |                        | ✅     |
| UI    | `ErrorMessages.js`          | `src/routs/Form/FormDesign/components/designPanel/ErrorMessages.js`          |                        |
| UI    | `LayoutProperties.js`       | `src/routs/Form/FormDesign/components/designPanel/LayoutProperties.js`       |                        |
| UI    | `NavigationProperties.js`   | `src/routs/Form/FormDesign/components/designPanel/NavigationProperties.js`   |                        |
| UI    | `PersonalizeTheme.js`       | `src/routs/Form/FormDesign/components/designPanel/PersonalizeTheme.js`       |                        |
| UI    | `QuestionStyle.js`          | `src/routs/Form/FormDesign/components/designPanel/QuestionStyle.js`          |                        |
| UI    | `SectionIndividualStyle.js` | `src/routs/Form/FormDesign/components/designPanel/SectionIndividualStyle.js` |                        |
| UI    | `SectionStyle.js`           | `src/routs/Form/FormDesign/components/designPanel/SectionStyle.js`           |                        |
| hooks | `useBackground.js`          | `src/hooks/styles/useBackground.js`                                          | Control Background     |
| hooks | `useButtonColor.js`         | `src/hooks/styles/useButtonColor.js`                                         | Control Button Colour  |
| hooks | `useFlexAlignment.js`       | `src/hooks/useFlexAlignment.js`                                              | Control Flex Alignment |
| hooks | `useFormTheme.js`           | `src/hooks/useFormTheme.js`                                                  | Control Form Theme     |
| hooks | `useFont.js`                | `src/hooks/styles/useFont.js`                                                | Control Font Styles    |

## Data Structures

![designSetting_2](media/designSettings_2.png)

Inside the `desingSetting`, various styles properties stored in different categories.

## How it works?

![designSettings_1](media/designSettings_1.png)

For how's the data to be stored according to the state in different categories and where it's located, please refer to the scripts.

In `display`, the design properties will be extracted using it path.

## Grid Layout Feature

In `design mode`, you can DIY your form in different representation. For details how's it behind logic, please visit the previous document. `smb://192.168.0.56/Cafe Project/Handover/5. Grid Layout design .docx`
