# LangUtils

!> **Docs may not up-to-date**

| Last updated | Author | Version | Remarks |
|:------:|:------:|:------:|:------:|
| 21 Oct 2019 | Felix Chum | 1.0.0 | - |

The **langUtils** aims to
> 1. **Store** all the functions that access the language mapper  

## Structure

> - **State**
>   - [getLanguageMap](#getLanguageMap)
>   - [setLanguageMap](#setLanguageMap)
> - **Funcs**
>   - [init](#init)
>   - [getLanguageByIdentifier](#getLanguageByIdentifier)
>   - [addLangComponent](#addLangComponent)
>   - [getLangComponent](#getLangComponent)
>   - [setLangComponent](#setLangComponent)  
>   - [isDefaultLang](#isDefaultLang)
/* DEPRECATED */  
>   - [getLangComponentInAnswer](#getLangComponentInAnswer)
>   - [setLangComponentInAnswer](#setLangComponentInAnswer)

<hr />

### getLanguageMap

Return the state stored ``languageMap``

### setLanguageMap

The ``funcs`` that to store the ``languageMap``

### init

!> **MUST RUN BEFORE YOU USE IT**

> - [getLanguageMap](#getLanguageMap)
> - [setLanguageMap](#setLanguageMap)

### getLanguageByIdentifier()

!> **INTERNAL**

Return the language Identifier,  
which is ``language.code``  

### addLangComponent()

> - language = { code: defaultLangID }
> - componentId
> - value
> - languageMap = state.getLanguageMap()

!> **CAVEAT** <br /> **PASS the ``languageMap`` if you are accessing the ``languageMap`` more than ``1`` times before the state update**

Assign the ``value`` by ``componentId`` under the ``language's languageMap``.  
**NOTED**  
it will also assign to the ``default language`` if the ``default language`` has no ``value`` for the ``componentId``

### getLangComponent()

> - language = { code: defaultLangID }
> - componentId
> - defaultValue = ""
> - debugId = ""

Return the ``value`` that the ``componentId`` contained in the ``languageMap`` under that ``language``  
It will retreive the value by this order.

1. language provided
2. default langauge
3. defaultValue

### setLangComponent()

> - language = { code: defaultLangID }
> - componentId
> - value
> - langaugeMap = state.getLanguageMap()

Assign the ``value`` by ``componentId`` under the ``language's languageMap``.  

### getLangComponentInAnswer()

!> **DEPRECATED** <br /> Only for those questionType not compatible to the ``languageMap`` format

> - language = { code: defaultLangID }
> - component
> - defaultValue = ""

Get the ``value`` from the ``component`` provided by ``language``

### setLangComponentInAnswer()

!> **DEPRECATED** <br /> Only for those questionType not compatible to the ``languageMap`` format

> - language = { code: defaultLangID }
> - component
> - value

Set the ``value`` to the ``component`` provided by ``language``

### isDefaultLang()

Return ``true`` if the result of ``getLanguageByIdentifier()`` equals to the default Language