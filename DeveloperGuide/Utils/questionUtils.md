# QuestionUtils

!> **Docs may not up-to-date**

| Last updated | Author | Version | Remarks |
|:------:|:------:|:------:|:------:|
| 19 Aug 2019 | Felix Chum | 1.0.0 | - |

The **questionUtils** aims to
> 1. **Store** all the validation functions  
> 2. **Unify** the classes to handle ``question.answers`` and ``propsValue`` respecitvely for export

## Structure

> - **Class**
>   - [AnswerMapper](#class-AnswerMapper) // for **propsValue**
>   - [QuestionAnswer](#class-QuestionAnswer) // for **question.answers**
> - **Functions**
>   - [TEXT_INPUT]
>     - getMaxLength
>     - isEmail
>     - isCharacter
>     - isNumeric **// DEPRECATED // Group it to general**
>   - [CALC_INPUT]
>     - getAddition
>     - getSubtraction
>   - [DATE]
>     - **// getDateByFormat // Future Plan**
>   - [PLAIN_TEXT]
>   - isInValueMap
>   - isNumeric
>   - getFormattedNumber
>   - getValueMapById
>   - getValueAnswerById

<hr />

## class - AnswerMapper

> - [constructor](#AnswerMapper-constructor)
> - [type](#AnswerMapper-type)
> - [value](#AnswerMapper-value)
> - [optionalValue](#AnswerMapper-optionalValue)
> - [valueType](#AnswerMapper-valueType) **// Getter only**
> - [optionalValueType](#AnswerMapper-optionalValueType) **// Getter only**
> - [isDisplayId](#AnswerMapper-isDisplayId) **// Getter only**
> - [ToMap](#AnswerMapper-t)
### AnswerMapper - constructor

Accept the following parameters to construct the object  

> - [questionType](#AnswerMapper-questionType) **// Mandatory**
> - [type](#AnswerMapper-type) = null **// sub question-type**
> - [value](#AnswerMapper-value) = null **// if any**
> - [optionalValue](#AnswerMapper-optionalValue) = null **// if any**

To create an object, do such things,

```javascript
new questionUtils.AnswerMapper({
  questionType: // the questionType
  type: // the answerType
  value: // the value
  optionalValue: // the value used by optional fields
})
```

If you're familar with the [spread syntax](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax).  
For simplicity, you can do something like this if the object contains the required properties.

```javascript
new questionUtils.AnswerMapper({
  ...answerObj, // includes the properties above-mentioned if necessary
})
```

### AnswerMapper - questionType

!> **Mandatory**

It should align with the ``question.answers.questionType``  
Used for validation, distinguish where the value should be.

![questionUtils_questionType1](img/questionUtils_questionType1.png)  

### AnswerMapper - type

> **Optional**  
> Default value = null

Store the subType of the question.  

The fields mentioned in below should use ``type``

![questionUtils_type1](img/questionUtils_type1.png)  

### AnswerMapper - value

> **Optional**  
> Default value = null

Store the value used by the question

### AnswerMapper - optionalValue

> **Optional**  
> Default value = null

Store the **optional field's** value used by the question

### AnswerMapper - valueType

!> [**Getter**](https://developer.mozilla.org/zh-TW/docs/Web/JavaScript/Reference/Functions/get)

Return the ``valueType``,  
which stored in the value classified by ``questionType``,  
e.g., ``array``, ``object``... etc

### AnswerMapper - optionalValueType

!> [**Getter**](https://developer.mozilla.org/zh-TW/docs/Web/JavaScript/Reference/Functions/get)

Return the ``optionalValueType``,  
which is the structure the stored in optionalValue classified by ``questionType``,  
**Currently, we only use ``text`` to store optionalValue**

### AnswerMapper - isDisplayId // Boolean

!> [**Getter**](https://developer.mozilla.org/zh-TW/docs/Web/JavaScript/Reference/Functions/get)

Return ``true`` if the value is stored in displayId,  ``false`` if stored in text

### AnswerMapper - ToMap

!> **Function** <br/> Not Finalized

To run it,

```javascript
new questionUtils.AnswerMapper({...question}).ToMap()
```

it shoulds return you the following properties

> - value
> - valueType
> - type
> - optionalValue
> - optionalValueType
> - isDisplayId

<hr />

## class - QuestionAnswer

?> **Todo** <br/> Values not cleared yet,<br/>it should be cleared in the future when save

Accept the following parameters to construct the object  

> - [constructor](#QuestionAnswer-constructor)
> - [type](#QuestionAnswer-type)
> - [formatProps](#QuestionAnswer-formatProps)
> - [styleProps](#QuestionAnswer-styleProps)
> - [optionalProps](#QuestionAnswer-optionalProps)
> - [value](#QuestionAnswer-value)
> - [values](#QuestionAnswer-values)
> - [valueText](#QuestionAnswer-valueText) **// Please do not use it unless it is necessary**
> - [valueArray](#QuestionAnswer-valueArray) **// Please do not use it unless it is necessary**
> - [valueObject](#QuestionAnswer-valueObject) **// Please do not use it unless it is necessary**
> - [valueArrayObject](#QuestionAnswer-valueArrayObject) **// Please do not use it unless it is necessary**
> - [update](#QuestionAnswer-update) **// Function**
> - [WriteToQuestion](#QuestionAnswer-WriteToQuestion) **// Function**

### QuestionAnswer - constructor

> - [questionType](#QuestionAnswer-questionType) **// Mandatory**
> - [question](#QuestionAnswer-question) **// Mandatory**
> - [value](#QuestionAnswer-value) = {} **// if any**
> - [type](#QuestionAnswer-type) = null **// sub question-type**
> - [formatProps](#QuestionAnswer-formatProps) = {} **// if any**
> - [styleProps](#QuestionAnswer-styleProps) = {} **// if any**
> - [optionalProps](#QuestionAnswer-optionalProps) = null **// if any**

To create an object, do such things,

```javascript
new questionUtils.QuestionAnswer({
  questionType: // the questionType
  question: // the question
  value: // the value
  type: // the answerType
  formatProps: // all format-related properties should put it here
  styleProps: // all style-related properties should put it here
  optionalProps: // all optional field properties should put it here
})
```

If you're familar with the [spread syntax](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax).  
For simplicity, you can do something like this if the object contains the required properties.

```javascript
new questionUtils.QuestionAnswer({
  ...questionObj, // includes the properties above-mentioned if necessary
})
```

### QuestionAnswer - questionType

!> **Mandatory**

It should align with the ``question.answers.questionType``  
Used for validation, distinguish where the value should be.

![questionUtils_questionType1](img/questionUtils_questionType1.png)  

### QuestionAnswer - question

!> **Mandatory**

Used for rewrite the value to ``question`` object,  
``WriteToQuestion()`` help to do this at the end of each designed functions

### QuestionAnswer - type

> **Optional**  
> Default value = null

Store the subType of the question.  
Return the current ``answerType`` by validating the ``questionType``,  
fallback to default values if violated  

The fields mentioned in below should use ``type``

![questionUtils_type1](img/questionUtils_type1.png)  

### QuestionAnswer - formatProps

?> **Formatting** is **NOT** required for this props yet as it is not necessary <br/> but reserve to change it in future.  

**Assign and Return** the ``formatProps``,  
The fields mentioned in below should use ``formatProps``  

![questionUtils_formatProps1](img/questionUtils_formatProps1.png)  
![questionUtils_formatProps2](img/questionUtils_formatProps2.png)  

### QuestionAnswer - styleProps

?> **Formatting** is **NOT** required for this props yet as it is not necessary <br/> but reserve to change it in future. 

**Assign and Return** the ``styleProps``,  
The fields mentioned in below should use ``styleProps``  

![questionUtils_styleProps1](img/questionUtils_styleProps1.png)

### QuestionAnswer - optionalProps

?> **Formatting** is **NOT** required for this props yet as it is not necessary <br/> but reserve to change it in future. 

**Assign and Return** the ``optionalProps``,  
The fields mentioned in below should use ``optionalProps``  

![questionUtils_optionalProps1](img/questionUtils_optionalProps1.png)


### QuestionAnswer - value

> **Optional**  
> Default value = {} **// the root value, which is the get values**

**Assign and Return** the value which belongs to the valueType,  
i.e., ``array``, ``object``, ``text``, etc...

### QuestionAnswer - values

!> [**Getter**](https://developer.mozilla.org/zh-TW/docs/Web/JavaScript/Reference/Functions/get) <br/> You should not use it **unless it is necessary** or **debug** purpose.

Return the ``private`` value object that contains differnet ``questionType`` values. 

### QuestionAnswer - valueType

!> [**Getter**](https://developer.mozilla.org/zh-TW/docs/Web/JavaScript/Reference/Functions/get)

Return the ``valueType``,  
which store the value classified by ``questionType``,  
See the following value type for reference.  

> - [Text](#questionanswer-valuetext)
> - [Array](#questionanswer-valueArray)
> - [Object](#questionanswer-valueObject)
> - [ArrayObject](#questionanswer-valueArrayObject)

### QuestionAnswer - valueText

!> You should not use it **unless it is necessary** or **debug** purpose.

**Assign and Return** the ``private`` value if the type is ``text``  
Below ``questionType`` is using this type to store the value.

?> **No questionType** is using this type to store value, yet.<br/> By **Default**, ``text`` will be used to store the value if the ``questionType`` is not in the validation

### QuestionAnswer - valueArray

!> You should not use it **unless it is necessary** or **debug** purpose.

**Assign and Return** the ``private`` value if the type is ``array``  
Below ``questionType`` is using this type to store the value.

> - DROP_DOWN
> - SELECTOR
> - TOGGLE
> - SLIDER

### QuestionAnswer - valueObject

!> You should not use it **unless it is necessary** or **debug** purpose.

**Assign and Return** the ``private`` value if the type is ``object``  
Below ``questionType`` is using this type to store the value.

> - CALC_INPUT

### QuestionAnswer - valueArrayObject

!> You should not use it **unless it is necessary** or **debug** purpose.

**Assign and Return** the ``array of object``,  
Below ``questionType`` is using this type to store the value.

> - PLAIN_TEXT

### QuestionAnswer - update

!> **Function** <br/> Not Finalized

**Combine** the existing the ``object`` with the new ``object``,
and store the merged one.  
Ensure that you're not passing the ``EMPTY`` property value to the object,  otherwise it got rewrote  

Accepted properties

> - [type](#QuestionAnswer-type)
> - [formatProps](#QuestionAnswer-formatProps)
> - [styleProps](#QuestionAnswer-styleProps)
> - [optionalProps](#QuestionAnswer-optionalProps)
> - [value](#QuestionAnswer-value)

### QuestionAnswer - WriteToQuestion

!> **Function** <br/> Not Finalized

if there is ``this.question``, return ``object``

> - [value](#QuestionAnswer-value)
> - [type](#QuestionAnswer-type)
> - [formatProps](#QuestionAnswer-formatProps)
> - [styleProps](#QuestionAnswer-styleProps)
> - [optionalProps](#QuestionAnswer-optionalProps)
