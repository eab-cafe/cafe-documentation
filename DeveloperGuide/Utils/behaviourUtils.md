# BehaviourUtils

!> **Docs may not up-to-date**

| Last updated | Author | Version | Remarks |
|:------:|:------:|:------:|:------:|
| 21 Oct 2019 | Felix Chum | 1.0.0 | - |

The **behaviourUtils** aims to
> 1. **Store** all the behaviours  
> 2. **Unify** the classes to handle ``behaviour`` and ``behaviourController`` respecitvely for specify the behaviour and control the behaviours

## Structure

> - **Class**
>   - [Behaviour](#class-Behaviour)
>   - [BehaviourController](#class-BehaviourController)

<hr />

## class - Behaviour

> - [constructor](#Behaviour-constructor)
> - [controller](#Behaviour-controller)
> - [isValid](#Behaviour-isValid)
> - [run](#Behaviour-run)

### Behaviour - constructor

Accept the following parameters to construct the object  

> - [type](#Behaviour-type) **// Mandatory**
> - [controller](#Behaviour-controller)
> - [href](#Behaviour-href)
> - [condition](#Behaviour-condition)

To create an object, do such things,

```javascript
new behaviourUtils.Behaviour({
  type: // the behaviour Types
  controller: // the behaviourController
  href: // the link href which is used by some type
  condition: // some condition before its valid to excute
})
```

### Behaviour - type

!> **Mandatory**

Currently we accepted the following ``constants``,  
which you can also find it at ``cafe-common/constants/formBehaviour.js``

> - REDIRECT_EXTERNAL_LINK
> - REDIRECT_CAFE_PAGE
> - SUBMIT

### Behaviour - controller

> **Optional**  
> Default value = null

Store the bahaviourController for execution.  

### Behaviour - href

> **Optional**  
> Default value = null

Store the herf used by the ``REDIRECT_EXTERNAL_LINK``,  ``REDIRECT_CAFE_PAGE`` and ``SUBMIT``

### Behaviour - condition

> **Optional**  
> Default value = null

Store the **condition** which is required before the execution of the behaviour

### Behaviour - isValid()

Check if the Behaviour is Valid by some performing some pre-check conditions

### Behaviour - run()

Do what it is expected for running this behaviour

<hr />

## class - BehaviourController

> - [constructor](#BehaviourController-constructor) **// Mandatory**
> - [currentPage](#BehaviourController-currentPage)
> - [checkedList](#BehaviourController-checkedList)
> - [condition](#BehaviourController-condition)
> - [isPassCondition](#BehaviourController-isPassCondition)
> - [pageIndexDisplayId](#BehaviourController-pageIndexDisplayId)
> - [pageIndex](#BehaviourController-pageIndex)
> - [clickNextButton](#BehaviourController-clickNextButton)
> - [submitForm](#BehaviourController-submitForm)

### BehaviourController - constructor

Accept the following parameters to construct the object  

> - [form](#BehaviourController-form) **// Mandatory**
> - [page](#BehaviourController-page) **// Mandatory**
> - [pageIndex](#BehaviourController-pageIndex) **// Mandatory**  
/* setState */  
> - [setSubmit](#BehaviourController-setSubmit) **// Mandatory**
> - [setPageIndex](#BehaviourController-setPageIndex) **// Mandatory**
> - [setNextClickedList](#BehaviourController-setNextClickedList) **// Mandatory**
> - [setCheckError](#BehaviourController-setCheckError) **// Mandatory**  
/* State */  
> - [valueMap](#BehaviourController-valueMap)
> - [checkedList](#BehaviourController-checkedList) 
> - [visibilityMap](#BehaviourController-visibilityMap)
> - [validationMap](#BehaviourController-validationMap)  
/* Constants */  
> - [hasErrorCheck](#BehaviourController-hasErrorCheck)

To create an object, do such things,

```javascript
new behaviourUtils.BehaviourController({
    form,
    page,
    pageIndex,
    setSubmit,
    setPageIndex,
    setNextClickedList,
    setCheckError,
    valueMap,
    checkedList,
    visibilityMap,
    validationMap,
    hasErrorCheck: true || false
})
```

### BehaviourController - form

!> **Mandatory**

Current ``form`` object

### BehaviourController - page

!> **Mandatory**

Current ``page`` object

### BehaviourController - pageIndex

!> **Mandatory**

The ``index`` of ``form.pages``,  
which is equal to ``pageIndex``

!> [**Setter**](https://developer.mozilla.org/zh-TW/docs/Web/JavaScript/Reference/Functions/set) <br/> **You should not use it as it is for internal use only**.

set the ``pageIndex`` to the state directly

### BehaviourController - setSubmit

!> **Mandatory**

The ``function`` that set the form is submitted

### BehaviourController - setPageIndex

!> **Mandatory**

The ``function`` that set the current pageIndex

### BehaviourController - setNextClickedList

!> **Mandatory**

The ``function`` that set this page is clicked or not, among the whole ``form``

### BehaviourController - setCheckError

!> **Mandatory**

The ``function`` that set this ``page`` should check error or not

### BehaviourController - valueMap

!> **Optional**

The ``object`` that store the answers among whole ``form``

### BehaviourController - checkList

!> **Optional**

The ``arrayList`` that store the pages checked or not among the ``form``

### BehaviourController - visibilityMap

!> **Optional**

The ``object`` that store the question is visible or not among the ``form``

### BehaviourController - validationMap

!> **Optional**

The ``object`` that store the question has ``error`` or not proceed by validation Rules among the ``form``

### BehaviourController - hasErrorCheck

!> **Optional**

The ``boolean`` that indicate the ``behaviourController`` should check for error or not

### BehaviourController - currentPage

!> [**Setter**](https://developer.mozilla.org/zh-TW/docs/Web/JavaScript/Reference/Functions/set)

set the current ``pageIndex`` by the ``index`` provided

### BehaviourController - pageIndexDisplayId

!> [**Setter**](https://developer.mozilla.org/zh-TW/docs/Web/JavaScript/Reference/Functions/set)

set the current ``pageIndex`` by the ``displayId`` of the page provided, compute its ``pageIndex`` and assign to ``this._pageIndex``

### BehaviourController - checkedList

!> [**Setter**](https://developer.mozilla.org/zh-TW/docs/Web/JavaScript/Reference/Functions/set)

set the current ``checkedList`` by the ``list`` provided

### BehaviourController - condition

!> [**Setter**](https://developer.mozilla.org/zh-TW/docs/Web/JavaScript/Reference/Functions/set)

set the current ``condition`` by the ``condition`` provided

### BehaviourController - isPassCondition

!> [**Getter**](https://developer.mozilla.org/zh-TW/docs/Web/JavaScript/Reference/Functions/get)

Get the condition is passed or not by the provided condition  
Currently, we support:  

> - PAGE_CHECK // check the page is completed or not

### BehaviourController - clickNextButton()

Perform as the nextButton is clicked,  
assign clicked status to ``nextClickedList``

### BehaviourController - submitForm()

Perform as the nextButton is clicked,  
assign clicked status to ``nextClickedList``,  
set ``submit`` is ``true``