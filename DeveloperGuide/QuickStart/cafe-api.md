# Cafe API

!> **Docs may not up-to-date**

| Last updated | Author | Version | Remarks |
|:------:|:------:|:------:|:------:|
| 21 Oct 2019 | Felix Chum | 1.0.0 | - |

### How To Start, FAQ

[Click Me](http://192.168.222.60:8001/eab-cafe/cafe-api/blob/DEV/README.md)

### Swagger

[V1](http://docs.swagger.k8s-inno1/?location=http://docs.swagger.k8s-inno1/yaml?path=@eab-cafe/api/documentation.yaml)  
[V2](http://docs.swagger.k8s-inno1/?location=http://docs.swagger.k8s-inno1/yaml?path=@eab-cafe/api/documentationV2.yaml)