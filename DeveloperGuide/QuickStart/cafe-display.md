# Cafe Display Engine

!> **Docs may not up-to-date**

| Last updated | Author | Version | Remarks |
|:------:|:------:|:------:|:------:|
| 21 Oct 2019 | Felix Chum | 1.0.0 | - |

## Screenshots

![questionUtils_type1](img/cafe-display.png)  

## Prerequisite

1. [Chrome](https://www.google.com/chrome/)

## Before you starts

### Suggested Plugins

- **Chrome**
  - [React Debugger](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=en)
- **Visual Studio Code**
  - Auto Rename Tag
  - Bracket Pair Colorizer 2
  - CSS-in-JS
  - Color Info
  - ESLint

### How To Start, FAQ

[Click Me](http://192.168.222.60:8001/eab-cafe/cafe-interface-engine/blob/DEV/README.md)