# Cafe Display Engine

!> **Docs may not up-to-date**

| Last updated | Author | Version | Remarks |
|:------:|:------:|:------:|:------:|
| 21 Oct 2019 | Felix Chum | 1.0.0 | - |

## Before you starts

### Suggested Plugins

- **Visual Studio Code**
  - Auto Rename Tag
  - Bracket Pair Colorizer 2
  - CSS-in-JS
  - Color Info
  - ESLint

### How To Start, FAQ

[Click Me](http://192.168.222.60:8001/eab-cafe/cafe-helper-functions/blob/DEV/README.md)