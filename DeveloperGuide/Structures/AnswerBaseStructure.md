# AnswerBaseStructure

## Properties

| Property                                | Types             | Available Options                                    |
| :-------------------------------------- | :---------------- | :--------------------------------------------------- |
| [value](#value)                         | valueType         |
| [valueType](#valueType)                 | string            | ARRAY, TEXT, ARRAYOBJECT, OBJECT, ATTACHMENT, BASE64 |
| [type](#type)                           | string            |
| [optionalValue](#optionalValue)         | optionalValueType |
| [optionalValueType](#optionalValueType) | string            | TEXT                                                 |
| [isDisplayId](#isDisplayId)             | bool              |

### value

?> This property follow the logic on the based util - [AnswerMapper](DeveloperGuide/Utils/questionUtils.md#class-answermapper).

It will store the value into the valueType mentioned format.  
Other services will use the valueType to compute their logic accordingly.

### valueType

?> This property follow the logic on the based util - [AnswerMapper](DeveloperGuide/Utils/questionUtils.md#class-answermapper).

### type

Question-defined string.  
Question can use this one if there are aditional identifier question need to use.

### optionalValue

?> This property follow the logic on the based util - [AnswerMapper](DeveloperGuide/Utils/questionUtils.md#class-answermapper).

If there are optionalValue, it will fill in this field.

### optionalValueType

?> This property follow the logic on the based util - [AnswerMapper](DeveloperGuide/Utils/questionUtils.md#class-answermapper).

The type of optionalValue, currently we accepted the `TEXT` only as only text are allowed.

### isDisplayId

?> This property follow the logic on the based util - [AnswerMapper](DeveloperGuide/Utils/questionUtils.md#class-answermapper).

If it is `isDisplayId`, it will treat the value as displayId and compute it accordingly.

## Examples

When it is storing in MongoDB, it will store it like this.  
For details, you may refer to the [swagger](DeveloperGuide/dev-overview.md) - Response

```javascript
const responseObject = {
    id: "/* uniqueResponseId */",
    publishFormId: " /* relative publishedFormId */",
    answers: [
        {
            id: "/* uniqueResponseIdForQuestion */",
            answer: {
                value: "",
                valueType: "",
                type: "",
                optionalValue: "",
                optionalValueType:"",
                isDisplayId: ""
            }
        },
        ...
    ]
}
```
