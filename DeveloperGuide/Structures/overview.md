# Structures

As there are many `questionTypes`, it is pretty difficult to align all the structure to one managable format.  
So there are [questionUtils](DeveloperGuide/Utils/questionutils) to handle the [question.answers](QuestionBaseStructure.md) and [propsValue](AnswerBaseStructure.md) structures.