# QuestionBaseStructure

## Properties

| Property                          | Types          | Available Options |
| :-------------------------------- | :------------- | :---------------- |
| [answers](#answers)               | QuestionAnswer |
| [isDisable](#isDisable)           | bool           |                   |
| [isMandatory](#isMandatory)       | bool           |                   |
| [visibilityRule](#visibilityRule) | Array          |
| [validationRule](#validationRule) | Array          |
| [relationRule](#relationRule)     | Array          |
| [title](#title)                   | string         |
| [questionType](#questionType)     | string         |
| [displayId](#displayId)           | string         |
| [referenceId](#referenceId)       | string         |
| [\_id](#_id)                      | string         |
| [designSetting](#designSetting)   | string         |

## answers

?> This property follow the logic on the based util - [QuestionAnswer](DeveloperGuide/Utils/questionUtils.md#class-questionanswer).

### isDisable

If isDisable, the question is `readonly`

### isMandatory

If isMandatory, the question must be fill in before proceed to next page.

### visibilityRule

?> This property follow the logic on the based util - [VisibilityRule](DeveloperGuide/Features/visibilityRules).

Rules that contains logic of the question is visible or not.

### validationRule

?> This property follow the logic on the based util - [ValidationRule](DeveloperGuide/Features/validationRules).

Rules that contains logic of the question has error or not.

### relationRule

?> This property follow the logic on the based util - [RelationRule](DeveloperGuide/Features/relationRules).

Rules that contains logic of the question can be auto-filled by other question or not.

### title

!> Deprecated. Replaced by [langUtils](DeveloperGuide/Utils/langutils)

Form Title

### questionType

?> This property follow the logic on the based util - [QuestionAnswer](DeveloperGuide/Utils/questionUtils.md#class-questionanswer).

### displayId

Unique identifier for Cafe use.

### referenceId

Unique identifier for User use, can be customize.

### \_id

Unique identifier for system use.

### designSetting

Style properties for the question.
